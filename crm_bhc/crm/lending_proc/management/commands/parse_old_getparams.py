from django.core.management.base import BaseCommand, CommandError

from crm.lending_proc.views import save_getparams
from crm.lending_proc.models import ClientsTrack,YandexGetParametrsDynamic

from furl import furl

class Command(BaseCommand):
    help = 'парсим старые гет параметры'

    def handle(self, *args, **options):
        replace1={
            'source':'source_type',
            'utm_source':'source_type',
            'added':'addphrases',
            'block':'position_type',
            'pos':'position',
            'key':'keyword',
            'campaign':'campaign_id',
            'retargeting':'retargeting_id',
            'ad':'ad_id',
            'phrase':'phrase_id',
            'device':'device_type',
            'region':'region_id',       
        }

        getparamsreplacelist=[replace1]



        for visit in ClientsTrack.objects.all():
            try:
                paramsdin=YandexGetParametrsDynamic.objects.get(visit=visit)
            except:
                paramsdin=None

            if not visit.getparamstat and not paramsdin:
                url=furl(visit.url)
                for getparamsreplace in getparamsreplacelist:
                    for getparam in url.args.keys():
                        if getparam in getparamsreplace:  
                            url.add({getparamsreplace[getparam]:url.args[getparam]})
                save_getparams(url,visit)
                print('parsed ',url)


