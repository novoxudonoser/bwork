# -*- coding: utf-8 -*- 
from django.core.urlresolvers import reverse
from django.db import models

from crm.api.models import User
# from example.orders.models import OrderContact

CALL_TYPES = (
    (1, 'Входящий'),
    (2, 'Исходящий'),
)



class CallTheme(models.Model):
    """
        call theme
    """
    name = models.CharField(
        max_length=255,
        verbose_name='Тема звонка',
        unique=True
    )
    sort = models.PositiveSmallIntegerField(
        verbose_name='Порядок сортировки',
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['sort']
        verbose_name = 'Тема звонка'
        verbose_name_plural = 'Темы звонков'


class CallSource(models.Model):
    """
        call source
    """
    name = models.CharField(
        max_length=255,
        verbose_name='Название источника',
        unique=True
    )

    def __str__(self):
        return self.name


class CallQuestion(models.Model):
    """
        call questions
    """
    name = models.CharField(
        max_length=255,
        verbose_name='Вопрос звонка'
    )
    theme = models.ForeignKey(CallTheme, verbose_name='Тема вопроса', default=None, null=True)
    sort = models.PositiveSmallIntegerField(
        verbose_name='Порядок сортировки',
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['sort']
        verbose_name = 'Вопрос звонка'
        verbose_name_plural = 'Вопросы звонков'


class Call(models.Model):
    """
        Model for in and out calls
    """
    type = models.IntegerField(choices=CALL_TYPES, verbose_name='Тип звонка', default=1)
    question = models.ManyToManyField(CallQuestion, verbose_name='Вопросы звонка', related_name='questions')
    comment = models.TextField(verbose_name='Комментарий оператора', blank=True, null=True)
    user = models.ForeignKey(User, related_name='calls', verbose_name='Оператор')
    number = models.CharField(max_length=255, verbose_name='Номер заказа', null=True, blank=True)

    date_pushed = models.DateTimeField(verbose_name='Дата нажатия кнопки')
    date_created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    date_updated = models.DateTimeField(verbose_name='Дата обновления', auto_now=True)

    source = models.ForeignKey(CallSource, verbose_name='Источник информации')

    client = models.ForeignKey('orders.OrderContact', verbose_name='Клиент', related_name='client_calls', null=True,
                               default=None, blank=True)

    def get_absolute_url(self):
        return reverse('calls-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return 'Звонок'