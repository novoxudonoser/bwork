# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('calls', '0001_initial'),
        ('orders', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='call',
            name='client',
            field=models.ForeignKey(null=True, related_name='client_calls', default=None, blank=True, verbose_name='Клиент', to='orders.OrderContact'),
        ),
        migrations.AddField(
            model_name='call',
            name='question',
            field=models.ManyToManyField(verbose_name='Вопросы звонка', related_name='questions', to='calls.CallQuestion'),
        ),
        migrations.AddField(
            model_name='call',
            name='source',
            field=models.ForeignKey(verbose_name='Источник информации', to='calls.CallSource'),
        ),
        migrations.AddField(
            model_name='call',
            name='user',
            field=models.ForeignKey(related_name='calls', verbose_name='Оператор', to=settings.AUTH_USER_MODEL),
        ),
    ]
