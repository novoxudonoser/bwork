# -*- coding: utf-8 -*- 
from __future__ import unicode_literals

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.wagtailcore import blocks
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, \
    PageChooserPanel

from bhc_site.home.snippets import HomeSnippets, OfficeSnippet, NewsCategory


class BHCPage(Page):
    show_rigth_news = models.BooleanField(default=True, verbose_name="Показывать новости в правом блоке")
    show_rigth_partners = models.BooleanField(default=True, verbose_name="Показывать партнеров в правом блоке")
    is_abstract = True

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('show_rigth_news'),
            FieldPanel('show_rigth_partners'),
        ], "Блоки в правом меню"),
    ]

    class Meta:
        abstract = True


class NewsPage(BHCPage):
    category = models.ForeignKey(NewsCategory, default=None, null=True, blank=True, related_name='category_news')
    short_title = models.CharField(max_length=255)
    anons = RichTextField()
    content = StreamField([
        ('image', ImageChooserBlock()),
        ('paragraph', blocks.RichTextBlock()),
    ])

    class Meta:
        verbose_name = "Новость"

    content_panels = BHCPage.content_panels + [
        SnippetChooserPanel('category', NewsCategory),
        FieldPanel('short_title'),
        FieldPanel('anons'),
        StreamFieldPanel('content'),
        InlinePanel('related_links', label="Связанные ссылки"),
    ]


class NewsListPage(BHCPage):
    class Meta:
        verbose_name = "Страница списка новостей"

    def get_context(self, request, *args, **kwargs):
        context = super(NewsListPage, self).get_context(request, *args, **kwargs)
        news_queryset = NewsPage.objects.live().order_by('-go_live_at', '-id')

        category = request.GET.get('category')
        if category:
            news_queryset = news_queryset.filter(category_id=category)

        paginator = Paginator(news_queryset, 2)
        page = request.GET.get('page')
        try:
            items = paginator.page(page)
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)

        context['news_items'] = items
        if category:
            context['category'] = category

        return context


class ServicePage(BHCPage):
    category = models.ForeignKey(NewsCategory, default=None, null=True, blank=True, related_name='category_services')
    short_title = models.CharField(max_length=255)
    anons = RichTextField()
    content = StreamField([
        ('image', ImageChooserBlock()),
        ('paragraph', blocks.RichTextBlock()),
    ])

    content_panels = BHCPage.content_panels + [
        SnippetChooserPanel('category', NewsCategory),
        FieldPanel('short_title'),
        FieldPanel('anons'),
        StreamFieldPanel('content'),
        InlinePanel('related_links', label="Связанные ссылки"),
    ]

    class Meta:
        verbose_name = "Услуга"


class ServiceListPage(BHCPage):
    def get_context(self, request, *args, **kwargs):
        context = super(ServiceListPage, self).get_context(request, *args, **kwargs)
        service_queryset = ServicePage.objects.live().order_by('-go_live_at', '-id')
        paginator = Paginator(service_queryset, 5)

        page = request.GET.get('page')
        try:
            items = paginator.page(page)
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)

        context['news_items'] = items
        return context



class Meta:
    verbose_name = "Страница списка Услуг"


class HomePage(Page):
    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name='Главный баннер'
    )

    texts = models.ForeignKey(
        'HomeSnippets',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name='Тексты'
    )

    news = models.ManyToManyField('NewsPage')

    content_panels = Page.content_panels + [
        SnippetChooserPanel('texts', HomeSnippets),
        ImageChooserPanel('banner_image'),
    ]

    promote_panels = [
        MultiFieldPanel(Page.promote_panels, "Основные настройки системы"),
    ]

    class Meta:
        verbose_name = "Главная страница"


class ContactPage(BHCPage):
    class Meta:
        verbose_name = "Страница контактов"
        verbose_name_plural = "Страницы контактов"

    content_panels = BHCPage.content_panels + [
        InlinePanel('office_placements', label="Офисы"),
    ]


class ContactPageOfficePlacement(Orderable, models.Model):
    page = ParentalKey(ContactPage, related_name='office_placements')
    office = models.ForeignKey(OfficeSnippet, related_name='+')

    class Meta:
        verbose_name = "Office Placement"
        verbose_name_plural = "Office Placements"

    panels = [
        SnippetChooserPanel('office', OfficeSnippet)
    ]

    def __str__(self):
        return self.page.title + " -> " + self.office.title


# A couple of abstract classes that contain commonly used fields

class LinkFields(models.Model):
    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+'
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
        DocumentChooserPanel('link_document'),
    ]

    class Meta:
        abstract = True


# Related links

class RelatedLink(LinkFields):
    title = models.CharField(max_length=255, help_text="Link title")

    panels = [
        FieldPanel('title'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

    class Meta:
        abstract = True


class ServicePageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('ServicePage', related_name='related_links')

class NewsPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('NewsPage', related_name='related_links')
