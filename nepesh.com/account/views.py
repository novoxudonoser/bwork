from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from django.views.generic.edit import FormView

from account.forms import UserCreationForm, PersonalDataForm
from account.models import VeloUser


class RegisterView(FormView):
    template_name = 'account/register.html'
    form_class = UserCreationForm
    success_url = '/account/login/'

    def form_valid(self, form):
        form.save()
        return super(RegisterView, self).form_valid(form)


class ProfileView(LoginRequiredMixin, generic.UpdateView):
    template_name = 'account/profile.html'
    form_class = PersonalDataForm
    model = get_user_model()

    def get_object(self, queryset=None):
        return VeloUser.objects.get(email=self.request.user.email)

    def get_context_data(self, **kwargs):
        c = super(ProfileView, self).get_context_data(**kwargs)
        c['view'] = 'personal'
        c['methods'] = self.form_class.Meta.model.CONTACT_METHODS
        return c


class ProfilePasswordView(LoginRequiredMixin, generic.FormView):
    template_name = 'account/profile.html'
    form_class = PasswordChangeForm

    def get_form(self, form_class):
        return form_class(self.request.user, **self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        c = super(ProfilePasswordView, self).get_context_data(**kwargs)
        c['view'] = 'password'
        return c


class ProfileDeleteView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'account/profile.html'

    def get_context_data(self, **kwargs):
        c = super(ProfileDeleteView, self).get_context_data(**kwargs)
        c['view'] = 'delete'
        return c
