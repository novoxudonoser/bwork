from django.conf import urls
from images.views import ParcelAjaxTmpFileUploadView, ParcelAjaxTmpFileDeleteView

urlpatterns = [
    urls.url(r'^upload/?$', ParcelAjaxTmpFileUploadView.as_view(), name='images_upload'),
    urls.url(r'^delete/?$', ParcelAjaxTmpFileDeleteView.as_view(), name='images_delete'),
]
