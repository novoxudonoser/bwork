# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lending_proc', '0004_auto_20160122_2114'),
    ]

    operations = [
        migrations.CreateModel(
            name='DirectAdds',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('ad_id', models.BigIntegerField(unique=True, verbose_name='Идентификатор объявления')),
            ],
            options={
                'verbose_name': 'Direct Adds',
                'verbose_name_plural': 'Direct Adds',
            },
        ),
        migrations.CreateModel(
            name='DirectCompanes',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('campaign_id', models.BigIntegerField(unique=True, verbose_name='Идентификатор рекламной кампании')),
                ('name', models.CharField(blank=True, default=None, max_length=20, null=True)),
                ('compane', models.ForeignKey(verbose_name='Кампания', to='lending_proc.Companes', blank=True, default=None, null=True)),
                ('leding', models.ForeignKey(to='lending_proc.Lendings', verbose_name='Лендинг')),
            ],
            options={
                'verbose_name': 'Direct Companes',
                'verbose_name_plural': 'Direct Companes',
            },
        ),
        migrations.CreateModel(
            name='DirectGroups',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('gbid', models.BigIntegerField(unique=True, verbose_name='Идентификатор группы')),
                ('diectcomp', models.ForeignKey(verbose_name='Директ рекламная копания', to='lending_proc.DirectCompanes', blank=True, default=None, null=True)),
            ],
            options={
                'verbose_name': 'Direct Groups',
                'verbose_name_plural': 'Direct Groups',
            },
        ),
        migrations.CreateModel(
            name='DirectPhrases',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('phrase_id', models.BigIntegerField(unique=True, verbose_name='Идентификатор ключевой фразы')),
                ('diectadd', models.ForeignKey(verbose_name='Директ объявление', to='lending_proc.DirectAdds', blank=True, default=None, null=True)),
            ],
            options={
                'verbose_name': 'Direct Phrases',
                'verbose_name_plural': 'Direct Phrases',
            },
        ),
        migrations.CreateModel(
            name='DirectRegions',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('region_id', models.SmallIntegerField(unique=True, verbose_name='Идентификатор региона')),
            ],
            options={
                'verbose_name': 'Direct Regions',
                'verbose_name_plural': 'Direct Regions',
            },
        ),
        migrations.CreateModel(
            name='SearchqQuerys',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('qerydata', models.CharField(max_length=50, verbose_name='Текст запроса')),
            ],
            options={
                'verbose_name': 'Поисковые запросы',
                'verbose_name_plural': 'Поисковые запросы',
            },
        ),
        migrations.CreateModel(
            name='SearchqQuerysStats',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('num_visits', models.IntegerField(default=0, verbose_name='Количетво переходов')),
                ('num_calls', models.IntegerField(default=0, verbose_name='Количетво звонков')),
                ('num_apps', models.IntegerField(default=0, verbose_name='Количетво заявок')),
            ],
            options={
                'verbose_name': 'Счётчик статиситки',
                'verbose_name_plural': 'Счётчик статиситки',
            },
        ),
        migrations.CreateModel(
            name='YandexGetParametrsDynamic',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('yclid', models.CharField(blank=True, max_length=30, null=True, verbose_name='click id')),
                ('device_type', models.SmallIntegerField(blank=True, choices=[(1, 'desktop'), (2, 'mobile'), (3, 'tablet')], null=True, verbose_name='Тип устройства')),
                ('retargeting_id', models.BigIntegerField(blank=True, null=True, verbose_name='Идентификатор условия ретаргетинга')),
                ('adtarget_id', models.BigIntegerField(blank=True, null=True, verbose_name='Идентификатор условия ретаргетинга')),
                ('adtarget_name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Условие нацеливания, по которому было показано динамическое объявление')),
                ('position', models.SmallIntegerField(blank=True, null=True, verbose_name='Точная позиция объявления в блоке')),
                ('position_type', models.SmallIntegerField(blank=True, choices=[(1, 'premium'), (2, 'other'), (3, 'none')], null=True, verbose_name='Тип блока')),
                ('addphrases', models.SmallIntegerField(blank=True, choices=[(1, 'yes'), (2, 'no')], null=True, verbose_name='Авто добавление фразы')),
                ('addphrasestext', models.CharField(blank=True, max_length=50, null=True, verbose_name='Текст автоматически добавленной фразы')),
            ],
            options={
                'verbose_name': 'Yandex get параметры Динамические',
                'verbose_name_plural': 'Yandex get параметры Динамические',
            },
        ),
        migrations.CreateModel(
            name='YandexGetParametrsStatic',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('hash', models.CharField(unique=True, max_length=20, editable=False)),
                ('campaign_type', models.SmallIntegerField(blank=True, choices=[(1, 'type1'), (2, 'type2'), (3, 'type3')], null=True, verbose_name='Тип кампании')),
                ('keyword', models.CharField(blank=True, max_length=50, null=True, verbose_name='Ключевая фраза')),
                ('source', models.CharField(blank=True, max_length=50, null=True, verbose_name='Место показа')),
                ('source_type', models.SmallIntegerField(blank=True, choices=[(1, 'search'), (2, 'context')], null=True, verbose_name='Тип площадки')),
                ('region_name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Регион')),
                ('ad_id', models.ForeignKey(verbose_name='Идентификатор объявления', to='lending_proc.DirectAdds', blank=True, null=True)),
                ('campaign_id', models.ForeignKey(verbose_name='Идентификатор рекламной кампании', to='lending_proc.DirectCompanes', blank=True, null=True)),
                ('gbid', models.ForeignKey(verbose_name='Идентификатор группы', to='lending_proc.DirectGroups', blank=True, null=True)),
                ('phrase_id', models.ForeignKey(verbose_name='Идентификатор ключевой фразы', to='lending_proc.DirectPhrases', blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Yandex get параметры Статические',
                'verbose_name_plural': 'Yandex get параметры Статические',
            },
        ),
        migrations.CreateModel(
            name='YgetparamesstatSearchqerysRel',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('getparamstat', models.ForeignKey(to='lending_proc.YandexGetParametrsStatic', related_name='YgetparamesstatSearchqerysRel')),
                ('query', models.ForeignKey(to='lending_proc.SearchqQuerys', related_name='YgetparamesstatSearchqerysRel')),
                ('stats', models.OneToOneField(to='lending_proc.SearchqQuerysStats', blank=True, default=None, null=True, related_name='YgetparamesstatSearchqerysRel')),
            ],
        ),
        migrations.RemoveField(
            model_name='getparametrs',
            name='lending',
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='counted',
            field=models.BooleanField(default=False, verbose_name='Было подсчитано при роцесенге поисковых запросов'),
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='visit',
            field=models.ForeignKey(verbose_name='Посещение', to='lending_proc.ClientsTrack', blank=True, null=True, related_name='app'),
        ),
        migrations.AddField(
            model_name='clients',
            name='apped',
            field=models.BooleanField(default=False, verbose_name='Отсылал ли заявку'),
        ),
        migrations.AddField(
            model_name='clients',
            name='lending',
            field=models.ForeignKey(verbose_name='Лендинг', to='lending_proc.Lendings', null=True),
        ),
        migrations.AddField(
            model_name='clients',
            name='phoned',
            field=models.BooleanField(default=False, verbose_name='Звонил ли'),
        ),
        migrations.AddField(
            model_name='clients',
            name='visits',
            field=models.BigIntegerField(default=0, verbose_name='Количество посещений по рекламе'),
        ),
        migrations.AddField(
            model_name='clientstrack',
            name='ip_address',
            field=models.GenericIPAddressField(null=True),
        ),
        migrations.AddField(
            model_name='pfoneincomelog',
            name='counted',
            field=models.BooleanField(default=False, verbose_name='Было подсчитано при роцесенге поисковых запросов'),
        ),
        migrations.AddField(
            model_name='pfoneincomelog',
            name='visit',
            field=models.ForeignKey(verbose_name='Посещение', to='lending_proc.ClientsTrack', blank=True, null=True, related_name='phone'),
        ),
        migrations.AlterField(
            model_name='applicationuser',
            name='ip_address',
            field=models.GenericIPAddressField(null=True),
        ),
        migrations.AlterField(
            model_name='clientstrack',
            name='lending',
            field=models.ForeignKey(verbose_name='Лендинг', to='lending_proc.Lendings', null=True),
        ),
        migrations.DeleteModel(
            name='GetParametrs',
        ),
        migrations.AddField(
            model_name='yandexgetparametrsstatic',
            name='query',
            field=models.ManyToManyField(to='lending_proc.SearchqQuerys', through='lending_proc.YgetparamesstatSearchqerysRel', related_name='getparamstat', verbose_name='Поисковый запрос'),
        ),
        migrations.AddField(
            model_name='yandexgetparametrsstatic',
            name='region_id',
            field=models.ForeignKey(verbose_name='Идентификатор региона', to='lending_proc.DirectRegions', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='yandexgetparametrsdynamic',
            name='visit',
            field=models.OneToOneField(verbose_name='Посещение', to='lending_proc.ClientsTrack', related_name='getparamdyn'),
        ),
        migrations.AddField(
            model_name='searchqquerys',
            name='stats',
            field=models.OneToOneField(to='lending_proc.SearchqQuerysStats', blank=True, default=None, null=True, related_name='SearchqQuerys'),
        ),
        migrations.AddField(
            model_name='directphrases',
            name='stats',
            field=models.OneToOneField(to='lending_proc.SearchqQuerysStats', blank=True, default=None, null=True, related_name='DirectPhrases'),
        ),
        migrations.AddField(
            model_name='directgroups',
            name='stats',
            field=models.OneToOneField(to='lending_proc.SearchqQuerysStats', blank=True, default=None, null=True, related_name='DirectGroups'),
        ),
        migrations.AddField(
            model_name='directcompanes',
            name='stats',
            field=models.OneToOneField(to='lending_proc.SearchqQuerysStats', blank=True, default=None, null=True, related_name='DirectCompanes'),
        ),
        migrations.AddField(
            model_name='directadds',
            name='diectgroup',
            field=models.ForeignKey(verbose_name='Директ группа объявлений', to='lending_proc.DirectGroups', blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='directadds',
            name='stats',
            field=models.OneToOneField(to='lending_proc.SearchqQuerysStats', blank=True, default=None, null=True, related_name='DirectAdds'),
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='query',
            field=models.ForeignKey(verbose_name='Поисковый запрос', to='lending_proc.SearchqQuerys', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='clientstrack',
            name='getparamstat',
            field=models.ForeignKey(verbose_name='YandexGetParametrsStatic', to='lending_proc.YandexGetParametrsStatic', blank=True, default=None, null=True, related_name='visit'),
        ),
        migrations.AddField(
            model_name='clientstrack',
            name='getparamstatquery',
            field=models.OneToOneField(to='lending_proc.YgetparamesstatSearchqerysRel', blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='clientstrack',
            name='query',
            field=models.ForeignKey(verbose_name='Поисковый запрос', to='lending_proc.SearchqQuerys', blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='pfoneincomelog',
            name='query',
            field=models.ForeignKey(verbose_name='Поисковый запрос', to='lending_proc.SearchqQuerys', blank=True, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='ygetparamesstatsearchqerysrel',
            unique_together=set([('query', 'getparamstat')]),
        ),
    ]
