__author__ = 'PekopT'
from django.db import models
from django.contrib import admin
from django.forms import CheckboxSelectMultiple

from .models import CallTheme, CallQuestion, CallSource


class CallQuestionlAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort')
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

class CallThemeAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort')


admin.site.register(CallTheme, CallThemeAdmin)
admin.site.register(CallSource)
admin.site.register(CallQuestion, CallQuestionlAdmin)
