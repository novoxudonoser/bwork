from django.core.urlresolvers import reverse

from django.utils import timezone
from django.forms import models
from django import forms

from crm.calls.models import Call, CallQuestion, CallSource, CallTheme

__author__ = 'PekopT'


class CallForm(models.ModelForm):
    class Meta:
        model = Call
        fields = [
            # 'user_name',
            # 'email',
            # 'phone',
            'client',
            'source',
            'theme',
            'question',
            'comment',

            'number'
        ]

    class Media:
        js = ('/static/jquery.maskedinput/src/jquery.maskedinput.js',
              '/static/js/call_form.js',
              )

    question = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), required=True,
                                              queryset=CallQuestion.objects.all())
    date_pushed = forms.DateTimeField(widget=forms.HiddenInput(), required=True)

    source = forms.ModelChoiceField(widget=forms.RadioSelect(), required=True, queryset=CallSource.objects.all())
    theme = forms.ModelChoiceField(widget=forms.RadioSelect(), required=True, queryset=CallTheme.objects.all())

    def __init__(self, *args, **kwargs):
        super(CallForm, self).__init__(*args, **kwargs)
        self.initial['date_pushed'] = timezone.now().strftime('%Y-%m-%d %H:%M:%S')
