# -*- coding: utf-8 -*- 
from django.db import models

__author__ = 'PekopT'


class Counters(models.Model):
    id = models.IntegerField(primary_key=True)
    site = models.CharField(max_length=255)
    name = models.CharField(max_length=255)


class TrafficSummary(models.Model):
    """
        denial: 0.2143,
        date: "20151024",
        visits: 14,
        visitors: 14,
        depth: 3.6429,
        page_views: 51,
        visit_time: 51,
        new_visitors: 14,
        id: "20151024"
    """
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    visitors = models.IntegerField(help_text="Посетители.")
    depth = models.FloatField()
    page_views = models.IntegerField(help_text="Просмотры.")
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    new_visitors = models.IntegerField(help_text="Новые посетители.")


class TrafficDeepness(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    name = models.CharField(max_length=20)
    visits = models.IntegerField(help_text="Визиты.")
    depth = models.FloatField()
    visits_percent = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


class TrafficHourly(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    avg_visits = models.FloatField()
    hours      = models.TimeField()


class TrafficLoad(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    max_rps        = models.IntegerField(help_text="Запросы в секунду (максимум).")
    max_users      = models.IntegerField(help_text="Посетители онлайн (максимум).")
    max_rps_date   = models.DateField(help_text="Дата достижения максимального числа запросов в секунду.")
    max_rps_time   = models.TimeField(help_text="Время достижения максимального числа запросов в секунду.")
    max_users_date = models.DateField(help_text="Дата достижения максимального числа посетителей.")
    max_users_time = models.TimeField(help_text="Время достижения максимального числа посетителей.")


class SourcesSummary(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    depth = models.FloatField()


class SourcesSites(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


class SourcesSearch_engines(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    name = models.CharField(max_length=20)
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


class SourcesPhrases(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    phrase = models.CharField(max_length=100)
    search_engine = models.CharField(max_length=20)


class SourcesMarketing(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


class SourcesDirectSummary(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


class SourcesDirectPlatforms(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    name = models.CharField(max_length=255, help_text="Название рекламной площадки.")
    name_platform = models.CharField(max_length=255, help_text="Тип рекламной площадки: «Поиск» или «Контекст».")


class SourcesDirectRegions(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


class SourcesDirectTags(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    transits = models.IntegerField(help_text="Переходы.")


class ContentPopular(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    url = models.CharField(max_length=255, help_text="Адрес страницы или раздела сайта.")
    page_views = models.IntegerField(help_text="Просмотры.")
    entrance = models.IntegerField(help_text="Количество входов.")
    exit = models.IntegerField(help_text="Количество выходов.")


class ContentEntrance(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    url = models.CharField(max_length=255, help_text="Адрес страницы или раздела сайта.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


class ContentExit(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    url = models.CharField(max_length=255, help_text="Адрес страницы или раздела сайта.")
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


class ContentTitles(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    page_views = models.IntegerField(help_text="Просмотры.")
    name = models.CharField(max_length=20)


class ContentUrlParam(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    page_views = models.IntegerField(help_text="Просмотры.")
    name = models.CharField(max_length=20)

class ContentUserVars(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")


#class ContentEcommerce(models.Model):
#    counter_id = models.ForeignKey(Counters)
#    date = models.DateField()


class Geo(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    name = models.CharField(max_length=20)

class DemographyAgeGender_Age(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    visits_percent = models.FloatField()
    name = models.CharField(max_length=20)

class DemographyAgeGender_Sex(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    visits_percent = models.FloatField()
    name = models.CharField(max_length=20)


class DemographyStructure(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    visits_percent = models.FloatField()
    name = models.CharField(max_length=20)
    name_gender = models.CharField(max_length=20)


class TechBrowsers(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    name = models.CharField(max_length=255, help_text="Название браузера.")
    version=models.CharField(max_length=255, help_text="Название браузера.")


class TechOs(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    name = models.CharField(max_length=255, help_text="Название операционной системы.")


class TechDisplay(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    name = models.CharField(max_length=20)
    format = models.CharField(max_length=20, help_text="Формат дисплея.")
    width = models.IntegerField(help_text="Ширина разрешения дисплея в пикселях.")
    height = models.IntegerField(help_text="Высота разрешения дисплея в пикселях.")


class TechMobile(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    name = models.CharField(max_length=255, help_text="Название производителя и модели устройства.")

class TechMobile_percent(models.Model):
    counter_id = models.ForeignKey(Counters)
    date = models.DateField()
    visits = models.IntegerField(help_text="Визиты.")
    page_views = models.IntegerField(help_text="Просмотры.")
    depth = models.FloatField()
    visit_time = models.IntegerField(help_text="Среднее время в секундах, проведенное на сайте посетителями.")
    name = models.CharField(max_length=20)




