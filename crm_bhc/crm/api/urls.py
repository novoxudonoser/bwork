from django.conf.urls import patterns, url

from crm.calls.api import CallList, CallDetail, OrderContactList, QuestionList
from crm.orders.api import ExcellentAddNewCity, ClientRequisites, GetAddrObject, OrderDocumentList, OrderAddressList, OrderDocumentDetail, OrderContactPerson

urlpatterns = patterns('',
       # url(r'^users', include(user_urls)),
       # url(r'^posts', include(post_urls)),
       # url(r'^photos', include(photo_urls)),
       url(r'^calls/?$', CallList.as_view(), name='call-list'),

       url(r'^call/?', CallDetail.as_view(), name='call-detail'),
       url(r'^call/(?P<pk>\d+)/?$', CallDetail.as_view(), name='call-detail'),

       url(r'^orders/contacts/?$', OrderContactList.as_view(), name='api-orderclient-list'),
       url(r'^orders/contact/last_data/?$', ClientRequisites.as_view(), name='api-last-client-data'),
       url(r'^orders/documents/?$', OrderDocumentList.as_view(), name='api-orderaddresses-list'),
       url(r'^documents/questions/?$', QuestionList.as_view(), name='api-question-list'),

       url(r'^document/?$', OrderDocumentDetail.as_view(), name='document-detail'),
       url(r'^document/(?P<pk>\d+)/?$', OrderDocumentDetail.as_view(), name='document-detail'),

       url(r'^addresses/?$', OrderAddressList.as_view(), name='api-address-list'),
       url(r'^get_contact_person/?$', OrderContactPerson.as_view(), name='api-contact-person'),
       url(r'^get_addr_object/?$', GetAddrObject.as_view(), name='api-get-addr-object'),
       url(r'^add_city/?$', ExcellentAddNewCity.as_view(), name='api-add-city2')
)
