from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

__author__ = 'PekopT'


class LoginRequiredMixin(object):
    @method_decorator(login_required(login_url='/crm/login'))
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)
