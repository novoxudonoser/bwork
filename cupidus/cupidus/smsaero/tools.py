import requests
from requests.exceptions import RequestException
from furl import furl
import re

MAIN_URL            = 'http://gate.smsaero.ru'
SMS_SEND_PATH       = 'send'
SMS_GET_STATUS_PATH = 'status'


def add_default_parameters(user,password):
	url=furl(MAIN_URL)
	url.add({"user":user,"password":password,"answer":"json"})
	return url

def parse_number(number):
	return str(number).replace(' ', '').replace('-', '').replace('+', '').replace('(', '').replace(')', '')

def make_request(url):
	try:
		data=requests.get(url)
	except RequestException as e:
		return False,e
	if data.status_code != 200:
		return False,"Bad reqest: "+str(data.status_code)
	try:
		data = data.json()
	except:
		return False,data.text
	return True,data

def send_sms(user,password,number,text,signature):
	text=text.replace('&nbsp;','')
	url=add_default_parameters(user,password)
	url.path=SMS_SEND_PATH
	url.add({"to":parse_number(number),"text":text,"from":signature})
	ok,data=make_request(url.url)
	if not ok:
		return False,data
	if data.get('result') == 'accepted':
		return True,data.get('id')
	else:
		return False,data.get('reason')

def get_sms_status(user,password,sms_id):
	url=add_default_parameters(user,password)
	url.path=SMS_GET_STATUS_PATH
	url.add({"id":str(sms_id)})
	ok,data=make_request(url.url)
	if not ok:
		return False,data
	if data.get('result') != 'reject':
		return True,data.get('result')
	else:
		return False,data.get('reason')



