from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.views.generic import ListView
from crm.users.mixins import LoginRequiredMixin

from crm.lending_proc.models import ApplicationUser


__author__ = 'nyafka'


class ApplicationUsers(LoginRequiredMixin, ListView):
    model = ApplicationUser

    def get_template_names(self):
        return 'application_user.html'

    def get_context_data(self, **kwargs):
        context = super(ApplicationUsers, self).get_context_data(**kwargs)

        applications_list = ApplicationUser.objects.all().order_by('-created')

        paginator = Paginator(applications_list, 25)

        page = self.request.GET.get('page')
        try:
            applications = paginator.page(page)
        except PageNotAnInteger:
            applications = paginator.page(1)
        except EmptyPage:
            applications = paginator.page(paginator.num_pages)

        context['applications'] = applications
        context['page'] = page

        return context
