import json
import os

from django import forms
from django.db import models
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from velos.models import BikePhoto, StudioPhotos


class TmpFileStorage(models.Model):
    attach = models.FileField(u'Приложение', upload_to='tmp/filestorage')

    class Meta:
        verbose_name = u'Временное хранилище файлов'
        verbose_name_plural = u'Временное хранилище файлов'

    def __unicode__(self):
        if self.pk:
            return u'TMP FILE #%s' % self.pk
        return u'TMP FILE'

    def as_dict(self):
        if self.attach:
            name = None
            split_name = os.path.split(self.attach.name)

            if split_name and len(split_name) > 0:
                name = split_name[-1]
            return {
                'pk': self.pk,
                'attach': {
                    'url': self.attach.url,
                    'name': name,
                },
                'model': 'tmp'
            }
        return None


class TmpFileStorageForm(forms.ModelForm):
    class Meta:
        model = TmpFileStorage
        fields = ('attach',)


class ParcelAjaxTmpFileUploadView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ParcelAjaxTmpFileUploadView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = TmpFileStorageForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            tmpfile_storage = form.save()
            return HttpResponse(json.dumps({
                'status': 'success',
                'success': True,
                'data': tmpfile_storage.as_dict(),
            }), content_type='application/json')
        else:
            return HttpResponse(json.dumps({
                'status': 'fail',
                'success': False,
                'errors': form.errors,
            }), content_type='application/json')


class ParcelAjaxTmpFileDeleteView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ParcelAjaxTmpFileDeleteView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        try:
            models = dict(
                tmp=TmpFileStorage,
                BikePhoto=BikePhoto,
                StudioPhotos=StudioPhotos
            )
            model = models[request.POST.get('model', False)]
            i = model.objects.get(pk=self.request.POST.get('pk'))
            i.delete()
            return HttpResponse(json.dumps({
                'status': 'success',
                'success': True,
            }), content_type='application/json')
        except Exception as e:
            return HttpResponse(json.dumps({
                'status': 'fail',
                'success': False,
                'errors': str(e),
            }), content_type='application/json')
