from django.views.generic import TemplateView
from example.forms import BaseForm

from django.http import HttpResponse
import json

class BaseFormView(TemplateView):
    template_name = 'baseform.html'

    def get_context_data(self, **kwargs):
        context = super(BaseFormView, self).get_context_data(**kwargs)
        context.update(form=BaseForm())
        return context
    def post(self, request, *args, **kwargs):
            if not request.is_ajax():
                return HttpResponseBadRequest('Expected an XMLHttpRequest')
            in_data = json.loads(request.body.decode('utf-8'))
            form = BaseForm(data={'subject': in_data.get('subject')})
            if form.is_valid():
                print('DATA:',form.data)
                return HttpResponse(status=200)
            else:
                print('DATA isnt valid')
                return HttpResponse(status=400)
            # now validate ‘bound_contact_form’ and use it as in normal Django