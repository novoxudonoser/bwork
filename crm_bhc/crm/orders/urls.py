__author__ = 'PekopT'

from django.conf.urls import patterns, url

from .views import OrderList, OrderDetail, OrderDelete, OrderViewForm

urlpatterns = patterns('',
       url(r'^$', OrderList.as_view(), name='orders-list'),
       url(r'^(?P<pk>\d+)/?$', OrderDetail.as_view(), name='orders-detail'),
       url(r'^(?P<pk>\d+)/edit/?$', OrderViewForm.as_view(), name='orders-update'),
       url(r'^(?P<pk>\d+)/delete/?$', OrderDelete.as_view(), name='orders-delete'),
       url(r'add/?$', OrderViewForm.as_view(), name='orders-create'),
       url(r'^add/(?P<call_id>\d+)/?$', OrderViewForm.as_view(), name='orders-create-call'),
       url(r'^(?P<pk>\d+)/edit/?$', OrderViewForm.as_view(), name='orders-route-update'),
)
