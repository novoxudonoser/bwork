
var my_app = angular.module('example',['ng.django.urls','uniqueField','ui.mask','ui.select','ui.event','ui.bootstrap','telfilter']).config(function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }).config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    }).run(['$templateCache', function($templateCache){
    $templateCache.put("bootstrap/match.tpl.html","<div class=\"ui-select-match\" ng-hide=\"$select.open\" ng-disabled=\"$select.disabled\" ng-class=\"{\'btn-default-focus\':$select.focus}\"><span name=\"city\" tabindex=\"-1\" class=\"btn btn-default form-control form-control-city ui-select-toggle\" aria-label=\"{{ $select.baseTitle }} activate\" ng-disabled=\"$select.disabled\" ng-click=\"$select.activate()\" style=\"outline: 0;\" ng-class=\"{'has-error': registerForm.city.$invalid && registerForm.city.$touched}\"><span ng-show=\"$select.isEmpty()\" class=\"ui-select-placeholder text-muted\">{{$select.placeholder}}</span> <span ng-hide=\"$select.isEmpty()\" class=\"ui-select-match-text pull-left\" ng-class=\"{\'ui-select-allow-clear\': $select.allowClear && !$select.isEmpty()}\" ng-transclude=\"\"></span> <i class=\"caret pull-right\" ng-click=\"$select.toggle($event)\"></i> <a ng-show=\"$select.allowClear && !$select.isEmpty()\" aria-label=\"{{ $select.baseTitle }} clear\" style=\"margin-right: 10px\" ng-click=\"$select.clear($event)\" class=\"btn btn-xs btn-link pull-right\"><i class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></i></a></span></div>");
    $templateCache.put("bootstrap/select.tpl.html","<div class=\"ui-select-container ui-select-bootstrap dropdown\" ng-class=\"{open: $select.open}\"><div class=\"ui-select-match\"></div><input ng-class=\"{'has-error': registerForm.city.$invalid && registerForm.city.$touched}\" type=\"text\" autocomplete=\"off\" tabindex=\"-1\" aria-expanded=\"true\" aria-label=\"{{ $select.baseTitle }}\" aria-owns=\"ui-select-choices-{{ $select.generatedId }}\" aria-activedescendant=\"ui-select-choices-row-{{ $select.generatedId }}-{{ $select.activeIndex }}\" class=\"form-control form-control-city ui-select-search \" placeholder=\"{{$select.placeholder}}\" ng-model=\"$select.search\" ng-show=\"$select.searchEnabled && $select.open\" name=\"123\"><div class=\"ui-select-choices\"></div></div>");

    }
    ]);

angular.module('telfilter', []).filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3,5) + '-' + number.slice(5,7);

        return ('+7' + "(" + city + ")" + number).trim();
    };
});

angular.module('uniqueField', [])
            .directive('uniqueField', function($http) {
              var toId;
              return {
                restrict: 'A',
                require: 'ngModel',
                link: function(scope, elem, attr, ctrl) {
                  scope.$watch(attr.ngModel, function(value) {
                    if(toId) clearTimeout(toId);
                    toId = setTimeout(function(){
                      $http.get(document.location.origin+'/check_unique?'+elem[0].name+'='+value).success(function(data) {
                        if (data == "true")
                        {
                            ctrl.$setValidity('uniqueField', true);
                        }
                        else if (data == "false")
                        {
                            ctrl.$setValidity('uniqueField', false);
                        }
                      });
                    }, 200);
                  })
                }
              }
            });




my_app.controller('RegisterForm', function($scope, $http, djangoUrl) {

$scope.$watch('registerForm.phonecode.$valid', function()
{
    if ($scope.registerForm.phonecode && $scope.registerForm.phonecode.error && registerForm.phonecode.$invalid){
        $scope.registerForm.phonecode.error=false;
    }
    if ($scope.registerForm.phonecode && $scope.registerForm.phonecode.$modelValue){
        $http.post(djangoUrl.reverse('registerphonecode'),{'code':$scope.registerForm.phonecode.$modelValue,'phone':$scope.registerForm.phone.$modelValue}).then(function successCallback(response) {
            if (response.data['status'] == 'ok'){
                $scope.registerForm.phonecodeok=true;
                $scope.registerForm.phonepopover=false;
            }
            else {
                console.log(response.data);
                $scope.registerForm.phonecodeok=false;
            }
          }, function errorCallback(response) {
            alert("error")
          });
    }

})

$scope.$watch('registerForm.phone.$valid', function()
{
    // $scope.registerForm.phonepopover=true
    if ($scope.registerForm.phone.$valid){
        $scope.registerForm.phonepopover=true;
    }
    if (!($scope.registerForm.phone.$valid) && $scope.registerForm.phonepopover){
        $scope.registerForm.phonepopover=false;
    }
})


$scope.dynamicPopover = {
    content: 'Hello, World!',
    templateUrl: 'myPopoverTemplate.html',
    title: 'Title'
  };




    $scope.FormSendPhoneCode = function(model) {

        $http.post(djangoUrl.reverse('registersendphonesms'),{'phone':model.$modelValue}).then(function successCallback(response) {
            if (response.data['status'] == 'ok'){
        $scope.cantsend=true
        $scope.countDown = 30;
        var timer = setInterval(function(){
            if ($scope.countDown == 0){
                $scope.cantsend=false
                clearInterval(timer);
            }
            $scope.countDown--;
            $scope.$apply();
            console.log($scope.countDown);
        }, 1000);
            }
            else {
                console.log(response.data);
            }
          }, function errorCallback(response) {
            alert("error")
          });
    }

    $scope.FormCheckError = function(fildname) {
        fild2=$scope.$scope.registerForm[fildname]
        fild=$scope.registerForm[fildname]

        return (fild.$invalid && fild.$touched && fild2.blur)
    }

    $scope.TrigerFormFocus = function(fildname) {
        fild2=$scope.$scope.registerForm[fildname]
        fild=$scope.registerForm[fildname]

        if (!($scope.FormCheckError(fildname)))
        {
            fild2.focus=true;
            fild2.blur=false;
        }

    }

    $scope.TrigerFormBlur = function(fildname) {
        fild2=$scope.$scope.registerForm[fildname]
        fild=$scope.registerForm[fildname]

        fild2.focus=false;
        fild2.blur=true;
    }

    $scope.TrigerFormValidate = function(evt) {
        fild=$scope.registerForm[evt.target.name || evt.target.getAttribute('name')]
        fild2=$scope.$scope.registerForm[evt.target.name || evt.target.getAttribute('name')]
        if (fild.$keypresed || fild.$modelValue){
            fild.$setTouched();
            fild.$pristine=false;
            fild2.blur=true;
            fild2.focus=false;
        }

    }

    $scope.TrigerFormOnkeypress = function(evt) {
        fild=$scope.registerForm[evt.target.name || evt.target.getAttribute('name')]
        fild.$keypresed=true
    }

    $http.get(djangoUrl.reverse('citieslist'))
           .then(function(res){
              $scope.addresses = res.data;
            });

    $scope.submit = function() {
        if ($scope.registerForm.$invalid) {
            $scope.registerForm.phonecodeerror=true
            $scope.registerForm.city.$setTouched()
            angular.forEach($scope.registerForm.$error, function (field) {
                angular.forEach(field, function(errorField){
                    errorField.$setTouched();
                    errorField.$pristine=false;
                    fild2=$scope.$scope.registerForm[errorField.$name]
                    fild2.focus=false;
                    fild2.blur=true;
                });
            });
        }
        else{
            $scope.fields['city']=$scope.registerForm.city.$modelValue.id
            $http.post(djangoUrl.reverse('register'),$scope.fields).then(function successCallback(response) {
                if (response.data['status'] == 'ok'){
                    document.location.href=djangoUrl.reverse('profile');
                }
                else {
                    console.log(response.data);
                    alert(response.data);
                }
              }, function errorCallback(response) {
                alert("error")
              });
        }
    }
});

