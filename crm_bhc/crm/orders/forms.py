# -*- coding: utf-8 -*- 
from django import forms
from django.db.models import Q
from django.forms import models

from crm.api.models import SubwayStantions
from crm.calls.models import Call, CallQuestion, CallTheme
from crm.orders.models import AdditionalInfoAddress, Order, OrderContact, OrderAddress, OrderDocument, ContactPhone

__author__ = 'PekopT'


class OrderForm(models.ModelForm):
    class Meta:
        model = Order
        fields = [
            'client',
            'date_created',
            'payment_type',
            'payment_name',
            'payment_inn',
            'payment_kpp',
            'delivery_type',
            'number',
            'status'
        ]

    class Media:
        js = (
            '/static/jquery.maskedinput/src/jquery.maskedinput.js',
            '/static/js/questions_update.js',
            '/static/js/order_form.js',
            '/static/js/contact_form.js',
        )

    def __init__(self, user,  *args, **kwargs):
        import datetime
        if kwargs.get('call'):
            self.call = kwargs.pop('call')
        super(OrderForm, self).__init__(*args, **kwargs)

        if hasattr(self, 'call') and isinstance(self.call, Call):
            self.initial.update({
                'phone': self.call.phone,
                'email': self.call.email,
                'user_name': self.call.user_name,
                # 'number': '123'
            })

        self.fields['date_created'].initial = datetime.date.today()
        self.fields['payment_type'].label = 'Оплата: '
        self.fields['delivery_type'].initial = 1

        if not self.instance.id:
            try:
                self.fields['payment_type'].initial = user.settings.payment_type
            except:
                pass


class PhoneForm(models.ModelForm):
    class Meta:
        model = ContactPhone
        fields = ['email', 'phone', 'additional_number']

    def __init__(self, *args, **kwargs):
        super(PhoneForm, self).__init__(*args, **kwargs)
        self.fields['additional_number'].widget.attrs['class'] = 'phone-additional-number'


class ContactForm(models.ModelForm):
    class Media:
        js = (
            '/static/jquery.maskedinput/src/jquery.maskedinput.js',
            '/static/js/contact_form.js',
        )

    class Meta:
        model = OrderContact
        fields = ['name']


class AdditionalInfoAddressForm(models.ModelForm):
    class Meta:
        model = AdditionalInfoAddress
        fields = ['name', 'type_address']

    def __init__(self, address,  *args, **kwargs):
        self.address = address

    def save(self, commit=True, force_insert=False, force_update=False):
        f = super(AdditionalInfoAddressForm, self).save(commit=False)
        f.order_address = self.address
        f.save()
        return f


class AddressForm(models.ModelForm):
    """
        TODO: make fields saveable to db
    """

    class Meta:
        model = OrderAddress
        fields = [
            'name',
            'city',
            'metro',
            'street',
            'type_house',
            'house',
            'type_place',
            'place',
            'type_appartament',
            'appartament'
        ]

    def __init__(self, user, *args, **kwargs):
        super(AddressForm, self).__init__(*args, **kwargs)
        self.fields['name'].initial = '-'

        if not self.initial:
            try:
                self.fields['city'].initial = user.settings.city
            except Exception:
                pass

        for f in self.fields:
            self.fields[f].widget.attrs['class'] = 'class_address'
        self.fields['type_house'].widget.attrs['data-type_id'] = 1
        self.fields['house'].widget.attrs['data-type_id'] = 1
        self.fields['type_place'].widget.attrs['data-type_id'] = 2
        self.fields['place'].widget.attrs['data-type_id'] = 2
        self.fields['type_appartament'].widget.attrs['data-type_id'] = 3
        self.fields['appartament'].widget.attrs['data-type_id'] = 3

    def clean(self):
        data = self.cleaned_data

        addr_street = data.get('street', '').strip()
        addr_house = data.get('house', '').strip()
        addr_appartament = data.get('appartament', '').strip()
        addr_metro = data.get('metro', '')

        if not addr_metro:
            if not addr_street or not addr_house or not addr_appartament:
                raise forms.ValidationError('Введите все данные по адресу!')

        if not data.get('type_house'):
            data['house'] = ''

        if not data.get('type_place'):
            data['place'] = ''

        if not data.get('type_appartament'):
            data['appartament'] = ''

        return data
    #
    # def save(self, commit=True, force_insert=False, force_update=False):
    #     f = super(AddressForm, self).save(commit=False)
    #
    #     addr_city = self.cleaned_data.get('city', '')
    #     if addr_city:
    #         addr_city = addr_city.name
    #     addr_street = self.cleaned_data.get('street', '').strip()
    #     addr_house = self.cleaned_data.get('house', '').strip()
    #     addr_appartament = self.cleaned_data.get('appartament', '').strip()
    #     addr_metro = self.cleaned_data.get('metro', '')
    #
    #     if addr_metro:
    #         name = ', '.join([addr_city, addr_metro.name])
    #     else:
    #         name = ', '.join([addr_city, addr_street, addr_house, addr_appartament])
    #     f.name = name
    #     f.save()
    #     return f


class DocumentForm(models.ModelForm):
    class Meta:
        model = OrderDocument
        fields = [
            'question',
            'date_complete',
            'firm',
            'lname_ip',
            'fname_ip',
            'mname_ip',
            'inn',
            'ogrn',
            'count',
            'address',
            # 'theme'
        ]

    question = forms.ModelChoiceField(
        widget=forms.Select(),
        required=True,
        queryset=CallQuestion.objects.filter(~Q(pk=5)),
        label='Вопрос звонка',
        empty_label=None
    )

    theme = forms.ModelChoiceField(
        widget=forms.Select(),
        required=False,
        queryset=CallTheme.objects.all(),
        label='Тема звонка',
        empty_label=None
    )

    def __init__(self, document, *args, **kwargs):
        super(DocumentForm, self).__init__(*args, **kwargs)
        if document:
            self.fields['theme'].initial = document.theme
            self.fields['question'].initial = document.question
            self.fields['count'].initial = 1
        elif self.instance and self.initial:
            qs = CallQuestion.objects.filter(pk__in=self.initial['question'])
            self.initial['theme'] = [q.theme.pk for q in qs]
        else:
            self.fields['count'].initial = 1
            self.fields['question'].initial = 1
            self.fields['theme'].initial = 1

    def save(self, commit=True, force_insert=False, force_update=False):
        f = super(DocumentForm, self).save(commit=False)
        data = self.cleaned_data
        f.theme = data['theme']
        f.question = data['question']
        return f.save()

    class Media:
        js = ('/static/js/questions_update.js',
              '/static/js/document_form.js',
              )
