# -*- coding: utf-8 -*- 
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.db import models

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    followers = models.ManyToManyField('self', related_name='followees', symmetrical=False)
    password = ReadOnlyPasswordHashField(label=("Password"),
                                         help_text=("Raw passwords are not stored, so there is no way to see "
                                                    "this user's password, but you can change the password "
                                                    "using <a href=\"password/\">this form</a>."))


class Post(models.Model):
    author = models.ForeignKey(User, related_name='posts')
    title = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)


class Photo(models.Model):
    post = models.ForeignKey(Post, related_name='photos')
    image = models.ImageField(upload_to="%Y/%m/%d")


class City(models.Model):
    name = models.CharField(max_length=100, verbose_name='Город', unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name', ]
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class AddrObj(models.Model):
    id = models.CharField(max_length=36, verbose_name='Уникальный идентификатор записи', primary_key=True)
    formalname = models.CharField(max_length=120, verbose_name='Формализованное наименование')
    regioncode = models.CharField(max_length=2, verbose_name='Код региона')
    autocode = models.CharField(max_length=1, verbose_name='Код автономии')
    areacode = models.CharField(max_length=3, verbose_name='Код района')
    citycode = models.CharField(max_length=3, verbose_name='Код города')
    ctarcode = models.CharField(max_length=3, verbose_name='Код внутригородского района')
    placecode = models.CharField(max_length=3, verbose_name='Код населенного пункта')
    streetcode = models.CharField(max_length=4, verbose_name='Код улицы')
    extrcode = models.CharField(max_length=4, verbose_name='Код дополнительного адресообразующего элемента')
    sextcode = models.CharField(max_length=3,
                                verbose_name='Код подчиненного дополнительного адресообразующего элемента')
    offname = models.CharField(max_length=120, verbose_name='Официальное наименование')
    postalcode = models.CharField(max_length=6, verbose_name='Почтовый индекс')
    ifnsfl = models.CharField(max_length=4, verbose_name='Код ИФНС ФЛ')
    terrifnsfl = models.CharField(max_length=4, verbose_name='Код территориального участка ИФНС ФЛ')
    ifnsul = models.CharField(max_length=4, verbose_name='Код ИФНС ЮЛ')
    terrifnsul = models.CharField(max_length=4, verbose_name='Код территориального участка ИФНС ЮЛ')
    okato = models.CharField(max_length=11, verbose_name='ОКАТО')
    oktmo = models.CharField(max_length=8, verbose_name='ОКТМО')
    updatedate = models.DateField(verbose_name='Дата  внесения записи')
    shortname = models.CharField(max_length=10, verbose_name='Краткое наименование типа объекта')
    aolevel = models.SmallIntegerField(verbose_name='Уровень адресного объекта ')
    parentguid = models.CharField(max_length=36, verbose_name='Идентификатор объекта родительского объекта')
    aoguid = models.CharField(max_length=36, verbose_name='Глобальный уникальный идентификатор адресного объекта')
    previd = models.CharField(max_length=36,
                              verbose_name='Идентификатор записи связывания с предыдушей исторической записью')
    nextid = models.CharField(max_length=36,
                              verbose_name='Идентификатор записи  связывания с последующей исторической записью')
    code = models.CharField(max_length=17,
                            verbose_name='Код адресного объекта одной строкой с признаком актуальности из КЛАДР 4.0')
    plaincode = models.CharField(max_length=15,
                                 verbose_name='Код адресного объекта из КЛАДР 4.0 одной строкой без признака актуальности (последних двух цифр)')
    actstatus = models.IntegerField(
        verbose_name='Статус актуальности адресного объекта ФИАС. Актуальный адрес на текущую дату. Обычно последняя запись об адресном объекте.')
    centstatus = models.IntegerField(verbose_name='Статус центра')
    operstatus = models.IntegerField(verbose_name='Статус действия над записью – причина появления записи')
    currstatus = models.IntegerField(verbose_name='Статус актуальности КЛАДР 4 (последние две цифры в коде)')
    startdate = models.DateField(verbose_name='Начало действия записи')
    enddate = models.DateField(verbose_name='Окончание действия записи')
    normdoc = models.CharField(max_length=36, verbose_name='Внешний ключ на нормативный документ')

    class Meta:
        db_table = 'd_fias_addrobj'


class SubwayLines(models.Model):
    """
        Линии московского метро
    """

    name = models.CharField(max_length=30, verbose_name='Название линии ')

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'subway_lines'
        ordering = ['name']
        verbose_name = 'Линия метро'
        verbose_name_plural = 'Линии метро'


class SubwayStantions(models.Model):
    """
        Станции московского метро
    """

    name = models.CharField(max_length=30, verbose_name='Название станции')
    line = models.ForeignKey(SubwayLines, related_name='stantions')

    def __str__(self):
        return "%s (%s)" % (self.name, self.line.name)

    class Meta:
        db_table = 'subway_stations'
        ordering = ['name']
        verbose_name = 'Станция метро'
        verbose_name_plural = 'Станции метро'
