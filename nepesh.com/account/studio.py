from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.views import generic
from json_views.views import JSONDetailView

from account.forms import StudioContactsUpdateForm, StudioDescUpdateForm, BikeForm, TariffForm
from account.mixins import JSONMixin
from images.views import TmpFileStorage
from velos.models import Studio, Bike, BikePhoto, AdditionalService, Characteristic, Tariff, StudioPhotos


class ProfileMixin(object):
    def __init__(self):
        super(ProfileMixin, self).__init__()

    def get_context_data(self, **kwargs):
        if self.kwargs.get('pk'):
            self.studio = Studio.objects.get(pk=self.kwargs.get('pk'))

        c = super(ProfileMixin, self).get_context_data(**kwargs)
        c['studios'] = Studio.objects.filter(pk__in=self.request.user.get_studios())
        if self.kwargs.get('pk'):
            c['studio'] = self.studio
        return c


class BikesMixin(object):
    def get_context_data(self, **kwargs):
        c = super(BikesMixin, self).get_context_data(**kwargs)
        c['bikes'] = self.studio.bikes.all()
        c['tariffs'] = Tariff.objects.filter(user=self.request.user)
        c['tariff_form'] = TariffForm()
        return c


class StudioProfileView(LoginRequiredMixin, ProfileMixin, BikesMixin, generic.CreateView):
    template_name = 'account/studio/velos.html'
    model = Bike
    form_class = BikeForm

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save(commit=False)
        self.object.studio_id = self.kwargs.get('pk')
        self.object.save()

        return super(StudioProfileView, self).form_valid(form)

    def get_success_url(self):
        return reverse('bike_edit', kwargs=dict(pk=self.kwargs.get('pk'), bike_id=self.object.pk))

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            studios = request.user.get_studios()
        else:
            return redirect('login')
        if not kwargs.get('pk', False) and studios:
            return redirect('studio_profile', pk=studios[0])
        elif not studios:
            return redirect('studio_profile_add')
        else:
            return super(StudioProfileView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        c = super(StudioProfileView, self).get_context_data(**kwargs)
        c['bike_form_show'] = False
        return c


class StudioProfileBikeEditView(LoginRequiredMixin, ProfileMixin, BikesMixin, generic.UpdateView):
    template_name = 'account/studio/velos.html'
    model = Bike
    form_class = BikeForm
    pk_url_kwarg = 'bike_id'

    def get_success_url(self):
        return reverse('bike_edit', kwargs=self.kwargs)

    def get_context_data(self, **kwargs):
        c = super(StudioProfileBikeEditView, self).get_context_data(**kwargs)
        c['bike_form_show'] = True
        c['chars'] = Characteristic.objects.all()
        c['current_chars'] = [str(x.pk) for x in self.object.chars.all()]
        c['form'].fields['studio'].queryset = c['studios']

        return c

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        for photo in self.request.POST.getlist('photo[]', []):
            try:
                tmp_photo = TmpFileStorage.objects.get(pk=photo)
                bike_photo = BikePhoto()
                bike_photo.image = tmp_photo.attach
                bike_photo.bike = self.object
                bike_photo.save()
            except Exception as e:
                pass
        return super(StudioProfileBikeEditView, self).form_valid(form)


class StudioProfileAdd(LoginRequiredMixin, ProfileMixin, generic.CreateView):
    template_name = 'account/studio/add.html'
    model = Studio
    fields = ('name',)

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """

        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()

        try:
            g = Group.objects.get(name='studios')
            self.request.user.groups.add(g)
            self.request.user.save()
        except:
            pass

        return super(StudioProfileAdd, self).form_valid(form)

    def get_success_url(self):
        return reverse('studio_profile', kwargs=dict(pk=self.object.pk))


class StudioProfileDescView(LoginRequiredMixin, ProfileMixin, generic.UpdateView):
    model = Studio
    form_class = StudioDescUpdateForm
    template_name = 'account/studio/desc.html'

    def get_context_data(self, **kwargs):
        c = super(StudioProfileDescView, self).get_context_data(**kwargs)
        c['services'] = AdditionalService.objects.all()
        c['current_services'] = [str(x.pk) for x in self.object.additional.all()]
        return c

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        for photo in self.request.POST.getlist('photo[]', []):
            try:
                tmp_photo = TmpFileStorage.objects.get(pk=photo)  # TODO move file to
                studio_photo = StudioPhotos()
                studio_photo.image = tmp_photo.attach
                studio_photo.studio = self.object
                studio_photo.save()
            except Exception as e:
                pass
        return super(StudioProfileDescView, self).form_valid(form)

    def get_success_url(self):
        return reverse('studio_profile_desc', kwargs=dict(pk=self.object.pk))


class StudioProfileContactsView(LoginRequiredMixin, ProfileMixin, generic.UpdateView):
    model = Studio
    form_class = StudioContactsUpdateForm
    template_name = 'account/studio/contacts.html'


class BikePhotoUpload(LoginRequiredMixin, generic.CreateView):
    model = BikePhoto
    fields = ('image', )


class TariffCreateView(JSONMixin, generic.CreateView):
    model = Tariff
    form_class = TariffForm

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()

        return super(TariffCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('tariff', kwargs=dict(pk=self.object.pk))


class TariffUpdateView(JSONMixin, generic.UpdateView):
    model = Tariff
    form_class = TariffForm

    def get_success_url(self):
        return reverse('tariff', kwargs=dict(pk=self.object.pk))


class TariffJSON(JSONDetailView):
    model = Tariff
