# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    replaces = [('lending_proc', '0001_initial'), ('lending_proc', '0002_auto_20160110_0820'), ('lending_proc', '0003_auto_20160110_0825')]

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ApplicationUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_name', models.CharField(max_length=255, verbose_name='Имя')),
                ('email', models.EmailField(max_length=254)),
                ('phone', models.CharField(max_length=17, verbose_name='Телефон')),
                ('ip_address', models.IPAddressField()),
                ('type_application', models.IntegerField(default=1, choices=[(1, 'Обычная'), (2, 'Бесплатный период'), (3, 'Звонок'), (4, 'Обслуживание')])),
                ('calculation_type', models.IntegerField(blank=True, null=True, verbose_name='Тип обслуживания', choices=[(1, 'УСН 6%'), (2, 'УСН 15%'), (3, 'ОСНО')])),
                ('calculation_documents', models.IntegerField(null=True, verbose_name='Количество документов', blank=True)),
                ('calculation_employees', models.IntegerField(null=True, verbose_name='Количество сотрудников', blank=True)),
                ('calculation_cost', models.IntegerField(null=True, verbose_name='Стоимость', blank=True)),
                ('created', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-created'],
                'db_table': 'application_user',
                'verbose_name': 'Заявка пользователя',
                'verbose_name_plural': 'Заявки пользователей',
            },
        ),
        migrations.CreateModel(
            name='Clients',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cookie_track', models.CharField(max_length=10, verbose_name='cookie')),
                ('date', models.DateTimeField()),
            ],
            options={
                'verbose_name': 'Отслеживамые клиенты',
                'verbose_name_plural': 'Отслеживамые клиенты',
            },
        ),
        migrations.CreateModel(
            name='ClientsTrack',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=60, verbose_name='url')),
                ('date', models.DateTimeField()),
                ('client', models.ForeignKey(verbose_name='клиент', to='lending_proc.Clients')),
            ],
            options={
                'verbose_name': 'Лог посешения клиентами лендингов',
                'verbose_name_plural': 'Лог посешения клиентами лендингов',
            },
        ),
        migrations.CreateModel(
            name='Companes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20, verbose_name='Назвние рекламной компании')),
            ],
            options={
                'verbose_name': 'Рекламные компании',
                'verbose_name_plural': 'Рекламные компании',
            },
        ),
        migrations.CreateModel(
            name='GetParametrs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('get_parametrs', models.TextField()),
                ('created', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'users_get_parametrs',
            },
        ),
        migrations.CreateModel(
            name='Lendings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20, verbose_name='Назвние лендинга компании')),
            ],
            options={
                'verbose_name': 'Лендинги',
                'verbose_name_plural': 'Лендинги',
            },
        ),
        migrations.CreateModel(
            name='ManagerEmail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254, verbose_name='Email менеджера')),
                ('active', models.BooleanField(verbose_name='Активный')),
            ],
            options={
                'verbose_name': 'Email менеджеров',
                'verbose_name_plural': 'Email менеджеров',
            },
        ),
        migrations.CreateModel(
            name='PfoneIncomeLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone_to', phonenumber_field.modelfields.PhoneNumberField(max_length=128, verbose_name='Номер исходящий')),
                ('phone_from', phonenumber_field.modelfields.PhoneNumberField(max_length=128, verbose_name='Номер входящий')),
                ('cookie', models.CharField(max_length=10, verbose_name='cookie')),
                ('income_date', models.DateTimeField(verbose_name='Время звонка')),
                ('client', models.ForeignKey(verbose_name='клиент', to='lending_proc.Clients', null=True)),
                ('lending', models.ForeignKey(verbose_name='Лендинг', to='lending_proc.Lendings', null=True)),
            ],
            options={
                'verbose_name': 'Лог звонков',
                'verbose_name_plural': 'Лог звонков',
            },
        ),
        migrations.CreateModel(
            name='Pfones',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', phonenumber_field.modelfields.PhoneNumberField(unique=True, max_length=128, verbose_name='Номер')),
                ('expires_date', models.DateTimeField(null=True, editable=False, blank=True)),
                ('active', models.BooleanField(default=True, editable=False)),
                ('cookie', models.CharField(max_length=10, editable=False, blank=True)),
                ('is_default', models.BooleanField(default=False, verbose_name='Резервный')),
                ('compane', models.ForeignKey(verbose_name='Рекламная компания', to='lending_proc.Companes')),
                ('lending', models.ForeignKey(verbose_name='Лендинг', to='lending_proc.Lendings')),
            ],
            options={
                'verbose_name': 'Номера',
                'verbose_name_plural': 'Номера',
            },
        ),
        migrations.CreateModel(
            name='PfoneShown',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shown_date', models.DateTimeField(verbose_name='Время показа')),
                ('cookie', models.CharField(max_length=10, verbose_name='cookie')),
                ('active', models.BooleanField(default=True, editable=False)),
                ('client', models.ForeignKey(verbose_name='клиент', to='lending_proc.Clients', null=True)),
                ('phone', models.ForeignKey(verbose_name='Номер', to='lending_proc.Pfones')),
            ],
            options={
                'verbose_name': 'Лог индификаторов',
                'verbose_name_plural': 'Лог индификаторов',
            },
        ),
        migrations.AddField(
            model_name='getparametrs',
            name='lending',
            field=models.ForeignKey(verbose_name='Лендинг', to='lending_proc.Lendings'),
        ),
        migrations.AddField(
            model_name='clientstrack',
            name='lending',
            field=models.ForeignKey(verbose_name='Лендинг', to='lending_proc.Lendings'),
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='client',
            field=models.ForeignKey(verbose_name='клиент', to='lending_proc.Clients', null=True),
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='lending',
            field=models.ForeignKey(verbose_name='Лендинг', to='lending_proc.Lendings', null=True),
        ),
    ]
