from django import forms
from cupidus.home.models import UserProfile,Geo_sp_Cities,Phones_codes
from django.contrib.auth.models import User
import json

class RegisterForm(forms.Form):
	name  = forms.CharField(max_length=100,required=True)
	phone = forms.CharField(max_length=100,required=True)
	city  = forms.CharField(required=True)
	email = forms.EmailField(max_length=100,required=True)
	vk    = forms.CharField(max_length=100,required=False)
	face  = forms.CharField(max_length=100,required=False)
	ok    = forms.CharField(max_length=100,required=False)
	linkedin = forms.CharField(max_length=100,required=False)
	check = forms.BooleanField(required=True)
	phonecode = forms.BooleanField(required=True)

	def clean_name(self,name=None):
		name = name or self.cleaned_data['name']
		if User.objects.filter(username=name):
			raise forms.ValidationError("name already exists")
		return name

	def clean_phone(self,phone=None):
		phone = phone or self.cleaned_data['phone']
		if UserProfile.objects.filter(phone='+7'+phone).filter(phone_active=True):
			raise forms.ValidationError("phone already exists")
		return phone

	def clean_email(self,email=None):
		email = email or self.cleaned_data['email']
		if User.objects.filter(email=email).filter(profile__email_active=True):
			raise forms.ValidationError("email already exists")
		return email

	def clean_city(self):
		city = self.cleaned_data.get('city')
		city=int(city) if city.isdigit() else None
		if not city:
			raise forms.ValidationError("city id not int")
		city=Geo_sp_Cities.objects.filter(id=city)
		if not city:
			raise forms.ValidationError("city not in db")
		return city[0]



