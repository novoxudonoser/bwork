from django.conf import urls
from django.conf.urls import include
from django.contrib.auth.views import login

from account.studio import StudioProfileView, StudioProfileAdd, StudioProfileDescView, StudioProfileContactsView, \
    StudioProfileBikeEditView, BikePhotoUpload, TariffCreateView, TariffJSON, TariffUpdateView
from .views import RegisterView, ProfileView, ProfilePasswordView, ProfileDeleteView



urlpatterns = [
    urls.url(r'^login/?$', login, {'template_name': 'account/login.html'}, name='login'),
    urls.url(r'^register/?$', RegisterView.as_view(), name='register'),

    urls.url(r'^profile/?$', ProfileView.as_view(), name='profile'),
    urls.url(r'^profile/personal/?$', ProfileView.as_view(), name='profile_personal'),
    urls.url(r'^profile/password/?$', ProfilePasswordView.as_view(), name='profile_password'),
    urls.url(r'^profile/delete/?$', ProfileDeleteView.as_view(), name='profile_delete'),

    urls.url(r'^studio/?$', StudioProfileView.as_view(), name='studio_profile'),
    urls.url(r'^studio/(?P<pk>\d+)/?$', StudioProfileView.as_view(), name='studio_profile'),
    urls.url(r'^studio/(?P<pk>\d+)/bike/(?P<bike_id>\d+)/edit/?$', StudioProfileBikeEditView.as_view(), name='bike_edit'),

    urls.url(r'^tariff/create/?$', TariffCreateView.as_view(), name='create_tariff'),
    urls.url(r'^tariff/(?P<pk>\d+)/?$', TariffJSON.as_view(), name='tariff'),
    urls.url(r'^tariff/update/(?P<pk>\d+)/?$', TariffUpdateView.as_view(), name='update_tariff'),

    urls.url(r'^studio/add/?$', StudioProfileAdd.as_view(), name='studio_profile_add'),
    urls.url(r'^studio/(?P<pk>\d+)/desc/?$', StudioProfileDescView.as_view(), name='studio_profile_desc'),
    urls.url(r'^studio/(?P<pk>\d+)/contacts/?$', StudioProfileContactsView.as_view(), name='studio_profile_contacts'),
]
