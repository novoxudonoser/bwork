# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Call',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.IntegerField(verbose_name='Тип звонка', choices=[(1, 'Входящий'), (2, 'Исходящий')], default=1)),
                ('comment', models.TextField(null=True, verbose_name='Комментарий оператора', blank=True)),
                ('number', models.CharField(null=True, max_length=255, verbose_name='Номер заказа', blank=True)),
                ('date_pushed', models.DateTimeField(verbose_name='Дата нажатия кнопки')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('date_updated', models.DateTimeField(verbose_name='Дата обновления', auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='CallQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Вопрос звонка')),
                ('sort', models.PositiveSmallIntegerField(null=True, verbose_name='Порядок сортировки', blank=True)),
            ],
            options={
                'verbose_name': 'Вопрос звонка',
                'ordering': ['sort'],
                'verbose_name_plural': 'Вопросы звонков',
            },
        ),
        migrations.CreateModel(
            name='CallSource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название источника', unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='CallTheme',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Тема звонка', unique=True)),
                ('sort', models.PositiveSmallIntegerField(null=True, verbose_name='Порядок сортировки', blank=True)),
            ],
            options={
                'verbose_name': 'Тема звонка',
                'ordering': ['sort'],
                'verbose_name_plural': 'Темы звонков',
            },
        ),
        migrations.AddField(
            model_name='callquestion',
            name='theme',
            field=models.ForeignKey(null=True, default=None, verbose_name='Тема вопроса', to='calls.CallTheme'),
        ),
    ]
