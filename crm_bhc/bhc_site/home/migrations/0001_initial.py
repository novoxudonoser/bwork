# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import modelcluster.fields
import django.db.models.deletion
import wagtail.wagtailcore.fields
import wagtail.wagtailimages.blocks
import wagtail.wagtailcore.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('wagtaildocs', '0003_add_verbose_names'),
        ('wagtailimages', '0006_add_verbose_names'),
        ('wagtailcore', '0001_squashed_0016_change_page_url_path_to_text_field'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('show_rigth_news', models.BooleanField(verbose_name='Показывать новости в правом блоке', default=True)),
                ('show_rigth_partners', models.BooleanField(verbose_name='Показывать партнеров в правом блоке', default=True)),
            ],
            options={
                'verbose_name': 'Страница контактов',
                'verbose_name_plural': 'Страницы контактов',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='ContactPageOfficePlacement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(null=True, editable=False, blank=True)),
            ],
            options={
                'verbose_name': 'Office Placement',
                'verbose_name_plural': 'Office Placements',
            },
        ),
        migrations.CreateModel(
            name='HomePage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('banner_image', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', blank=True, verbose_name='Главный баннер', to='wagtailimages.Image')),
            ],
            options={
                'verbose_name': 'Главная страница',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='HomeSnippets',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=255, verbose_name='Описание', help_text='Не надо редактировать это')),
                ('text1', models.CharField(null=True, max_length=255, verbose_name='Текст 1', blank=True)),
                ('text2', models.CharField(null=True, max_length=255, verbose_name='Текст 2', blank=True)),
                ('text3', models.CharField(null=True, max_length=255, verbose_name='Текст 3', blank=True)),
                ('button_text', models.CharField(null=True, max_length=255, verbose_name='Текст на кнопки', blank=True)),
                ('url', models.URLField(null=True, verbose_name='URL Адрес', blank=True)),
            ],
            options={
                'verbose_name': 'Данные главной страницы',
                'verbose_name_plural': 'Данные главной страницы',
            },
        ),
        migrations.CreateModel(
            name='MenuSnippet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Заголовок')),
                ('anons', wagtail.wagtailcore.fields.RichTextField(verbose_name='Анонс', help_text='Выводится на главной под баннером')),
                ('on_main', models.BooleanField(verbose_name='Выводить на главной под баннером', default=False)),
                ('on_menu', models.BooleanField(verbose_name='Выводить в главном меню', default=False)),
            ],
            options={
                'verbose_name': 'Пункт главного меню',
                'verbose_name_plural': 'Пункты главного меню',
            },
        ),
        migrations.CreateModel(
            name='NewsCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'Категория',
                'verbose_name_plural': 'Категории новостей и услуг',
            },
        ),
        migrations.CreateModel(
            name='NewsListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('show_rigth_news', models.BooleanField(verbose_name='Показывать новости в правом блоке', default=True)),
                ('show_rigth_partners', models.BooleanField(verbose_name='Показывать партнеров в правом блоке', default=True)),
            ],
            options={
                'verbose_name': 'Страница списка новостей',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='NewsPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('show_rigth_news', models.BooleanField(verbose_name='Показывать новости в правом блоке', default=True)),
                ('show_rigth_partners', models.BooleanField(verbose_name='Показывать партнеров в правом блоке', default=True)),
                ('short_title', models.CharField(max_length=255)),
                ('anons', wagtail.wagtailcore.fields.RichTextField()),
                ('content', wagtail.wagtailcore.fields.StreamField((('image', wagtail.wagtailimages.blocks.ImageChooserBlock()), ('paragraph', wagtail.wagtailcore.blocks.RichTextBlock())))),
                ('category', models.ForeignKey(null=True, related_name='category_news', default=None, blank=True, to='home.NewsCategory')),
            ],
            options={
                'verbose_name': 'Новость',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='NewsPageRelatedLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(null=True, editable=False, blank=True)),
                ('link_external', models.URLField(verbose_name='External link', blank=True)),
                ('title', models.CharField(max_length=255, help_text='Link title')),
                ('link_document', models.ForeignKey(null=True, related_name='+', blank=True, to='wagtaildocs.Document')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OfficeSnippet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('address', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=255)),
                ('time', models.CharField(max_length=255)),
                ('map', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'Офис',
                'verbose_name_plural': 'Офисы',
            },
        ),
        migrations.CreateModel(
            name='PartnerSnippet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('partners', models.BooleanField(verbose_name="В блок на главной 'Наши партнеры'")),
                ('trust', models.BooleanField(verbose_name="В блок на главной 'Нам доверяют'")),
                ('right_block', models.BooleanField(verbose_name='В правый блок на страницах контента')),
                ('image', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', blank=True, verbose_name='Логотип партнера', to='wagtailimages.Image')),
            ],
            options={
                'verbose_name': 'Партнер',
                'verbose_name_plural': 'Партнеры',
            },
        ),
        migrations.CreateModel(
            name='ServiceListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('show_rigth_news', models.BooleanField(verbose_name='Показывать новости в правом блоке', default=True)),
                ('show_rigth_partners', models.BooleanField(verbose_name='Показывать партнеров в правом блоке', default=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='ServicePage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('show_rigth_news', models.BooleanField(verbose_name='Показывать новости в правом блоке', default=True)),
                ('show_rigth_partners', models.BooleanField(verbose_name='Показывать партнеров в правом блоке', default=True)),
                ('short_title', models.CharField(max_length=255)),
                ('anons', wagtail.wagtailcore.fields.RichTextField()),
                ('content', wagtail.wagtailcore.fields.StreamField((('image', wagtail.wagtailimages.blocks.ImageChooserBlock()), ('paragraph', wagtail.wagtailcore.blocks.RichTextBlock())))),
                ('category', models.ForeignKey(null=True, related_name='category_services', default=None, blank=True, to='home.NewsCategory')),
            ],
            options={
                'verbose_name': 'Услуга',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='ServicePageRelatedLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(null=True, editable=False, blank=True)),
                ('link_external', models.URLField(verbose_name='External link', blank=True)),
                ('title', models.CharField(max_length=255, help_text='Link title')),
                ('link_document', models.ForeignKey(null=True, related_name='+', blank=True, to='wagtaildocs.Document')),
                ('link_page', models.ForeignKey(null=True, related_name='+', blank=True, to='wagtailcore.Page')),
                ('page', modelcluster.fields.ParentalKey(related_name='related_links', to='home.ServicePage')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SocialSnippet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link_external', models.URLField(null=True, verbose_name='External link', help_text='Set an external link if you want the link to point somewhere outside the CMS.', blank=True)),
                ('image', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', blank=True, verbose_name='Главный баннер', to='wagtailimages.Image')),
            ],
            options={
                'verbose_name': 'Социальная сеть',
                'verbose_name_plural': 'Социальные сети',
            },
        ),
        migrations.AddField(
            model_name='newspagerelatedlink',
            name='link_page',
            field=models.ForeignKey(null=True, related_name='+', blank=True, to='wagtailcore.Page'),
        ),
        migrations.AddField(
            model_name='newspagerelatedlink',
            name='page',
            field=modelcluster.fields.ParentalKey(related_name='related_links', to='home.NewsPage'),
        ),
        migrations.AddField(
            model_name='menusnippet',
            name='link_page',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, help_text='Выберите страницу', blank=True, related_name='+', to='wagtailcore.Page'),
        ),
        migrations.AddField(
            model_name='homepage',
            name='news',
            field=models.ManyToManyField(to='home.NewsPage'),
        ),
        migrations.AddField(
            model_name='homepage',
            name='texts',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', blank=True, verbose_name='Тексты', to='home.HomeSnippets'),
        ),
        migrations.AddField(
            model_name='contactpageofficeplacement',
            name='office',
            field=models.ForeignKey(related_name='+', to='home.OfficeSnippet'),
        ),
        migrations.AddField(
            model_name='contactpageofficeplacement',
            name='page',
            field=modelcluster.fields.ParentalKey(related_name='office_placements', to='home.ContactPage'),
        ),
    ]
