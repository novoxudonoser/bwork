from django.db.models import Q
from crm.orders.models import OrderDocument

__author__ = 'PekopT'


class RouteQuerysetMixin(object):
    def get_queryset(self):
        documents = OrderDocument.objects.filter(document_order__client__isnull=False,
                                                 document_order__delivery_type=1,
                                                 ).filter(~Q(address__name=''))
        route = self.request.POST.get('route', False)
        if route and route != '':
            if route == '-2':
                documents = documents.filter(route__isnull=True)
            else:
                documents = documents.filter(Q(route=route) | Q(route__isnull=True))
        return documents
