from django.shortcuts import redirect, render_to_response

from social.pipeline.partial import partial


@partial
def add_email_for_user(backend, details, response, is_new=False, *args, **kwargs):
    data = backend.strategy.request_data()
    if details.get('email') is None:
        return render_to_response('account/add_email_form.html')
    else:
        return {'email': details.get('email')}

@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    if kwargs.get('ajax') or user and user.email:
        return
    elif is_new and not details.get('email'):
        email = strategy.request_data().get('email')
        if email:
            details['email'] = email
        else:
            return redirect('require_email')