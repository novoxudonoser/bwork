from django.conf.urls import patterns, include, url
from cupidus.home.views import IndexView,RegisterHendler,UniqueHendler,CitiesList,PhoneCodeCheckHendler,PhoneCodeSendHendler

urlpatterns = patterns('',
    url(r'^/?$', IndexView.as_view()),
    url(r'^register', RegisterHendler.as_view(),name='register'),
    url(r'^check_unique', UniqueHendler.as_view(),name='check_unique'),
    url(r'^profile', RegisterHendler.as_view(),name='profile'),
    url(r'^citieslist', CitiesList.as_view(),name='citieslist'),
    url(r'^codephonecheck', PhoneCodeCheckHendler.as_view(),name='registerphonecode'),
    url(r'^codephonesend', PhoneCodeSendHendler.as_view(),name='registersendphonesms'),
	)