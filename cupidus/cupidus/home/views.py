from django.views.generic import TemplateView, View
from django.db.models import Q
from django.db import transaction
from django import forms
from django.http import JsonResponse,HttpResponse
from django.contrib.auth import authenticate, login

from cupidus.home.forms import RegisterForm
from cupidus.smsaero.custom_db_mailer_backend import send_db_smsaero

from cupidus.home.models import Contacts_sp_Type, UserProfile, User_Contacts, Geo_sp_Cities, Geo_Location, Phones_codes
from django.contrib.auth.models import User

import random
import string
import json
from datetime import datetime
import pytz

from settings import CUP_REGISTER_SENDSMSREGPARAMS,REG_RESEND_SMS_INTERVAL,REG_RESEND_SMS_TIMEOUT,CUP_REGISTER_SEND_SMS_CODE

class IndexView(TemplateView):
	template_name = "home/index.html"

	def get_context_data(self, **kwargs):
			context = super(IndexView, self).get_context_data(**kwargs)
			context['register_form']=RegisterForm()
			return context

class UniqueHendler(View):
	def get(self, request, **kwargs):
		if 'name' in request.GET:
			try:
				RegisterForm.clean_name(None,name=request.GET.get('name', ''))
			except forms.ValidationError:
				return HttpResponse("false")
			else:
				return HttpResponse("true")
		if 'phone' in request.GET:
			try:
				RegisterForm.clean_phone(None,phone=request.GET.get('phone', ''))
			except forms.ValidationError:
				return HttpResponse("false")
			else:
				return HttpResponse("true")
		if 'email' in request.GET:
			try:
				RegisterForm.clean_email(None,email=request.GET.get('email', ''))
			except forms.ValidationError:
				return HttpResponse("false")
			else:
				return HttpResponse("true")


class RegisterHendler(View):
	def post(self, request, **kwargs):
		if request.is_ajax():
			return self.ajax(request)
		return super(RegisterHendler, self).post(request, **kwargs)

	def ajax(self, request):
		valid=False
		with transaction.atomic():
			form = RegisterForm(data=json.loads(request.body.decode('utf-8')))
			if form.is_valid():
				valid=True
				name=form.cleaned_data.get('name')
				phone='+7'+form.cleaned_data.get('phone')
				email=form.cleaned_data.get('email')
				city=form.cleaned_data.get('city')
				geo=Geo_Location.objects.create(city=city,country=city.country,arya=None)
				password=''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(8))
				user=User.objects.create_user(
					username=name,
					email=email,
					password=password,
					is_active=True,
				)
				user_profile=UserProfile.objects.create(
					phone_active=True,
					user=user,
					phone=phone,
					geo=geo,
					geo_at_register=geo
				)

		if not valid:
				return JsonResponse({'status':'error','errors':{k:v for k,v in form.errors.items()}})

		for fild_name in form.cleaned_data.keys():
			if not fild_name in ('name','phone','email','city'):
				contact_type=Contacts_sp_Type.objects.filter(key=fild_name)
				if contact_type and form.cleaned_data[fild_name]:
					User_Contacts.objects.create(user=user,contact_type=contact_type[0],value=form.cleaned_data[fild_name])

		if CUP_REGISTER_SENDSMSREGPARAMS:
			send_db_smsaero(slug='regd',recipient=str(user_profile.phone),**{'password':password,'email':user.email})

		user.backend = "django.contrib.auth.backends.ModelBackend"
		login(request, user)

		return JsonResponse({'status':'ok'})

class CitiesList(View):
	def get(self, request):
		cities=[{'id':city.id,'name':city.name} for city in Geo_sp_Cities.objects.all().order_by('-points')]
		return JsonResponse(cities,safe=False)

class PhoneCodeSendHendler(View):
	def post(self, request, **kwargs):
		if request.is_ajax():
			return self.ajax(request)
		return super(PhoneCodeSendHendler, self).post(request, **kwargs)

	def ajax(self, request):
		data=json.loads(request.body.decode('utf-8'))
		phone=data.get('phone')
		phone='+7'+phone if phone else None
		if UserProfile.objects.filter(phone=phone,phone_active=True):
			return JsonResponse({'status':'error','error':'phone alredy activated'})
		if not phone:
			return JsonResponse({'status':'error','error':'no phone in data'})
		try:
			lastphonecode=Phones_codes.objects.filter(phone=phone).latest('date')
		except:
			lastphonecode=None
		if lastphonecode:
			if (datetime.utcnow().replace(tzinfo=pytz.utc) - lastphonecode.date).seconds < REG_RESEND_SMS_INTERVAL:
				return JsonResponse({'status':'error','error':'too early for sending code'})
		code=''.join(random.SystemRandom().choice(string.digits) for _ in range(6))
		Phones_codes.objects.create(phone=phone,code=code)
		if CUP_REGISTER_SEND_SMS_CODE:
			send_db_smsaero(slug='rcode',recipient=phone,**{'code':code})
		return JsonResponse({'status':'ok'})





class PhoneCodeCheckHendler(View):
	def post(self, request, **kwargs):
		if request.is_ajax():
			return self.ajax(request)
		return super(PhoneCodeCheckHendler, self).post(request, **kwargs)

	def ajax(self, request):
		data=json.loads(request.body.decode('utf-8'))
		phone=data.get('phone')
		phone='+7'+phone if phone else None
		code=data.get('code')
		try:
			phonecode=Phones_codes.objects.get(phone=phone,code=code)
		except:
			phonecode=None
		if not phonecode:
			return JsonResponse({'status':'error','error':'no record with this code and phone'})
		if (datetime.utcnow().replace(tzinfo=pytz.utc) - phonecode.date).seconds > REG_RESEND_SMS_TIMEOUT:
			return JsonResponse({'status':'error','error':'code timeout'})
		return JsonResponse({'status':'ok'})
