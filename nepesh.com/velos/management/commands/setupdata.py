from django.contrib.auth.models import Group
from django.core.management import BaseCommand

from account.models import VeloUser
from pages.models import SiteMenu, SocialMenu

from velos.models import  BikeModels, BikeMarks, BikeColors, BikeWeelSizes, BikeSpeeds, BikeFrames, BikeSuspensions, Characteristic
from velos.models import  BikeSizes, BikeGender, BikeTypes, BikeSpeeds, BikeFrames, BikePurposes, BikeTypes, AdditionalService

try:
    from local_settings import *
except ImportError:
    pass


class Command(BaseCommand):
    help = 'Setup test data'

    def handle(self, *args, **options):
        VeloUser.objects.all().delete()
        u = VeloUser.objects.create_superuser('admin@example.com', 123)
        g = Group.objects.create(name='studios')
        u.groups.add(g)
        u.save()

        SiteMenu.objects.create(name="FAQ", link='012', show_header=True, show_footer=True)
        SiteMenu.objects.create(name="Помощь", link='123', show_header=True, show_footer=True)
        SiteMenu.objects.create(name="Безопасность", link='234', show_header=True, show_footer=True)
        SiteMenu.objects.create(name="Соглашение", link='345', show_header=True, show_footer=True)
        SiteMenu.objects.create(name="Полезное", link='456', show_header=True, show_footer=True)

        SocialMenu.objects.create(name="VK", link='012', show_header=True, show_footer=True)
        SocialMenu.objects.create(name="Fb", link='123', show_header=True, show_footer=True)
        SocialMenu.objects.create(name="Tw", link='234', show_header=True, show_footer=True)
        SocialMenu.objects.create(name="G+", link='345', show_header=True, show_footer=True)
        SocialMenu.objects.create(name="Yt", link='456', show_header=True, show_footer=True)



        # -------------- sp bikes -----------------------
        BikeColors.objects.create(color='#fff')
        BikeColors.objects.create(color='#86b5f5')
        BikeColors.objects.create(color='#4d5a73')
        BikeColors.objects.create(color='#65c886')
        BikeColors.objects.create(color='#ffdc7c')
        BikeColors.objects.create(color='#fd3f5a')

        BikeWeelSizes.objects.create(num=24)
        BikeWeelSizes.objects.create(num=26)
        BikeWeelSizes.objects.create(num=28)
        BikeWeelSizes.objects.create(num=29)
        BikeWeelSizes.objects.create(num=30)
        BikeWeelSizes.objects.create(num=32)

        BikeSpeeds.objects.create(num=1)
        BikeSpeeds.objects.create(num=18)
        BikeSpeeds.objects.create(num=21)
        BikeSpeeds.objects.create(num=24)
        BikeSpeeds.objects.create(num=27)
        BikeSpeeds.objects.create(num=30)

        BikeFrames.objects.create(name='стальные')
        BikeFrames.objects.create(name='алюминиевые')
        BikeFrames.objects.create(name='карбоновые')

        BikeSuspensions.objects.create(name='без амортизаторов')
        BikeSuspensions.objects.create(name='с амортизационной вилкой')
        BikeSuspensions.objects.create(name='двухподвесы')

        AdditionalService.objects.create(name='Рукомойник')
        AdditionalService.objects.create(name='Туалет')
        AdditionalService.objects.create(name='Питьевая вода')
        AdditionalService.objects.create(name='Камера хранения')

        BikeSizes.objects.create(name='детские')
        BikeSizes.objects.create(name='подростковые')
        BikeSizes.objects.create(name='S')
        BikeSizes.objects.create(name='M')
        BikeSizes.objects.create(name='L')
        BikeSizes.objects.create(name='XL')

        BikeTypes.objects.create(name='прогулочные')
        BikeTypes.objects.create(name='спортивные')
        BikeTypes.objects.create(name='экстремальные')

        BikeGender.objects.create(name='Мужской')
        BikeGender.objects.create(name='Женский')
        BikeGender.objects.create(name='Подростковый')
        BikeGender.objects.create(name='Детский')                

        BikePurposes.objects.create(name='горные')
        BikePurposes.objects.create(name='ретро')
        BikePurposes.objects.create(name='дорожные')
        BikePurposes.objects.create(name='шоссейные')
        BikePurposes.objects.create(name='стрит (экстремальные)')
        BikePurposes.objects.create(name='тандем')
        BikePurposes.objects.create(name='fixed')
        BikePurposes.objects.create(name='необычный велосипед')




        # -------------- sp bikes -----------------------

        self.stdout.write(self.style.SUCCESS('Successfully'))
