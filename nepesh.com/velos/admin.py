from django.contrib import admin

from velos.models import Studio, Bike, AdditionalService, StudioPhotos,Tariff


class StudioPhotosInline(admin.TabularInline):
    model = StudioPhotos
    extra = 1


class StudioAdmin(admin.ModelAdmin):
    inlines = (StudioPhotosInline,)


admin.site.register(Studio, StudioAdmin)
admin.site.register(Bike)
admin.site.register(AdditionalService)
admin.site.register(Tariff)

from velos.models import BikeModels,BikeMarks,BikeColors,BikeWeelSizes,BikeSizes,BikeGender,BikeTypes
from velos.models import BikeSpeeds,BikeFrames,BikeSuspensions,BikePurposes,BikePhoto,Arya,City

admin.site.register(BikeModels)
admin.site.register(BikeMarks)
admin.site.register(BikeColors)
admin.site.register(BikeWeelSizes)
admin.site.register(BikeSizes)
admin.site.register(BikeGender)
admin.site.register(BikeTypes)
admin.site.register(BikeSpeeds)
admin.site.register(BikeFrames)
admin.site.register(BikeSuspensions)
admin.site.register(BikePurposes)
admin.site.register(BikePhoto)
admin.site.register(Arya)
admin.site.register(City)