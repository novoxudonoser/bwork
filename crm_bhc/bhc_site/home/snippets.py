# -*- coding: utf-8 -*- 
from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel
from wagtail.wagtailsnippets.models import register_snippet


@register_snippet
class HomeSnippets(models.Model):
    text = models.CharField(max_length=255, verbose_name="Описание", help_text="Не надо редактировать это")
    text1 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Текст 1")
    text2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Текст 2")
    text3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Текст 3")
    button_text = models.CharField(max_length=255, null=True, blank=True, verbose_name="Текст на кнопки")
    url = models.URLField(null=True, blank=True, verbose_name="URL Адрес")

    panels = [
        FieldPanel('text'),
        FieldPanel('text1'),
        FieldPanel('text2'),
        FieldPanel('text3'),
        FieldPanel('button_text'),
        FieldPanel('url'),
    ]

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = "Данные главной страницы"
        verbose_name_plural = "Данные главной страницы"


@register_snippet
class MenuSnippet(models.Model):
    title = models.CharField(max_length=255, verbose_name='Заголовок')
    anons = RichTextField(verbose_name='Анонс', help_text='Выводится на главной под баннером')
    on_main = models.BooleanField(default=False, verbose_name='Выводить на главной под баннером')
    on_menu = models.BooleanField(default=False, verbose_name='Выводить в главном меню')
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        on_delete=models.SET_NULL,
        blank=True,
        related_name='+',
        help_text='Выберите страницу'
    )

    class Meta:
        verbose_name = "Пункт главного меню"
        verbose_name_plural = "Пункты главного меню"

    def __str__(self):
        return self.title


@register_snippet
class SocialSnippet(models.Model):
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name='Главный баннер'
    )
    link_external = models.URLField(
        "External link",
        blank=True,
        null=True,
        help_text='Set an external link if you want the link to point somewhere outside the CMS.'
    )

    class Meta:
        verbose_name = "Социальная сеть"
        verbose_name_plural = "Социальные сети"

    def __str__(self):
        return self.link_external


@register_snippet
class PartnerSnippet(models.Model):
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name='Логотип партнера'
    )
    partners = models.BooleanField(verbose_name="В блок на главной 'Наши партнеры'")
    trust = models.BooleanField(verbose_name="В блок на главной 'Нам доверяют'")
    right_block = models.BooleanField(verbose_name="В правый блок на страницах контента")

    def __str__(self):
        return 'Логотип партнера: ' + self.image.title

    class Meta:
        verbose_name = "Партнер"
        verbose_name_plural = "Партнеры"

    content_panels = [
        ImageChooserPanel('image'),
        FieldPanel('partners'),
        FieldPanel('trust'),
        FieldPanel('right_block'),
    ]


@register_snippet
class OfficeSnippet(models.Model):
    title = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    time = models.CharField(max_length=255)
    map = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Офис"
        verbose_name_plural = "Офисы"

    def __str__(self):
        return self.title


@register_snippet
class NewsCategory(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории новостей и услуг"

    def __str__(self):
        return self.name

    content_panels = [
        FieldPanel('name'),
    ]
