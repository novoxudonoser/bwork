
from django.conf.urls import patterns, url
from example.views import BaseFormView

urlpatterns = patterns('',
    url(r'classic_form', BaseFormView.as_view(),name='classic_form'),
	)