from django import forms
from django.contrib import admin

from .models import Settings

class SettingsForm(forms.ModelForm):
    class Meta:
        model = Settings
        fields = ('email', 'city', 'payment_type')

    def save(self, commit=True, force_insert=False, force_update=False):
        f = super(SettingsForm, self).save(commit=False)
        f.user = self.user
        f.save()
        return f

class SettingsAdmin(admin.ModelAdmin):
    form = SettingsForm
    list_display = ['user', ]

    def get_form(self, request, *args, **kwargs):
         form = super(SettingsAdmin, self).get_form(request, *args, **kwargs)
         form.user = request.user
         return form

    def get_queryset(self, request):
        if not request.user.is_superuser:
            return Settings.objects.filter(user=request.user)
        else:
            return Settings.objects.all()

admin.site.register(Settings, SettingsAdmin)
