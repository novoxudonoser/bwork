from django.core.urlresolvers import reverse

__author__ = 'PekopT'

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, Group
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class VeloUserManager(BaseUserManager):


    def create_user(self, email, password=None):

        if not email:
            raise ValueError('Email not blank')
        user = self.model(email=VeloUserManager.normalize_email(email), )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class VeloUser(AbstractBaseUser, PermissionsMixin):
    CONTACT_METHODS = (
        ('email', 'Почта'),
        ('phone', 'Телефон'),
    )
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    phonenumber = models.CharField(max_length=20, blank=False, null=True)
    first_name = models.CharField(max_length=255, blank=False, null=True)
    last_name = models.CharField(max_length=255, blank=False, null=True)
    patronym = models.CharField(max_length=50, blank=False, null=True)
    preferred_contact_method = models.CharField(max_length=20, choices=CONTACT_METHODS, blank=True, null=True)
    social_auth_id = models.CharField(max_length=50, blank=True, null=True)
    subscribed_to_newsletter = models.BooleanField(blank=True, default=False)
    count_adverts = models.IntegerField(default=0)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse('profile_personal')

    @property
    def is_staff(self):
        return self.is_admin

    def is_user(self):
        g = Group.objects.get(name='users')
        return True if g in self.groups.all() else False

    def is_studio(self):
        g = Group.objects.get(name='studios')
        return True if g in self.groups.all() else False

    def get_studios(self):
        from velos.models import Studio
        return [x.pk for x in Studio.objects.filter(user=self)]

    def __str__(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def get_full_name(self):
        return "%s %s" % (self.first_name, self.last_name)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = VeloUserManager()
