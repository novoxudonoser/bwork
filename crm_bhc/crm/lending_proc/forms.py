from django import forms

from crm.lending_proc.models import ApplicationUser


class ApplicationUserForm(forms.ModelForm):
    class Meta:
        model = ApplicationUser
        fields = ['user_name','coment', 'email', 'phone', 'type_application', 'calculation_employees', 'calculation_documents', 'calculation_type', 'calculation_cost']

    def __init__(self, ip_address,lending,client,visit, *args, **kwargs):
        super(ApplicationUserForm, self).__init__(*args, **kwargs)

        self.ip_address = ip_address
        self.lending = lending
        self.client = client
        self.visit = visit

    def save(self, commit=True, force_insert=False, force_update=False):
        f = super(ApplicationUserForm, self).save(commit=False)

        f.ip_address = self.ip_address
        f.lending = self.lending
        f.client = self.client
        f.visit = self.visit
        return f.save()
