from django.shortcuts import render
from django.views.generic import ListView, DetailView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse

from crm.calls.forms import CallForm
from crm.calls.models import Call
from crm.orders.forms import ContactForm, PhoneForm
from crm.orders.models import OrderContact
from crm.users.mixins import LoginRequiredMixin

__author__ = 'PekopT'


class CallList(LoginRequiredMixin, ListView):
    model = Call


class CallDetail(DetailView):
    template_name = 'calls/call.html'
    model = Call

    def get_context_data(self, **kwargs):
        context = super(CallDetail, self).get_context_data(**kwargs)
        return context


class CallCreate(LoginRequiredMixin, CreateView):
    model = Call
    form_class = CallForm
    template_name_suffix = '_form'

    def form_valid(self, form):
        from django.utils import timezone

        self.object = form.save(commit=False)
        if self.request.user.is_authenticated():
            self.object.user = self.request.user
        else:
            self.object.user_id = 1  # TODO: auth on whole interface. and User only
        self.object.date_pushed = self.request.POST.get('date_pushed', timezone.now())
        self.object.save()
        self.object.number = timezone.now().strftime('%d%m') + '-' + str(self.object.pk)
        self.object.save()
        return super(CallCreate, self).form_valid(form)

    def get_success_url(self, *args, **kwargs):
        action = self.request.POST.get('action', False)
        if action:
            if action == 'save_call':
                return reverse('calls-create')
            if action == 'save_close':
                return reverse('calls-list')
            if action == 'save_order':
                return reverse('orders-create-call', kwargs={'call_id': self.object.pk})

        return super(CallCreate, self).get_success_url(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CallCreate, self).get_context_data(**kwargs)
        context['contact_form'] = ContactForm(prefix='cntc')
        context['phone_form'] = PhoneForm(prefix='phone')

        return context


class CallUpdate(LoginRequiredMixin, UpdateView):
    model = Call
    template_name = 'calls/call_form.html'
    form_class = CallForm

    def post(self, request, *args, **kwargs):
        ret = super(CallUpdate, self).post(request, *args, **kwargs)

        if not request.POST.get('client'):
            form = ContactForm(request.POST, prefix='cntc')
            if form.is_valid():
                new_client = form.save()
                self.object.client = new_client
        else:
            new_client = OrderContact.objects.get(pk=request.POST.get('client'))
            self.object.client = new_client

        self.object.save()

        if request.POST.get('phone-phone', False):
            form1 = PhoneForm(request.POST, prefix='phone')
            if form1.is_valid():
                new_phone = form1.save()
                self.object.client.phones.add(new_phone)

        self.object.save()

        return ret

    def get_context_data(self, **kwargs):
        context = super(CallUpdate, self).get_context_data(**kwargs)
        context['contact_form'] = ContactForm(prefix='cntc')
        context['phone_form'] = PhoneForm(prefix='phone')

        return context


class CallDelete(LoginRequiredMixin, DeleteView):
    model = Call
    success_url = reverse_lazy('calls-list')
    form_class = CallForm


class BaseView(View):
    def get(self, request):
        return render(request, 'index.html')
