/**
 * Created by PekopT on 02.03.16.
 */
var app = angular.module('app', ['ui.bootstrap', 'ui.select', 'checklist-model', 'ui.bootstrap-slider']);

app.config(['$interpolateProvider', '$httpProvider', function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('{(');
    $interpolateProvider.endSymbol(')}');

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}]);

app.controller('AdvancedSearch', ['$scope', '$timeout', '$uibModal', function AdvancedSearch($scope, $timeout, $uibModal) {

    $scope.things = [{ id: 1, name: 'foo' }, { id: 2, name: 'bar' }, { id: 3, name: 'baz' }, { id: 4, name: 'qux' }, { id: 5, name: 'quux' }, { id: 5, name: 'quux' }, { id: 6, name: 'corge' }, { id: 6, name: 'corge' }, { id: 7, name: 'grault' }];

    $scope.openModal = function openModal($event, smyj) {
        // smth
        return $uibModal.open({
            templateUrl: 'confirmationModalTempl',
            controller: 'ModalCtrl as asdfasdfs'
        }).result.then(res => {
            if (res === 'yes') $scope.modalResult = 'yes';
        }, res => {
            $scope.modalResult = 'no';
        });
    };

    $timeout(function timeout() {
        $('input.-icheck').icheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
    });

    $scope.submit = function (e) {
        data = {
            'price_start': $scope.filterForm.price[0],
            'price_end': $scope.filterForm.price[0]
        };

        $http.post('/filter_bicycles', data).then(function successCallback(response) {
            $('#bicycles_list').html(response.data);
        }, function errorCallback(response) {
            console.log("error");
        });
    };
}]);

app.controller('ModalCtrl', ['$scope', '$uibModalInstance', function ConfirmationModalCtrl($scope, $uibModalInstance) {
    const s = this;

    s.yes = () => $uibModalInstance.close('yes');
    s.cancel = () => $uibModalInstance.dismiss();
}]);

app.controller('TopSearchCtrl', ['$scope', function TopSearchCtrl($scope, $timeout, $uibModal) {
    //TODO: make this values gettable from server
    $scope.peoples = [{ 'id': 1, name: '1 человек' }, { 'id': 2, name: 'Компания' }];

    $scope.genders = [{ 'id': 0, name: 'Женский' }, { 'id': 1, name: 'Мужской' }];

    $scope.velotypes = [{ 'id': 1, name: 'Женский' }, { 'id': 2, name: 'Мужской' }, { 'id': 3, name: 'Мужской' }];

    $scope.cities = [{ 'id': 1, name: 'Москва' }, { 'id': 2, name: 'Краснодар' }];

    $scope.regions = [{ 'id': 1, name: 'Бирюлево' }, { 'id': 2, name: 'Бутово' }];

    $scope.selectPeople = function (item, model) {
        if (item.id == 1) {
            //TODO
        } else {
                //TODO
            }
    };
}]);

app.controller('ReviewFormCtrl', ['$scope', function ReviewFormCtrl($scope, $timeout, $uibModal) {
    $scope.ratingClick = function (smth) {
        $("#id_rating").val(smth);
    };
}]);

app.controller('addDescCtrl', ['$scope', function addDescCtrl($scope, $timeout, $uibModal) {
    $scope.changeAdditional = function ($event) {
        var id = $($event.target).data('id');
        var field = $($event.target).data('field');
        var val = $($event.target).is(':checked');
        if (val) {
            $("#" + field + " option[value='" + id + "']").attr('selected', 'selected');
        } else {
            $("#" + field + " option[value='" + id + "']").removeAttr('selected');
        }
    };
}]);

app.controller('addVeloCtrl', ['$scope', function addVeloCtrl($scope) {
    $scope.showAddForm = function ($event) {
        $("#velo_form").show();
        $($event.target).hide();
    };
    $scope.chooseFile = function () {
        $("#file_1").click();
        return false;
    };
    $scope.handleFile = function ($event) {
        console.log($event);
    };

    //$("#upload_form").submit(function (e) {
    //    e.preventDefault(); // avoid to execute the actual submit of the form.
    //});

    $("#file_1").on('change', function (e) {
        let file = $(this).val();
        console.log(file);
        var url = $("#upload_form").attr('action'); // the script where you handle the form input.
        var formData = new FormData($("#upload_form")[0]);
        console.log(formData);
        //$("#upload_form").submit();
        $.ajax({
            type: "POST",
            url: url,
            data: formData, // serializes the form's elements.
            xhr: function () {
                // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // Check if upload property exists
                    myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
                }
                return myXhr;
            },
            success: function (data) {
                console.log(data);
            },
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });
    });
}]);

app.controller('veloListCtrl', ['$scope', function ($scope) {}]);

app.controller('SubscribeCtrl', ['$scope', function ($scope) {
    $scope.submit = function () {
        // TODO: make this creating subscribe
        return false;
    };
}]);