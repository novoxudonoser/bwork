from django.core.management import BaseCommand
from django.db import connection

__author__ = 'PekopT'


class Command(BaseCommand):
    help = 'Setup test data'

    def handle(self, *args, **options):
        cursor = connection.cursor()
        if connection.vendor == 'sqlite':
            tables = list(cursor.execute("select name from sqlite_master where type is 'table'"))
            cursor.executescript(';'.join(["drop table if exists %s" % i for i in tables[1:]]))
        else:
            cursor.execute('show tables;')
            parts = ('DROP TABLE IF EXISTS %s;' % table for (table,) in cursor.fetchall())
            sql = 'SET FOREIGN_KEY_CHECKS = 0;\n' + '\n'.join(parts) + 'SET FOREIGN_KEY_CHECKS = 1;\n'
            connection.cursor().execute(sql)
