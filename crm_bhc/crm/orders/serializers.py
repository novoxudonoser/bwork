from rest_framework.decorators import api_view
from rest_framework import serializers

from .models import Order, OrderContact


class OrderContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderContact


class OrderClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
