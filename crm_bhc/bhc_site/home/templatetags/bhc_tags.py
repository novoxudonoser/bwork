from django.utils.safestring import mark_safe
from wagtail.wagtailcore.models import Page

from bhc_site.home.snippets import HomeSnippets, MenuSnippet, SocialSnippet, PartnerSnippet, NewsCategory

from bhc_site.home.models import NewsPage

__author__ = 'PekopT'
from django import template

register = template.Library()


@register.assignment_tag(takes_context=True)
def get_site_root(context):
    # NB this returns a core.Page, not the implementation-specific model used
    # so object-comparison to self will return false as objects would differ
    return context['request'].site.root_page


@register.assignment_tag()
def snippet(snippet_id):
    try:
        return HomeSnippets.objects.get(pk=snippet_id)
    except HomeSnippets.DoesNotExist:
        return False


def has_menu_children(page):
    return page.get_children().live().in_menu().exists()


# Retrieves the top menu items - the immediate children of the parent page
# The has_menu_children method is necessary because the bootstrap menu requires
# a dropdown class to be applied to a parent
@register.inclusion_tag('tags/top_menu.html', takes_context=True)
def top_menu(context, parent, calling_page=None):
    menuitems = Page.objects.live().in_menu()
    for menuitem in menuitems:
        menuitem.active = (calling_page.url.startswith(menuitem.url)
                           if calling_page else False)
    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        # required by the pageurl tag that we want to use within this template
        'request': context['request'],
    }

@register.inclusion_tag('tags/right_menu.html', takes_context=True)
def childs_menu(context, parent, calling_page=None):
    menuitems = parent.get_children().live()
    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        'request': context['request'],
    }

@register.assignment_tag()
def main_menu():
    objects = MenuSnippet.objects.all()
    return [
        {'title': x.title, 'anons': mark_safe(x.anons), 'url': x.link_page, 'on_main': x.on_main, 'on_menu': x.on_menu}
        for x in objects
    ]


@register.inclusion_tag('tags/partners.html')
def partners(mode='partners', limit=5):
    objects = PartnerSnippet.objects.all()

    if mode == 'partners':
        objects = objects.filter(partners=True)
    elif mode == 'trust':
        objects = objects.filter(trust=True)
    elif mode == 'right':
        objects = objects.filter(right_block=True)

    return {
        'items': objects[:limit]
    }


@register.assignment_tag()
def socials():
    return SocialSnippet.objects.all()


@register.inclusion_tag('tags/right_news.html', takes_context=True)
def right_news(context, limit=3):
    news = NewsPage.objects.all().order_by('-go_live_at')

    page = context['self']
    if isinstance(page, NewsPage) and page.category_id:
        news = news.filter(category_id=page.category_id)

    return {
        'news_items': news[:limit],
        'request': context['request'],
    }


@register.inclusion_tag('tags/main_news.html', takes_context=True)
def main_news(context, limit=4):
    return {
        'news_items': NewsPage.objects.all().order_by('-go_live_at')[:limit],
        'request': context['request'],
    }


@register.inclusion_tag('tags/right_categories_menu.html', takes_context=True)
def categories(context):
    items = NewsCategory.objects.all()
    return {
        'items': items,
        'context': context['request']
    }
