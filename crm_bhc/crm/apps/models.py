# -*- coding: utf-8 -*- 
from django.db import models

from crm.api.models import City, User
from crm.orders.models import PAYMENT_TYPE


class Settings(models.Model):
    """ Настройки пользователя """

    user = models.OneToOneField(User, related_name='settings', unique=True)
    email = models.EmailField(max_length=255, blank=True, null=True)
    city = models.ForeignKey(City, verbose_name='Город по-умолчанию', blank=True, null=True)
    payment_type = models.IntegerField(choices=PAYMENT_TYPE, verbose_name='Способ оплаты по-умолчанию', default=1)

    class Meta:
        verbose_name = "Настройки"
        verbose_name_plural = "Настройки"