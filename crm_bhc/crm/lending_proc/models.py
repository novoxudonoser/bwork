from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.core.exceptions import ValidationError

class SearchqQuerysStats(models.Model):
    num_visits = models.IntegerField(verbose_name="Количетво переходов",default=0)
    num_calls  = models.IntegerField(verbose_name="Количетво звонков",default=0)
    num_apps   = models.IntegerField(verbose_name="Количетво заявок",default=0)

    def __str__(self):
        return str(self.num_visits)+' '+str(self.num_calls)+' '+str(self.num_apps)

    class Meta:
        verbose_name = 'Счётчик статиситки'
        verbose_name_plural = 'Счётчик статиситки'

class SearchqQuerys(models.Model):
    qerydata   = models.CharField(max_length=50,verbose_name="Текст запроса")
    stats      = models.OneToOneField(SearchqQuerysStats,related_name='SearchqQuerys',blank=True,null=True,default=None)


    def __str__(self):
        return str(self.qerydata)

    class Meta:
        verbose_name = 'Поисковые запросы'
        verbose_name_plural = 'Поисковые запросы'

#=======================================

class Lendings(models.Model):
    name       = models.CharField(max_length=20, verbose_name="Назвние лендинга компании")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Лендинги'
        verbose_name_plural = 'Лендинги'

class Clients(models.Model):
    lending          = models.ForeignKey(Lendings,verbose_name="Лендинг",null=True)
    cookie_track     = models.CharField(max_length=10,verbose_name="cookie")
    date             = models.DateTimeField()
    visits           = models.BigIntegerField(default=0,verbose_name='Количество посещений по рекламе')
    apped            = models.BooleanField(default=False,verbose_name='Отсылал ли заявку')
    phoned           = models.BooleanField(default=False,verbose_name='Звонил ли')

    def __str__(self):
        return self.cookie_track

    class Meta:
        verbose_name = 'Отслеживамые клиенты'
        verbose_name_plural = 'Отслеживамые клиенты'

class Companes(models.Model):
    name       = models.CharField(max_length=20, verbose_name="Назвние рекламной компании")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Рекламные компании'
        verbose_name_plural = 'Рекламные компании'

class Pfones(models.Model):
    lending      = models.ForeignKey(Lendings,verbose_name="Лендинг")
    number       = PhoneNumberField(unique=True, verbose_name="Номер")
    expires_date = models.DateTimeField(blank=True,editable=False,null=True)
    active       = models.BooleanField(default=True,editable=False)
    compane      = models.ForeignKey(Companes, verbose_name="Рекламная компания")
    cookie       = models.CharField(max_length=10,blank=True,editable=False)
    is_default   = models.BooleanField(default=False, verbose_name="Резервный")

    def __str__(self):
        return  str(self.number)

    class Meta:
        verbose_name = 'Номера'
        verbose_name_plural = 'Номера'
        
    def clean(self):
        if self.is_default:
            temp=None
            try:
                temp = Pfones.objects.get(is_default=True)
            except:
                pass
            if temp and self != temp:
                raise ValidationError("Может быть тоько один резервный номер")

class PfoneShown(models.Model):
    client     = models.ForeignKey(Clients,verbose_name="клиент",null=True)
    phone      = models.ForeignKey(Pfones,verbose_name="Номер")
    shown_date = models.DateTimeField(verbose_name="Время показа") #время генерации
    cookie     = models.CharField(max_length=10,verbose_name="cookie")
    active     = models.BooleanField(default=True,editable=False)

    def __str__(self):
        return str(self.client)

    class Meta:
        verbose_name = 'Лог индификаторов'
        verbose_name_plural = 'Лог индификаторов'







#======================================>





#-------------------------------------------------------------------->

class DirectCompanes(models.Model):
    leding      = models.ForeignKey(Lendings,verbose_name="Лендинг")
    compane     = models.ForeignKey(Companes,blank=True,null=True,default=None,verbose_name="Кампания")
    stats       = models.OneToOneField(SearchqQuerysStats,related_name='DirectCompanes',blank=True,null=True,default=None)
    campaign_id = models.BigIntegerField(unique=True,verbose_name="Идентификатор рекламной кампании")
    name        = models.CharField(max_length=20,blank=True,null=True,default=None)

    def __str__(self):
        return self.name or str(self.campaign_id)  

    class Meta:
        verbose_name = 'Direct Companes'
        verbose_name_plural = 'Direct Companes'

class DirectGroups(models.Model):
    diectcomp   = models.ForeignKey(DirectCompanes,blank=True,null=True,default=None,verbose_name="Директ рекламная копания")
    stats       = models.OneToOneField(SearchqQuerysStats,related_name='DirectGroups',blank=True,null=True,default=None) 
    gbid        = models.BigIntegerField(unique=True,verbose_name="Идентификатор группы")

    def __str__(self):
        return str(self.gbid)

    class Meta:
        verbose_name = 'Direct Groups'
        verbose_name_plural = 'Direct Groups'

class DirectAdds(models.Model):
    diectgroup  = models.ForeignKey(DirectGroups,blank=True,null=True,default=None,verbose_name="Директ группа объявлений")
    stats       = models.OneToOneField(SearchqQuerysStats,related_name='DirectAdds',blank=True,null=True,default=None) 
    ad_id       = models.BigIntegerField(unique=True,verbose_name="Идентификатор объявления")

    def __str__(self):
        return str(self.ad_id)

    class Meta:
        verbose_name = 'Direct Adds'
        verbose_name_plural = 'Direct Adds'

class DirectPhrases(models.Model):
    diectadd     = models.ForeignKey(DirectAdds,blank=True,null=True,default=None,verbose_name="Директ объявление")
    stats        = models.OneToOneField(SearchqQuerysStats,related_name='DirectPhrases',blank=True,null=True,default=None)
    phrase_id    = models.BigIntegerField(unique=True,verbose_name="Идентификатор ключевой фразы")

    def __str__(self):
        return str(self.phrase_id)

    class Meta:
        verbose_name = 'Direct Phrases'
        verbose_name_plural = 'Direct Phrases'

class DirectRegions(models.Model):
    region_id      = models.SmallIntegerField(unique=True,verbose_name="Идентификатор региона")

    def __str__(self):
        return str(self.region_id)

    class Meta:
        verbose_name = 'Direct Regions'
        verbose_name_plural = 'Direct Regions'


DIRECT_CAMPAIGN_TYPE= (
    (1,'type1'),
    (2,'type2'),
    (3,'type3'),
    )


DIRECT_SOURCE_TYPE= (
    (1, 'search'),
    (2, 'context'),
    )

DIRECT_ADDPHRASES= (
    (1,'yes'),
    (2,'no'),
    )

DIRECT_POSITION_TYPE= (
    (1,'premium'),
    (2,'other'),
    (3,'none'),
    )

DIRECT_DEVICE_TYPE= (
    (1,'desktop'),
    (2,'mobile'),
    (3,'tablet'),
    )



import hashlib

class YandexGetParametrsStatic(models.Model):
    """
from crm.lending_proc.views import *
from furl import furl
url=furl('http://buh.services/?type=search&source=1233&source_type=direct&addphrases=yes&addphrasestext=sdf&campaign_type=type1&adtarget_id=1123&position_type=premium&position=2&keyword=qwe&campaign_id=1&retargeting_id=123&ad_id=1597645534&phrase_id=123&gbid=1155300404&device_type=desktop&region_id=213&region_name=Москва&yclid=5954114684525238680/')
from crm.lending_proc.models import ClientsTrack
visit=ClientsTrack.objects.get(pk=1)
save_yandex_getparams(url,visit)
    """
    hash           = models.CharField(max_length=20,unique=True,editable=False)
    query          = models.ManyToManyField(SearchqQuerys,through='YgetparamesstatSearchqerysRel',related_name='getparamstat',verbose_name="Поисковый запрос")

    ad_id          = models.ForeignKey(DirectAdds,blank=True,null=True,verbose_name="Идентификатор объявления")
    campaign_id    = models.ForeignKey(DirectCompanes,blank=True,null=True,verbose_name="Идентификатор рекламной кампании")
    campaign_type  = models.SmallIntegerField(choices=DIRECT_CAMPAIGN_TYPE,blank=True,null=True,verbose_name="Тип кампании")
    gbid           = models.ForeignKey(DirectGroups,blank=True,null=True,verbose_name="Идентификатор группы")
    keyword        = models.CharField(max_length=50,blank=True,null=True,verbose_name="Ключевая фраза")
    phrase_id      = models.ForeignKey(DirectPhrases,blank=True,null=True,verbose_name="Идентификатор ключевой фразы")
    source         = models.CharField(max_length=50,blank=True,null=True,verbose_name="Место показа")
    source_type    = models.SmallIntegerField(choices=DIRECT_SOURCE_TYPE,blank=True,null=True,verbose_name="Тип площадки")
    region_name    = models.CharField(max_length=50,blank=True,null=True,verbose_name="Регион")
    region_id      = models.ForeignKey(DirectRegions,blank=True,null=True,verbose_name="Идентификатор региона")
    
    def __str__(self):
        return str(self.hash[:10])

    class Meta:
        verbose_name = 'Yandex get параметры Статические'
        verbose_name_plural = 'Yandex get параметры Статические'

    def calchash(self):
        hash = hashlib.sha256()
        data={
        'ad_id':self.ad_id.id if self.ad_id else 0,
        'campaign_id':self.campaign_id.id if self.campaign_id else 0,
        'campaign_type':self.campaign_type,
        'gbid':self.gbid,
        'keyword':self.keyword,
        'phrase_id':self.phrase_id.id if self.phrase_id else 0,
        'source':self.source,
        'source_type':self.source_type,
        'region_name':self.region_name,
        'region_id':self.region_id.id if self.region_id else 0,
        }
        hash.update(str(data).encode())
        return  hash.hexdigest()[:20].upper()

class YgetparamesstatSearchqerysRel(models.Model):
    query        = models.ForeignKey(SearchqQuerys,related_name='YgetparamesstatSearchqerysRel')
    getparamstat = models.ForeignKey(YandexGetParametrsStatic,related_name='YgetparamesstatSearchqerysRel')
    stats        = models.OneToOneField(SearchqQuerysStats,related_name='YgetparamesstatSearchqerysRel',blank=True,null=True,default=None)

    class Meta:
        unique_together = [("query", "getparamstat")]
        # abstract = True


#======================================>





class ClientsTrack(models.Model):
    client       = models.ForeignKey(Clients,verbose_name="клиент")
    lending      = models.ForeignKey(Lendings,verbose_name="Лендинг",null=True)
    url          = models.TextField(verbose_name="url")
    ip_address   = models.GenericIPAddressField(null=True)
    date         = models.DateTimeField()
    getparamstat = models.ForeignKey(YandexGetParametrsStatic,related_name='visit',blank=True,null=True,default=None,verbose_name="YandexGetParametrsStatic")
    query        = models.ForeignKey(SearchqQuerys,blank=True,null=True,default=None,verbose_name="Поисковый запрос")
    getparamstatquery=models.OneToOneField(YgetparamesstatSearchqerysRel,blank=True,null=True,default=None)

    def __str__(self):
        return str(self.client)+' '+str(self.lending)+' '+self.date.strftime("%Y-%m-%d %H:%M:%S")

    class Meta:
        verbose_name = 'Лог посешения клиентами лендингов'
        verbose_name_plural = 'Лог посешения клиентами лендингов'

class PfoneIncomeLog(models.Model):
    client       = models.ForeignKey(Clients,verbose_name="клиент",null=True)
    lending      = models.ForeignKey(Lendings,verbose_name="Лендинг",null=True)
    phone_to     = PhoneNumberField(verbose_name="Номер исходящий")
    phone_from   = PhoneNumberField(verbose_name="Номер входящий") # сделлано отдельным полем а не связью с Pfones чтобы Pfones можно было менять
    cookie       = models.CharField(max_length=10,verbose_name="cookie pfone")
    income_date  = models.DateTimeField(verbose_name="Время звонка")
    visit        = models.ForeignKey(ClientsTrack,related_name='phone',blank=True,null=True,verbose_name="Посещение")
    query        = models.ForeignKey(SearchqQuerys,blank=True,null=True,verbose_name="Поисковый запрос")
    counted      = models.BooleanField(default=False,verbose_name="Было подсчитано при роцесенге поисковых запросов")
    

    def __str__(self):
        return '{} -> {}'.format(str(self.phone_from),str(self.phone_to))

    class Meta:
        verbose_name = 'Лог звонков'
        verbose_name_plural = 'Лог звонков'

TYPE_APPLICATION = (
    (1, 'Обычная'),
    (2, 'Бесплатный период'),
    (3, 'Звонок'),
    (4, 'Обслуживание'),
)

CALCULATION_TYPE = (
    (1, 'УСН 6%'),
    (2, 'УСН 15%'),
    (3, 'ОСНО'),
)


class ApplicationUser(models.Model):
    """ Заявки пользователей """
    client      = models.ForeignKey(Clients,verbose_name="клиент",null=True)
    user_name = models.CharField(max_length=255, verbose_name='Имя')
    email = models.EmailField()
    phone = models.CharField(max_length=17, verbose_name='Телефон')
    coment = models.TextField(blank=True,null=True,verbose_name='Коментарий')
    ip_address = models.GenericIPAddressField(null=True)
    type_application = models.IntegerField(choices=TYPE_APPLICATION, default=1)

    calculation_type = models.IntegerField(choices=CALCULATION_TYPE, verbose_name='Тип обслуживания', blank=True, null=True)
    calculation_documents = models.IntegerField(blank=True, null=True, verbose_name='Количество документов')
    calculation_employees = models.IntegerField(blank=True, null=True, verbose_name='Количество сотрудников')
    calculation_cost = models.IntegerField(blank=True, null=True, verbose_name='Стоимость')

    created = models.DateTimeField(auto_now=True)
    lending = models.ForeignKey(Lendings,verbose_name="Лендинг",null=True)#лучше чтобы у неправильной заявки небыло лендинга,чем чтобы она не обработалась

    query    = models.ForeignKey(SearchqQuerys,blank=True,null=True,verbose_name="Поисковый запрос")
    counted  = models.BooleanField(default=False,verbose_name="Было подсчитано при роцесенге поисковых запросов")
    visit    = models.ForeignKey(ClientsTrack,related_name='app',blank=True,null=True,verbose_name="Посещение")

    def calculation_type_name(self):
        return dict(map(iter,CALCULATION_TYPE)).get(self.calculation_type,'')
    def type_application_name(self):
        return dict(map(iter,TYPE_APPLICATION)).get(self.type_application,'')

    def __str__(self):
        return self.user_name

    class Meta:
        db_table = 'application_user'
        verbose_name = 'Заявка пользователя'
        verbose_name_plural = 'Заявки пользователей'
        ordering = ['-created']


class ManagerEmail(models.Model):
    email  = models.EmailField(verbose_name='Email менеджера')
    active =  models.BooleanField(verbose_name='Активный',default=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Email менеджеров'
        verbose_name_plural = 'Email менеджеров'

class YandexGetParametrsDynamic(models.Model):
    visit          = models.OneToOneField(ClientsTrack,related_name='getparamdyn',verbose_name="Посещение")
    yclid          = models.CharField(max_length=30,blank=True,null=True,verbose_name="click id")
    device_type    = models.SmallIntegerField(choices=DIRECT_DEVICE_TYPE,blank=True,null=True,verbose_name="Тип устройства")
    retargeting_id = models.BigIntegerField(blank=True,null=True,verbose_name="Идентификатор условия ретаргетинга")
    adtarget_id    = models.BigIntegerField(blank=True,null=True,verbose_name="Идентификатор условия ретаргетинга")
    adtarget_name  = models.CharField(max_length=50,blank=True,null=True,verbose_name="Условие нацеливания, по которому было показано динамическое объявление")
    position       = models.SmallIntegerField(blank=True,null=True,verbose_name="Точная позиция объявления в блоке")

    position_type  = models.SmallIntegerField(choices=DIRECT_POSITION_TYPE,blank=True,null=True,verbose_name="Тип блока")
    addphrases     = models.SmallIntegerField(choices=DIRECT_ADDPHRASES,blank=True,null=True,verbose_name="Авто добавление фразы")
    addphrasestext = models.CharField(max_length=50,blank=True,null=True,verbose_name="Текст автоматически добавленной фразы")


    def __str__(self):
        return str(self.yclid)

    class Meta:
        verbose_name = 'Yandex get параметры Динамические'
        verbose_name_plural = 'Yandex get параметры Динамические'
