from django.db.models import Q

from crm.orders.models import OrderContact, Order

__author__ = 'PekopT'

from rest_framework import generics, permissions, mixins
from ..orders.serializers import OrderContactSerializer
from .models import Call, CallQuestion
from .serializers import CallSerializer, QuestionSerializer


class CallList(generics.ListAPIView):
    model = Call
    # serializer_class = UserSerializer
    permission_classes = [
        permissions.AllowAny
    ]


class CallDetail(generics.RetrieveUpdateDestroyAPIView, mixins.CreateModelMixin):
    model = Call
    serializer_class = CallSerializer
    # lookup_field = 'id'
    # http_method_names = ['get', 'post', 'delete']

    def post(self, request, *args, **kwargs):
        return super(CallDetail, self).create(request, *args, **kwargs)


class OrderContactList(generics.ListAPIView):
    model = OrderContact
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = OrderContactSerializer
    http_method_names = ['get']

    def get_queryset(self):
        """
            filtering
        """
        queryset = OrderContact.objects.all()
        term = self.request.GET.get('term', None)
        if term is not None:
            clients_id = list(
                Order.objects.filter(
                    Q(documents__firm__contains=term) |
                    Q(documents__fname_ip__contains=term) |
                    Q(documents__lname_ip__contains=term) |
                    Q(documents__mname_ip__contains=term) |
                    Q(documents__inn__contains=term) |
                    Q(documents__ogrn__contains=term)
                ).values_list('client__id', flat=True) \
                .annotate()
            )
            queryset = queryset.filter(
                Q(name__contains=term) |
                Q(phones__email__contains=term) |
                Q(phones__phone__contains=term) |
                Q(id__in=clients_id)
            )
        return queryset



class QuestionList(generics.ListAPIView):
    model = CallQuestion
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = QuestionSerializer
    http_method_names = ['get', 'post', 'delete']

    def get_queryset(self):
        """
        filtering
        :return: queryseyt
        """
        queryset = CallQuestion.objects.all()
        themes = self.request.GET.get('themes', None)
        if themes is not None:
            queryset = queryset.filter(theme__in=themes)

        return queryset
