# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-20 14:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TmpFileStorage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('attach', models.FileField(upload_to='tmp/filestorage', verbose_name='Приложение')),
            ],
            options={
                'verbose_name_plural': 'Временное хранилище файлов',
                'verbose_name': 'Временное хранилище файлов',
            },
        ),
    ]
