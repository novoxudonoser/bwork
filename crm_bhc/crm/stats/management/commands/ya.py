from django.core.management.base import BaseCommand
from crm.stats.models import Counters,TrafficSummary,TrafficDeepness, TrafficHourly,TrafficLoad, SourcesSummary, SourcesSites, SourcesSearch_engines, SourcesPhrases
from crm.stats.models import ContentPopular, ContentEntrance, ContentExit, ContentTitles, ContentUrlParam, Geo, TechMobile_percent
from crm.stats.models import DemographyAgeGender_Age,DemographyAgeGender_Sex,DemographyStructure,TechBrowsers,TechOs,TechDisplay,TechMobile
from crm.stats.yametrikapy.core import Metrika

import datetime

#import pdb

class Command(BaseCommand):
    args = '<startdate>'

    def handle(self, *args, **options):


        startdate=args[0] if args else None
        print('startdate',startdate)

        perpage=100

        login = 'pekopt2'
        client_id = '60601a5d7ce04605bf0417169a7536ce'
        password = 'f77dc7821c364af6beab0fdc1272d12d'
        password = 'vfhbyrf'
        token = 'd4d87b30af77425288d44a25fbd9ddc6'
        # metrika = MetrikaV1(client_id, login, password, token)
        metrika = Metrika(client_id, login, password, token)
        # counters = metrika.GetCounterList().counters
        # print(counters)
        for counter in Counters.objects.all():
            try:
                last_trafficsummary_data=TrafficSummary.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_trafficsummary_data=[]
                print(e)
            print(last_trafficsummary_data,startdate)

            if last_trafficsummary_data !=[]:
                date1 =(last_trafficsummary_data.date+datetime.timedelta(days=1)) if last_trafficsummary_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None

            date2 =datetime.date.today().strftime("%Y%m%d")
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                try:
                    trafficsummary_data=metrika.GetStatTrafficSummary(id=counter.id,date1=date1,date2=date2).data
                except Exception as e:
                    print(e)
                    trafficsummary_data=[]
                for trafsum in trafficsummary_data:
                    print(counter.id)
                    newrow=TrafficSummary(
                        counter_id=counter,
                        date=datetime.datetime.strptime(trafsum.get('date'), "%Y%m%d").date(),
                        visits=trafsum.get('visits'),
                        visitors=trafsum.get('visitors'),
                        depth= trafsum.get('depth'),
                        page_views=trafsum.get('page_views'),
                        visit_time=trafsum.get('visit_time'),
                        new_visitors=trafsum.get('new_visitors')
                        )
                    newrow.save()


            try:
                last_TrafficDeepness_data=TrafficDeepness.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    print(date)
                    try:
                        TrafficDeepness_data=metrika.GetStatTrafficDeepness(id=counter.id,date1=date,date2=date).data_time
                    except Exception as e:
                        print(e)
                        TrafficDeepness_data=[]
                    for trafsum in TrafficDeepness_data:
                        newrow=TrafficDeepness(
                            counter_id=counter,
                            date=date,
                            name=trafsum.get('name'),
                            visits=trafsum.get('visits'),
                            depth=trafsum.get('depth'), #
                            visits_percent=trafsum.get('visits_percent'), #trafsum.get('visits_percent')
                            visit_time=trafsum.get('visit_time'),
                            )
                        newrow.save()

            #TrafficHourly
            try:
                last_TrafficDeepness_data=TrafficHourly.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    print(date)
                    try:
                        TrafficDeepness_data=metrika.GetStatTrafficHourly(id=counter.id,date1=date,date2=date).data
                    except Exception as e:
                        print(e)
                        TrafficDeepness_data=[]
                    for trafsum in TrafficDeepness_data:
                        newrow=TrafficHourly(
                            counter_id=counter,
                            date=date,
                            hours=datetime.datetime.strptime(trafsum.get('hours'), "%H:%M").time(),
                            depth=trafsum.get('depth'), #
                            avg_visits=trafsum.get('avg_visits'), #trafsum.get('visits_percent')
                            visit_time=trafsum.get('visit_time'),
                            )
                        newrow.save()


            #TrafficLoad
            try:
                last_trafficsummary_data=TrafficLoad.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_trafficsummary_data=[]
                print(e)
            print(last_trafficsummary_data,startdate)

            if last_trafficsummary_data !=[]:
                date1 =(last_trafficsummary_data.date+datetime.timedelta(days=1)) if last_trafficsummary_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None

            date2 =datetime.date.today().strftime("%Y%m%d")
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                try:
                    trafficsummary_data=metrika.GetStatTrafficLoad(id=counter.id,date1=date1,date2=date2).data
                except Exception as e:
                    print(e)
                    trafficsummary_data=[]
                for trafsum in trafficsummary_data:
                    print(counter.id)
                    newrow=TrafficLoad(
                        counter_id=counter,
                        date=datetime.datetime.strptime(trafsum.get('date'), "%Y%m%d").date(),
                        max_rps=trafsum.get('max_rps'),
                        max_users=trafsum.get('max_users'),
                        max_rps_date=datetime.datetime.strptime(trafsum.get('max_rps_date'), "%Y-%m-%d").date(),
                        max_rps_time=datetime.datetime.strptime(trafsum.get('max_rps_time'), "%H:%M:%S").time(),
                        max_users_date=datetime.datetime.strptime(trafsum.get('max_users_date'), "%Y-%m-%d").date(),
                        max_users_time=datetime.datetime.strptime(trafsum.get('max_users_time'), "%H:%M:%S").time()
                        )
                    newrow.save()

            #SourcesSummary
            try:
                last_TrafficDeepness_data=SourcesSummary.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    print(date)
                    try:
                        TrafficDeepness_data=metrika.GetStatSourcesSummary(id=counter.id,date1=date,date2=date).data
                    except Exception as e:
                        print(e)
                        TrafficDeepness_data=[]
                    for trafsum in TrafficDeepness_data:
                        newrow=SourcesSummary(
                            counter_id=counter,
                            date=date,
                            visits=trafsum.get('visits'),
                            page_views=trafsum.get('page_views'), #
                            visit_time=trafsum.get('visit_time'), #trafsum.get('visits_percent')
                            depth=trafsum.get('depth'),
                            )
                        newrow.save()

            #SourcesSites
            try:
                last_TrafficDeepness_data=SourcesSites.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatSourcesSites(id=counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=SourcesSites(
                                counter_id=counter,
                                date=date,
                                visits=trafsum.get('visits'),
                                page_views=trafsum.get('page_views'), #
                                visit_time=trafsum.get('visit_time'), #trafsum.get('visits_percent')
                                depth=trafsum.get('depth'),
                                )
                            newrow.save()

            #SourcesSearch_engines
            try:
                last_TrafficDeepness_data=SourcesSearch_engines.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatSourcesSearchEngines(id=counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=SourcesSearch_engines(
                                counter_id=counter,
                                date=date,
                                name=trafsum.get('name'),
                                visits=trafsum.get('visits'),
                                page_views=trafsum.get('page_views'), #
                                visit_time=trafsum.get('visit_time'), #trafsum.get('visits_percent')
                                depth=trafsum.get('depth'),
                                )
                            newrow.save()

            #SourcesPhrases
            try:
                last_TrafficDeepness_data=SourcesPhrases.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatSourcesPhrases(counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=SourcesPhrases(
                                counter_id=counter,
                                date=date,
                                visits=trafsum.get('visits'),
                                page_views=trafsum.get('page_views'), #
                                visit_time=trafsum.get('visit_time'), #trafsum.get('visits_percent')
                                depth=trafsum.get('depth'),
                                phrase=trafsum.get('phrase'),
                                search_engine=trafsum.get('search_engines')[0].get('se_url')
                                )
                            newrow.save()


            #ContentPopular
            try:
                last_TrafficDeepness_data=ContentPopular.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatContentPopular(counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=ContentPopular(
                                counter_id=counter,
                                date=date,
                                url=trafsum.get('url'),
                                page_views=trafsum.get('page_views'), #
                                entrance=trafsum.get('entrance'), #trafsum.get('visits_percent')
                                exit=trafsum.get('exit'),
                                )
                            newrow.save()


            #ContentEntrance
            try:
                last_TrafficDeepness_data=ContentEntrance.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatContentEntrance(counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=ContentEntrance(
                                counter_id=counter,
                                date=date,
                                url=trafsum.get('url'),
                                page_views=trafsum.get('page_views'), #
                                depth=trafsum.get('depth'),
                                visit_time=trafsum.get('visit_time'),
                                )
                            newrow.save()

            #ContentExit
            try:
                last_TrafficDeepness_data=ContentExit.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatContentExit(counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=ContentExit(
                                counter_id=counter,
                                date=date,
                                url=trafsum.get('url'),
                                page_views=trafsum.get('page_views'), #
                                visits=trafsum.get('visits'), #trafsum.get('visits_percent')
                                depth=trafsum.get('depth'),
                                visit_time=trafsum.get('visit_time'),
                                )
                            newrow.save()

            #ContentTitles
            try:
                last_TrafficDeepness_data=ContentTitles.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatContentTitles(counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=ContentTitles(
                                counter_id=counter,
                                date=date,
                                page_views=trafsum.get('page_views'), #
                                name=trafsum.get('name'), #trafsum.get('visits_percent')
                                )
                            newrow.save()

            #ContentUrlParam
            try:
                last_TrafficDeepness_data=ContentUrlParam.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatContentUrlParam(counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=ContentUrlParam(
                                counter_id=counter,
                                date=date,
                                page_views=trafsum.get('page_views'), #
                                name=trafsum.get('name'), #trafsum.get('visits_percent')
                                )
                            newrow.save()


            #Geo
            try:
                last_TrafficDeepness_data=Geo.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatGeo(counter.id,date1=date,date2=date,per_page=perpage)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=Geo(
                                counter_id=counter,
                                date=date,
                                page_views=trafsum.get('page_views'), #
                                visits=trafsum.get('visits'), #trafsum.get('visits_percent')
                                depth=trafsum.get('depth'),
                                visit_time=trafsum.get('visit_time'),
                                name=trafsum.get('name'),
                                )
                            newrow.save()

            #DemographyAgeGender
            try:
                last_TrafficDeepness_data=DemographyAgeGender_Age.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatDemographyAgeGender(counter.id,date1=date,date2=date)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=DemographyAgeGender_Age(
                                counter_id=counter,
                                date=date,
                                depth=trafsum.get('depth'),
                                visit_time=trafsum.get('visit_time'),
                                visits_percent=trafsum.get('visits_percent'),
                                name=trafsum.get('name'),
                                )
                            newrow.save()
                            newrow=DemographyAgeGender_Sex(
                                counter_id=counter,
                                date=date,
                                depth=trafsum.get('depth'),
                                visit_time=trafsum.get('visit_time'),
                                visits_percent=trafsum.get('visits_percent'),
                                name=trafsum.get('name'),
                                )
                            newrow.save()

            #DemographyAgeGender
            try:
                last_TrafficDeepness_data=DemographyStructure.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatDemographyStructure(counter.id,date1=date,date2=date)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=DemographyStructure(
                                counter_id=counter,
                                date=date,
                                depth=trafsum.get('depth'),
                                visit_time=trafsum.get('visit_time'),
                                visits_percent=trafsum.get('visits_percent'),
                                name=trafsum.get('name'),
                                name_gender=trafsum.get('name_gender'),
                                )
                            newrow.save()

            #TechBrowsers
            try:
                last_TrafficDeepness_data=TechBrowsers.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatTechBrowsers(counter.id,date1=date,date2=date)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=TechBrowsers(
                                counter_id=counter,
                                date=date,
                                visits=trafsum.get('visits'),
                                depth=trafsum.get('depth'),
                                page_views=trafsum.get('page_views'),
                                visit_time=trafsum.get('visit_time'),
                                name=trafsum.get('name'),
                                version=trafsum.get('version'),
                                )
                            newrow.save()


            #TechOs
            try:
                last_TrafficDeepness_data=TechOs.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatTechOs(counter.id,date1=date,date2=date)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=TechOs(
                                counter_id=counter,
                                date=date,
                                visits=trafsum.get('visits'),
                                depth=trafsum.get('depth'),
                                page_views=trafsum.get('page_views'),
                                visit_time=trafsum.get('visit_time'),
                                name=trafsum.get('name'),
                                )
                            newrow.save()

            #TechDisplay
            try:
                last_TrafficDeepness_data=TechDisplay.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatTechDisplay(counter.id,date1=date,date2=date)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=TechDisplay(
                                counter_id=counter,
                                date=date,
                                visits=trafsum.get('visits'),
                                depth=trafsum.get('depth'),
                                page_views=trafsum.get('page_views'),
                                visit_time=trafsum.get('visit_time'),
                                name=trafsum.get('name'),
                                format=trafsum.get('format'),
                                width=trafsum.get('width'),
                                height=trafsum.get('height'),
                                )
                            newrow.save()

            #TechMobile
            try:
                last_TrafficDeepness_data=TechMobile.objects.filter(counter_id=counter.id).latest(field_name='date')
            except Exception as e:
                last_TrafficDeepness_data=[]
                print(e)
            print(last_TrafficDeepness_data,startdate)
            # pdb.set_trace()
            date2 =datetime.date.today()
            if last_TrafficDeepness_data !=[]:
                date1 =(last_TrafficDeepness_data.date+datetime.timedelta(days=1)) if last_TrafficDeepness_data.date != datetime.date.today() else None
            else:
                if startdate:
                    date1 =datetime.datetime.strptime(startdate, "%Y%m%d").date()
                else:
                    date1 =None
            print('d1:',date1,'d2:',date2)
            if date1 and date2 and date1 != date2:
                for date in (date1+datetime.timedelta(n) for n in range((date2-date1).days+1)):
                    link=True
                    print('========------')
                    while(link):
                        print('limk')
                        print(date)
                        try:
                            TrafficDeepness_data=metrika.GetStatTechMobile(counter.id,date1=date,date2=date)
                            linknew=TrafficDeepness_data.links['next'] if hasattr(TrafficDeepness_data,'links') and TrafficDeepness_data.links['next'] else False
                            link= linknew if linknew !=link else None
                            print(link)
                            subdata=TrafficDeepness_data.data_group
                            TrafficDeepness_data=TrafficDeepness_data.data
                        except Exception as e:
                            print(e)
                            TrafficDeepness_data=[]
                            link=False
                        for trafsum in TrafficDeepness_data:
                            newrow=TechMobile(
                                counter_id=counter,
                                date=date,
                                visits=trafsum.get('visits'),
                                depth=trafsum.get('depth'),
                                page_views=trafsum.get('page_views'),
                                visit_time=trafsum.get('visit_time'),
                                name=trafsum.get('name'),
                                )
                            newrow.save()
                        for trafsum in subdata:
                            newrow=TechMobile_percent(
                                counter_id=counter,
                                date=date,
                                visits=trafsum.get('visits'),
                                depth=trafsum.get('depth'),
                                page_views=trafsum.get('page_views'),
                                visit_time=trafsum.get('visit_time'),
                                name=trafsum.get('name'),
                                )
                            newrow.save()