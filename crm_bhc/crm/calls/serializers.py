from rest_framework import serializers

from .models import Call, CallQuestion
from crm.api.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name',)


class CallSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False)
    # photos = serializers.HyperlinkedIdentityField('photos', view_name='postphoto-list')
    # author = serializers.HyperlinkedRelatedField(view_name='user-detail', lookup_field='username')

    def get_validation_exclusions(self, *args, **kwargs):
        # Need to exclude `user` since we'll add that later based off the request
        exclusions = super(CallSerializer, self).get_validation_exclusions(*args, **kwargs)
        return exclusions + ['user']

    class Meta:
        model = Call


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallQuestion
