from django.apps import AppConfig


class PayanywayConfig(AppConfig):
    name = 'payanyway'
