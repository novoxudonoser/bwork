from django.apps import AppConfig


class VelosConfig(AppConfig):
    name = 'velos'
