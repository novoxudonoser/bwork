import os

from colorful.fields import RGBColorField
from django.db import models
from djangoratings.fields import RatingField

from account.models import VeloUser


class City(models.Model):
    name = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return self.name


class Arya(models.Model):
    name = models.CharField(max_length=30, blank=True, null=True)
    city = models.ForeignKey('City', on_delete=models.DO_NOTHING, related_name='aryas', blank=True, null=True)

    def __str__(self):
        return self.name


class Studio(models.Model):
    parent = models.ForeignKey('Studio', on_delete=models.DO_NOTHING, related_name='offices', null=True, blank=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=17, blank=True, null=True)
    wtime = models.CharField(max_length=255, blank=True, null=True)
    rating = RatingField(range=5)
    desc = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=True)
    reqs = models.TextField(null=True, blank=True)
    delivery = models.BooleanField(default=False)
    actions = models.TextField(null=True, blank=True)
    reviews_plus = models.PositiveIntegerField(default=0)
    reviews_minus = models.PositiveIntegerField(default=0)
    additional = models.ManyToManyField('AdditionalService', null=True, blank=True)
    user = models.ForeignKey(VeloUser, on_delete=models.DO_NOTHING, related_name='user')
    price_hour_start = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    num_men = models.IntegerField(null=True, blank=True)
    num_women = models.IntegerField(null=True, blank=True)
    num_teen = models.IntegerField(null=True, blank=True)
    num_kids = models.IntegerField(null=True, blank=True)

    city = models.ForeignKey('City', on_delete=models.DO_NOTHING, related_name='studios', blank=True, null=True)
    arya = models.ForeignKey('Arya', on_delete=models.DO_NOTHING, related_name='studios', blank=True, null=True)

    def __str__(self):
        return "{0} - {1}".format(self.pk, self.name)

    def get_all_studios(self):
        studios = [self.pk]
        offices = self.offices.all().values('id')
        if offices:
            studios += [x['id'] for x in offices]

        return studios

    def get_all_studios_qs(self):
        studios = [self.pk]
        offices = self.offices.all().values('id')
        if offices:
            studios += [x['id'] for x in offices]

        return Studio.objects.filter(pk__in=studios)

    # @property # TODO
    # def price_hour_start(self):
    #     return self.bikes.all().order_by('+tariff__price_hour').first().tariff.price_hour


class BikeModels(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class BikeMarks(models.Model):
    name = models.CharField(max_length=30)
    image = ''

    def __str__(self):
        return self.name


# colorfild
class BikeColors(models.Model):
    color = RGBColorField()
    image = ''

    def __str__(self):
        return self.color


class BikeWeelSizes(models.Model):
    num = models.PositiveIntegerField()
    image = 'img/images/colesa_26.png'

    def __str__(self):
        return str(self.num)


class BikeSizes(models.Model):
    name = models.CharField(max_length=30)
    image = 'img/images/children_b.png'

    def __str__(self):
        return self.name


class BikeGender(models.Model):
    name = models.CharField(max_length=30)
    image = 'img/images/podrostok_b.png'

    def __str__(self):
        return self.name


class BikeTypes(models.Model):
    name = models.CharField(max_length=30)
    image = 'img/images/cross_cantri.png'

    def __str__(self):
        return self.name


class BikeSpeeds(models.Model):
    num = models.PositiveIntegerField()
    image = 'img/images/16skorostey.png'

    def __str__(self):
        return str(self.num)


class BikeFrames(models.Model):
    '''rama'''
    name = models.CharField(max_length=30)
    image = 'img/images/rama_18.png'

    def __str__(self):
        return self.name


class BikeSuspensions(models.Model):
    ''' podveska '''
    name = models.CharField(max_length=30)
    image = 'img/images/dvyhpodves.png'

    def __str__(self):
        return self.name


class BikePurposes(models.Model):
    name = models.CharField(max_length=30)
    image = ''

    def __str__(self):
        return self.name


class Bike(models.Model):
    STATES = (
        ('1', 'исправен'),
        ('2', 'неполадки'),
        ('3', 'сломан'),
    )

    props = (
        'mark',
        'model',
        'gender',
        'color',
        'type',
        'speeds',
        'weelsize',
        'size',
        'frame',
        'suspension',
        'purpose',
    )

    name = models.CharField(max_length=30, blank=True, null=True)
    mark = models.ForeignKey(BikeMarks, related_name='bikes', on_delete=models.DO_NOTHING,
                             null=True, default=None)
    model = models.ForeignKey(BikeModels, related_name='bikes', on_delete=models.DO_NOTHING,
                              null=True, default=None)
    studio = models.ForeignKey(Studio, related_name='bikes', on_delete=models.DO_NOTHING)

    # chars start
    color = models.ForeignKey(BikeColors, related_name='bikes', on_delete=models.DO_NOTHING, verbose_name='цвет',
                              null=True, default=None)
    gender = models.ForeignKey(BikeGender, related_name='bikes', on_delete=models.DO_NOTHING, verbose_name='пол',
                               null=True, default=None)
    type = models.ForeignKey(BikeTypes, related_name='bikes', on_delete=models.DO_NOTHING, verbose_name='тип',
                             null=True, default=None)
    speeds = models.ForeignKey(BikeSpeeds, related_name='bikes', on_delete=models.DO_NOTHING, verbose_name='скорости',
                               null=True, default=None)
    weelsize = models.ForeignKey(BikeWeelSizes, related_name='bikes', on_delete=models.DO_NOTHING,
                                 verbose_name='размер колёс', null=True, default=None)
    size = models.ForeignKey(BikeSizes, related_name='bikes', on_delete=models.DO_NOTHING, verbose_name='размер',
                             null=True, default=None)
    frame = models.ForeignKey(BikeFrames, related_name='bikes', on_delete=models.DO_NOTHING,
                              verbose_name='тип рамы ', null=True, default=None)  # rama
    suspension = models.ForeignKey(BikeSuspensions, related_name='bikes', on_delete=models.DO_NOTHING,
                                   verbose_name='тип подвески', null=True, default=None)  # podveska
    purpose = models.ForeignKey(BikePurposes, related_name='bikes', on_delete=models.DO_NOTHING,
                                verbose_name='назначени', null=True, default=None)
    # chars end
    chars = models.ManyToManyField('Characteristic', related_name='bikes', null=True, blank=True)

    number = models.CharField(max_length=15, blank=True, null=True, verbose_name='Инвентарный номер')

    state = models.CharField(max_length=1, choices=STATES, blank=True, null=True, verbose_name='Состояние')
    busy = models.BooleanField(default=False, verbose_name='Занят')

    tariff = models.ForeignKey('Tariff', on_delete=models.DO_NOTHING, verbose_name="Тариф велосипеда")

    book = models.BooleanField(default=False)
    buy_price = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    cost = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    deposit = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    sold = models.BooleanField(default=False)
    desc = models.TextField(null=True, blank=True)

    @property
    def price(self):
        return self.tariff.price_hour

    @property
    def price_2hour(self):
        return self.tariff.price_2hour

    @property
    def price_day(self):
        return self.tariff.price_day

    @property
    def price_week(self):
        return self.tariff.price_week

    @staticmethod
    def get_repr(value):
        if callable(value):
            return '%s' % value()
        return value

    @staticmethod
    def get_field(instance, field):
        field_path = field.split('.')
        attr = instance
        for elem in field_path:
            try:
                attr = getattr(attr, elem)
            except AttributeError:
                return None
        return attr

    def get_properties(self):
        ret = dict()
        if self.gender:
            ret['gender'] = {
                'name': self.gender.name,
                'img': self.gender.image,
            }

        # if self.color:
        #     ret['color'] = {
        #         'name': self.color.color,
        #         'img': self.color.image,
        #     }

        if self.type:
            ret['type'] = {
                'name': self.type.name,
                'img': self.type.image,
            }

        if self.speeds:
            ret['speeds'] = {
                'name': self.speeds.num,
                'img': self.speeds.image,
            }

        if self.weelsize:
            ret['weelsize'] = {
                'name': self.weelsize.num,
                'img': self.weelsize.image,
            }

        if self.size:
            ret['size'] = {
                'name': self.size.name,
                'img': self.size.image,
            }

        if self.frame:
            ret['frame'] = {
                'name': self.frame.name,
                'img': self.frame.image,
            }
        if self.suspension:
            ret['suspension'] = {
                'name': self.suspension.name,
                'img': self.suspension.image,
            }
        if self.purpose:
            ret['purpose'] = {
                'name': self.purpose.name,
                'img': self.purpose.image,
            }

        return ret

    def __str__(self):
        return "{0} - {1}".format(self.pk, self.name)

    def get_main_photo_url(self):
        return "/static/img/images/Merida_RX_567.png"

    def get_main_photo_filename(self):
        return 'Merida_RX_567.png'


class Characteristic(models.Model):
    name = models.CharField(max_length=23, verbose_name='Название характеристики')

    def __str__(self):
        return self.name


class Review(models.Model):
    studio = models.ForeignKey(Studio, on_delete=models.DO_NOTHING, related_name='reviews')
    user = models.ForeignKey(VeloUser, on_delete=models.DO_NOTHING, null=True, blank=True)
    name = models.CharField(max_length=255, verbose_name='Ваше имя')
    rating = RatingField(range=5)
    date = models.DateTimeField(auto_now_add=True)
    plus = models.TextField(null=True, blank=True, verbose_name='Достоинства')
    minus = models.TextField(null=True, blank=True, verbose_name='Недостатки')
    comment = models.TextField(null=True, blank=True, verbose_name='Комментарий')


class BikeMove(models.Model):
    bike = models.ForeignKey(Bike, related_name='bike_moves')
    old_studio = models.ForeignKey(Studio, on_delete=models.DO_NOTHING, related_name='old_moves')
    new_studio = models.ForeignKey(Studio, on_delete=models.DO_NOTHING, related_name='new_moves')

    def __str__(self):
        return self.bike.name


class Tariff(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey(VeloUser, on_delete=models.DO_NOTHING)
    price_hour = models.DecimalField(max_digits=5, decimal_places=0, null=True, blank=True)
    price_2hour = models.DecimalField(max_digits=5, decimal_places=0, null=True, blank=True)
    price_day = models.DecimalField(max_digits=5, decimal_places=0, null=True, blank=True)
    price_week = models.DecimalField(max_digits=5, decimal_places=0, null=True, blank=True)

    def __str__(self):
        return "{0} ( {1}, {2}, {3}, {4} )".format(self.name, self.price_hour, self.price_2hour, self.price_day,
                                                   self.price_week)


class BikeRent(models.Model):
    bikes = models.ManyToManyField(Bike, related_name='bike_rents')
    date_start = models.DateTimeField(auto_now_add=True)
    date_end = models.DateTimeField()
    tariff = models.ForeignKey(Tariff, related_name='rents', on_delete=models.DO_NOTHING)
    manager_start = models.ForeignKey(VeloUser, on_delete=models.DO_NOTHING, related_name='managers_rents_start')
    manager_end = models.ForeignKey(VeloUser, on_delete=models.DO_NOTHING, related_name='managers_rents_end')
    price = models.DecimalField(max_digits=5, decimal_places=2)


class AdditionalService(models.Model):
    name = models.CharField(max_length=255, verbose_name='Дополнительные услуги')

    def __str__(self):
        return self.name


class JsonImage(object):
    def as_dict(self):
        if self.image:
            name = None
            split_name = os.path.split(self.image.name)

            if split_name and len(split_name) > 0:
                name = split_name[-1]
            return {
                'pk': self.pk,
                'attach': {
                    'url': self.image.url,
                    'name': name,
                },
                'model': self.__class__.__name__
            }
        return None


class StudioPhotos(models.Model, JsonImage):
    studio = models.ForeignKey(Studio, on_delete=models.DO_NOTHING, related_name='photos')
    image = models.ImageField(upload_to='studiophotos', verbose_name='Фотография студии')
    uploaded = models.DateTimeField(auto_created=True, auto_now_add=True)


class BikePhoto(JsonImage, models.Model):
    bike = models.ForeignKey(Bike, on_delete=models.DO_NOTHING, related_name='photos', null=True)
    image = models.ImageField(upload_to='bikephotos', verbose_name='Фотография студии')
    uploaded = models.DateTimeField(auto_created=True, auto_now_add=True)

    def filename(self):
        return os.path.basename(self.image.name)
