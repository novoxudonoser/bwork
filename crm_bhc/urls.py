from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin

from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtailcore import urls as wagtail_urls

admin.autodiscover()

urlpatterns = patterns('',
       url(r'crm/', include('crm.urls')),
       url(r'^cms-admin/', include(wagtailadmin_urls)),
       url(r'', include(wagtail_urls))
)

if settings.DEBUG:
    from django.views.static import serve

    urlpatterns += patterns('',
                            url(r'^(?P<path>favicon\..*)$', serve, {'document_root': settings.STATIC_ROOT}),
                            url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], serve,
                                {'document_root': settings.MEDIA_ROOT}),
                            url(r'^%s(?P<path>.*)$' % settings.STATIC_URL[1:], 'django.contrib.staticfiles.views.serve',
                                dict(insecure=True)),
                            )
