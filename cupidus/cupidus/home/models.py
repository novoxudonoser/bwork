from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

from phonenumber_field.modelfields import PhoneNumberField

from django.core.urlresolvers import reverse
# import getpaid

from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import User

"""
заявки - то что пользо
"""

# class Order(models.Model):
# 	"""
# 	процесинг платежа денег
# 	"""

# 	name = models.CharField(max_length=100)
# 	total = models.DecimalField(decimal_places=2, max_digits=8, default=0)
# 	currency = models.CharField(max_length=3, default='EUR')
# 	status = models.CharField(max_length=1, blank=True, default='W')
# 	# def get_absolute_url(self):
# 	# 	return reverse('order_detail', kwargs={'pk': self.pk})

# 	def __str__(self):
# 		return self.name

# import getpaid
# Payment = getpaid.register_to_payment(Order, unique=False, related_name='payments')

class Phones_codes(models.Model):
	code = models.CharField(max_length=30)
	phone = PhoneNumberField()
	date= models.DateTimeField(auto_now=True)

class Geo_sp_Countries(models.Model):
	"""
	страны
	"""
	name = models.CharField(max_length=30)

	def __str__(self):
		return self.name

class Geo_sp_Cities(models.Model):
	"""
	города, посёлки, деревни
	"""
	name    = models.CharField(max_length=30)
	points  = models.PositiveSmallIntegerField()
	country = models.ForeignKey(Geo_sp_Countries, related_name='country')

	def __str__(self):
		return self.name

class Geo_sp_Areas(models.Model):
	"""
	станции метро или районы
	"""
	name = models.CharField(max_length=30)
	city = models.ForeignKey(Geo_sp_Cities)

	def __str__(self):
		return self.name

class Geo_Location(models.Model):
	'''
	меcтоположение заявки или ппользователя(имя дебильное, но лучше ни чего не придумал)
	включает в себя страну город метро (иль район) и будет включать
	полигон который отрисует пользователь когда гис прикрутим
	'''
	country = models.ForeignKey(Geo_sp_Countries)
	city    = models.ForeignKey(Geo_sp_Cities)
	arya    = models.ForeignKey(Geo_sp_Areas, blank=True, null=True)
	#poligon = #область выделенная пользователем


class UserProfile(models.Model):
	user = models.ForeignKey(User, unique=True, related_name='profile')
	geo_at_register = models.ForeignKey(Geo_Location, related_name='user')  #геопопзиция при регистрации
	geo             = models.ForeignKey(Geo_Location, related_name='user1') #геопопзиция текущая
	phone = PhoneNumberField(unique=True)
	phone_active = models.BooleanField(default=False)
	email_active = models.BooleanField(default=False)


	def __str__(self):
		return self.user.username

class Contacts_sp_Type(models.Model):
	name = models.CharField(max_length=30)
	key  = models.CharField(max_length=30)

	def __str__(self):
		return self.name

class User_Contacts(models.Model):
	user         = models.ForeignKey(User)
	contact_type = models.ForeignKey(Contacts_sp_Type)
	value        = models.CharField(max_length=200)

	def __str__(self):
		return self.value


class Categorys_tree(MPTTModel):
	"""
	дерево категорий товаров
	"""
	name = models.CharField(max_length=30)
	parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

	def __str__(self):
		return self.name

	class MPTTMeta:
		order_insertion_by = ['name']

class Order_sp_Type(models.Model):
	name = models.CharField(max_length=30)

	def __str__(self):
		return self.name


REQUESTS_STATUSES = (
	(1, 'жду предложений'),
	(2, 'заявка исполнена'),
)


class Requests(models.Model):
	"""
	запросы пользователей
	"""
	user     = models.ForeignKey(User)
	status   = models.IntegerField(choices=REQUESTS_STATUSES, verbose_name='Тип обслуживания')
	price    = models.IntegerField()
	pub_date = models.DateTimeField()
	views    = models.IntegerField()


class Contacts_visibility(models.Model):
	order     = models.ForeignKey(Requests)
	user      = models.ForeignKey(User)
	contact   = models.ForeignKey(User_Contacts)
	buy_date  = models.DateTimeField()

class Order_Views_Counter(models.Model):
	order     = models.ForeignKey(Requests)
	user      = models.ForeignKey(User)
	view_date = models.DateTimeField()








