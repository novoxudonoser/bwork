from django.views.generic import View
from django.http import HttpResponse
from crm.lending_proc.models import Pfones,PfoneShown,PfoneIncomeLog
from django.http import HttpResponse,HttpResponseServerError,JsonResponse

from django.db.models import Q
from django.db import transaction
from django.db.models import Min

from datetime import datetime, timedelta

import random
import hashlib
import string

from rest_framework import generics, serializers
from rest_framework.response import Response
from django.views.generic import ListView

from crm.lending_proc.forms import ApplicationUserForm
from crm.lending_proc.models import ApplicationUser, YandexGetParametrsStatic,YandexGetParametrsDynamic
from crm.lending_proc.models import Pfones,PfoneShown, Lendings,ClientsTrack,Clients
from crm.lending_proc.models import DirectCompanes,DirectGroups,DirectAdds,DirectPhrases,DirectRegions

from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from settings import EMAIL_DEFAULT
from crm.lending_proc.models import ManagerEmail
from datetime import datetime
from furl import furl
import urllib

from django.db.models import F

PRIVATE_IPS_PREFIX = ('10.', '172.', '192.',)

def get_client_ip(request):
    """ get the client ip from the request """

    remote_address = request.META.get('REMOTE_ADDR')
    ip = remote_address

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        proxies = x_forwarded_for.split(',')
        while (len(proxies) > 0 and
                proxies[0].startswith(PRIVATE_IPS_PREFIX)):
            proxies.pop(0)
        if len(proxies) > 0:
            ip = proxies[0]
    return ip

class ApplicationUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationUser

from crm.lending_proc.models import TYPE_APPLICATION, CALCULATION_TYPE

TYPE_APPLICATION=dict(map(reversed,TYPE_APPLICATION))
CALCULATION_TYPE=dict(map(reversed,CALCULATION_TYPE))

APPLICATION_SOURSE={
    '3':'обратный звонок',
    '1':'форма сверху лендинга',
    '4':'калькулятор',
    '2':'форма снизу лендинга',
}

class CreateApplicationUser(generics.RetrieveAPIView):
    """
    сохраняем форму с лендинга
    """
    serializer_class = ApplicationUserSerializer

    def post(self, request):
        try:
            lending=Lendings.objects.get(name=request.POST.get('lending',''))
        except:
            lending=None
        result = 'error'
        ip_address = get_client_ip(request)
        try:
            client=Clients.objects.get(cookie_track=request.POST.get('coockie_track',''))
        except:
            client=None
        visit=ClientsTrack.objects.filter(client=client).latest('date') if client else None
        form = ApplicationUserForm(ip_address,lending,client,visit,request.POST)
        if form.is_valid() and lending:
            form.save()
            result = 'success'

            template = get_template('mail.txt')
            context={
                "data_lending":lending.name,
                "data_user_name":str(form.cleaned_data.get('user_name','')),
                "data_email":str(form.cleaned_data.get('email','')),
                "data_phone":str(form.cleaned_data.get('phone','')),
                "data_coment":str(form.cleaned_data.get('coment','')),
                "data_ip_address":str(form.ip_address),
                "data_application_sourse":APPLICATION_SOURSE.get(request.POST.get('sourse'),''),
                "data_type_application":TYPE_APPLICATION.get(form.cleaned_data.get('type_application'),''),
                "data_calculation_type":CALCULATION_TYPE.get(form.cleaned_data.get('calculation_type'),''),
                "data_calculation_documents":str((lambda x: x or 0)(form.cleaned_data.get('calculation_documents',''))),
                "data_calculation_employees":str((lambda x: x or 0)(form.cleaned_data.get('calculation_employees',''))),
                "data_calculation_cost":str((lambda x: x or 0)(form.cleaned_data.get('calculation_cost',''))),
                "data_created":str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            }
            content = template.render(Context(context))
            email=form.cleaned_data.get('email','')
            headers={'Reply-To':email} if email else {}
            msg = EmailMultiAlternatives('заявка', content, EMAIL_DEFAULT, [manager.email for manager in ManagerEmail.objects.filter(active=True)],headers=headers)
            msg.send()

            cookie=request.POST.get('cookie_phone','')
            if cookie:
                try:
                    pfone=Pfones.objects.filter(active=False).get(cookie=cookie)
                except:
                    pfone=None
                if pfone:
                    pfone.active=True
                    pfone.save()
                try:
                    log=PfoneShown.objects.get(cookie=cookie)
                except:
                    log=None
                if log:
                    log.active=False
                    log.save()
            if client:
                client.apped=True
                client.save()
        else:
            result = form.errors
            result = result if result else 'error'
        return Response(result)

from crm.lending_proc.models import DIRECT_SOURCE_TYPE,DIRECT_ADDPHRASES,DIRECT_POSITION_TYPE,DIRECT_CAMPAIGN_TYPE,DIRECT_DEVICE_TYPE

DIRECT_SOURCE_TYPE   = dict(map(reversed,DIRECT_SOURCE_TYPE))
DIRECT_ADDPHRASES    = dict(map(reversed,DIRECT_ADDPHRASES))
DIRECT_POSITION_TYPE = dict(map(reversed,DIRECT_POSITION_TYPE))
DIRECT_CAMPAIGN_TYPE = dict(map(reversed,DIRECT_CAMPAIGN_TYPE))
DIRECT_DEVICE_TYPE   = dict(map(reversed,DIRECT_DEVICE_TYPE))

import hashlib

def save_yandex_getparams(url,visit):
    ad_id          = (lambda x:int(x) if x and x.isdigit() else None)(url.args.get('ad_id') or url.args.get('banner_id'))
    ad             = DirectAdds.objects.filter(ad_id=ad_id)
    ad_id          = ad[0] if ad else DirectAdds.objects.create(ad_id=ad_id) if ad_id else None
    addphrases     = DIRECT_ADDPHRASES.get(url.args.get('addphrases'))
    addphrasestext = url.args.get('addphrasestext')
    campaign_type  = DIRECT_CAMPAIGN_TYPE.get(url.args.get('campaign_type'))
    campaign_id    = (lambda x:int(x) if x and x.isdigit() else None)(url.args.get('campaign_id'))
    campaign       = DirectCompanes.objects.filter(campaign_id=campaign_id)
    campaign_id    = campaign[0] if campaign else DirectCompanes.objects.create(campaign_id=campaign_id,leding=visit.lending) if campaign_id else None
    device_type    = DIRECT_DEVICE_TYPE.get(url.args.get('device_type'))
    gbid_id        = (lambda x:int(x) if x and x.isdigit() else None)(url.args.get('gbid'))
    gbid           = DirectGroups.objects.filter(gbid=gbid_id)
    gbid           = gbid[0] if gbid else DirectGroups.objects.create(gbid=gbid_id) if gbid_id else None
    keyword        = url.args.get('keyword')
    phrase_id      = (lambda x:int(x) if x and x.isdigit() else None)(url.args.get('phrase_id'))
    phrase         = DirectPhrases.objects.filter(phrase_id=phrase_id)
    phrase_id      = phrase[0] if phrase else DirectPhrases.objects.create(phrase_id=phrase_id) if phrase_id else None
    retargeting_id = (lambda x:int(x) if x and x.isdigit() else None)(url.args.get('retargeting_id'))
    adtarget_id    = (lambda x:int(x) if x and x.isdigit() else None)(url.args.get('adtarget_id'))
    position       = (lambda x:int(x) if x and x.isdigit() else None)(url.args.get('position'))
    position_type  = DIRECT_POSITION_TYPE.get(url.args.get('position_type'))
    source         = url.args.get('source')
    source_type    = DIRECT_SOURCE_TYPE.get(url.args.get('source_type'))
    region_name    = url.args.get('region_name')
    region_id      = (lambda x:int(x) if x and x.isdigit() else None)(url.args.get('region_id'))
    region         = DirectRegions.objects.filter(region_id=region_id)
    region_id      = region[0] if region else DirectRegions.objects.create(region_id=region_id) if region_id else None
    yclid          = (lambda x:x if x and x.isdigit() else None)(url.args.get('yclid'))
    adtarget_name  = url.args.get('adtarget_name')

    paramsdynamic = YandexGetParametrsDynamic(
        visit          = visit,
        addphrases     = addphrases,
        addphrasestext = addphrasestext,
        device_type    = device_type,
        retargeting_id = retargeting_id,
        adtarget_id    = adtarget_id,
        adtarget_name  = adtarget_name,
        position       = position,
        position_type  = position_type,
        yclid          = yclid,
        )
    paramsdynamic.save()

    paramsstat = YandexGetParametrsStatic(
        ad_id          = ad_id,
        campaign_type  = campaign_type,
        campaign_id    = campaign_id,
        gbid           = gbid,
        keyword        = keyword,
        phrase_id      = phrase_id,
        source         = source,
        source_type    = source_type,
        region_name    = region_name,
        region_id      = region_id,
        )
    paramsstat.hash=paramsstat.calchash()
    param=YandexGetParametrsStatic.objects.filter(hash=paramsstat.hash)
    if param:
        visit.getparamstat=param[0]
        visit.save()
    else:
        paramsstat.save()
        visit.getparamstat=paramsstat
        visit.save()




def save_getparams(url,visit):
    if url.args.get('source_type') == 'direct':
        save_yandex_getparams(url,visit)


class Trackclient(generics.RetrieveAPIView):
    def get(self, request, *args, **kwargs):
        if self.request.method == 'GET':
            try:
                lending=Lendings.objects.get(name=request.GET.get('lending',''))
            except:
                lending=None
            if lending:
                url=request.GET.get('url', '')
                if url[-1:] == '/':
                    url=url[:-1]
                url=furl(url)

                ip_address = get_client_ip(request)
                try:
                    client=Clients.objects.get(cookie_track=request.GET.get('coockie_track',''))
                except:
                    client=None
                if client:
                    visit=ClientsTrack.objects.create(client=client,lending=lending,url=str(url),date=datetime.now(),ip_address=ip_address)
                    client.visits = F('visits') +1
                    client.save()
                    save_getparams(url,visit)
                    return JsonResponse({})
                else:
                    cookie_track=''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))
                    client=Clients(cookie_track=cookie_track,date=datetime.now(),lending=lending,visits=1)
                    client.save()
                    visit=ClientsTrack.objects.create(client=client,lending=lending,url=str(url),date=datetime.now(),ip_address=ip_address)
                    save_getparams(url,visit)
                    return JsonResponse({"addcoocke":cookie_track})
            else:
                response =  HttpResponseServerError()



class PfoneNum(View):
    """
    выдаём номера и печеньки
    """
    def get(self, request, *args, **kwargs):
        url=furl(request.GET.get('url', ''))
        utm=url.args.get('source','').split('/')[0] or url.args.get('source_type','').split('/')[0]
        now  = datetime.now()
        phones=None
        pfone=None
        addcoocke=''
        coocke=request.GET.get('cookie_phone','')
        if coocke:
            phones = Pfones.objects.filter(active=False).filter(cookie=coocke).filter(is_default=False)
            if phones:
                phone=str(phones[0].number) if phones else ''
                return JsonResponse({"phone":phone})
            else:
                phone = Pfones.objects.filter(is_default=True)
                phone = str(phone[0].number) if phone else ''
                import pdb
                pdb.set_trace()
                return JsonResponse({"phone":phone})
        newcoocke=''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))
        try:
            lending=Lendings.objects.get(name=request.GET.get('lending',''))
        except:
            lending=None
        try:
            client=Clients.objects.get(cookie_track=request.GET.get('coockie_track',''))
        except:
            client=None
        if lending:
            with transaction.atomic():
                phones = Pfones.objects.filter(active=True).filter(compane__name=utm).filter(is_default=False).filter(lending=lending)
                if phones:
                    pfone = random.choice(phones)
                    pfone.active = False
                    pfone.expires_date = now
                    pfone.cookie = newcoocke
                    pfone.save()
            if not phones:
                with transaction.atomic():
                    pfone = Pfones.objects.order_by('expires_date').filter(compane__name=utm).filter(is_default=False).filter(lending=lending)[:1]
                    if pfone:
                        pfone=pfone[0]
                        try:
                            log=PfoneShown.objects.get(cookie=pfone.cookie)
                            log.active=False
                            log.save()
                        except:
                            pass
                        pfone.active = False
                        pfone.expires_date = now
                        pfone.cookie = newcoocke
                        pfone.save()
        if pfone:
            PfoneShown.objects.create(phone = pfone,shown_date=datetime.now(),cookie=newcoocke,client=client)
            addcoocke = newcoocke
            phone = str(pfone.number)
            return JsonResponse({"phone":phone,"addcoocke":newcoocke})
        else:
            phone = Pfones.objects.filter(is_default=True)
            phone = str(phone[0].number) if phone else ''
            return JsonResponse({"phone":phone})


class PfoneLogProc(View):
    """
     логируем звонки из sip провайдера
    """
    def get(self, request, *args, **kwargs):
        """
        {'waiting_duration': ['0'], 'tp_project': ['uis2'], 'segment': ['[]'],
        'start_time': ['1451150199'], 'call_hd': ['703332637'], 'scenario_id': ['19256'],
        'event_timestamp': ['1451150199'], 'app_id': ['19297'], 'session_cdr_id': ['93714399'],
        'numa': ['79525492130'], 'numb': ['74957772838'], 'scenario_name': ['офис_01'], 'cdr_id': ['93714399'],
        'segment_id_list': ['[]'], 'call_time': ['2015-12-26 20:16:39.063174'], 'event': ['incoming_call']}
        """

        number_from=request.GET.get('numa', '')
        number_from='+'+number_from[0] if number_from and type(number_from) == list and number_from[:1] else '+'+number_from

        number_to=request.GET.get('numb', '')
        number_to='+'+number_to[0] if number_to and type(number_to) == list and number_to[:1] else '+'+number_to

        call_time=request.GET.get('call_time', '')
        call_time=datetime.strptime(call_time,"%Y-%m-%d %H:%M:%S.%f") if call_time and type(call_time) == list and call_time[:1] else datetime.strptime(call_time,"%Y-%m-%d %H:%M:%S.%f")

        pfone=Pfones.objects.filter(number=number_to)
        if pfone:
            pfoneshown = PfoneShown.objects.filter(phone=pfone).filter(shown_date__lt=call_time).filter(active=True).order_by('-shown_date')[:1]
            visit=ClientsTrack.objects.filter(client=pfoneshown[0].client).latest('date') if pfoneshown else None
            PfoneIncomeLog.objects.create(phone_to=number_to,phone_from=number_from,income_date=call_time,cookie=pfoneshown[0].cookie if pfoneshown else '',client=pfoneshown[0].client if pfoneshown else None,lending=pfone[0].lending if pfone else None,visit=visit)
            response = HttpResponse()
            return response
        else:
            #response =  HttpResponseServerError('<h1>Server Error (500)</h1>')
            response = HttpResponse()
            return response
