from django.contrib import admin
from cupidus.home.models import Requests,Categorys_tree
from django_mptt_admin.admin import DjangoMpttAdmin

class Categorys_tree_Admin(DjangoMpttAdmin):
        pass

admin.site.register(Requests)
admin.site.register(Categorys_tree,Categorys_tree_Admin)

from cupidus.home.models import Geo_sp_Countries,Geo_sp_Cities,Geo_sp_Areas,Order_sp_Type,Requests
from cupidus.home.models import Geo_Location,User,Contacts_sp_Type,User_Contacts,Contacts_visibility
from cupidus.home.models import Order_Views_Counter,UserProfile,Phones_codes

admin.site.register(Geo_sp_Countries)
admin.site.register(Geo_sp_Cities)
admin.site.register(Geo_sp_Areas)
admin.site.register(Order_sp_Type)
admin.site.register(Geo_Location)
admin.site.register(User_Contacts)
admin.site.register(Contacts_visibility)
admin.site.register(Order_Views_Counter)
admin.site.register(Contacts_sp_Type)
admin.site.register(UserProfile)
admin.site.register(Phones_codes)

# from getpaid.admin import PaymentAdmin
# from getpaid.models import PaymentManager

# admin.site.register(PaymentManager,PaymentAdmin)
# from models import Country
# admin.site.register(Country, CountryAdmin)