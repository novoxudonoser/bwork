from django import forms
from django.contrib.auth import get_user_model

from velos.models import Studio, Bike, Tariff


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])

        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ('email', 'phonenumber', 'patronym')


class PersonalDataForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = (
            'first_name',
            'last_name',
            'patronym',
            'email',
            'phonenumber',
            'preferred_contact_method',
            'subscribed_to_newsletter'
        )


class StudioDescUpdateForm(forms.ModelForm):
    class Meta:
        model = Studio
        fields = (
            'additional',
            'desc',
        )


class StudioContactsUpdateForm(forms.ModelForm):
    class Meta:
        model = Studio
        fields = (
            'address',
            'phone',
        )


class BikeForm(forms.ModelForm):
    # photos =
    class Meta:
        model = Bike
        fields = ('name',
                  'desc',
                  'tariff',
                  'studio',
                  'mark',
                  'model',
                  'gender',
                  'color',
                  'type',
                  'speeds',
                  'weelsize',
                  'size',
                  'frame',
                  'suspension',
                  'purpose',
                  'chars',
                  )

    def __init__(self, *args, **kwargs):
        super(BikeForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field not in ['name', 'studio']:
                self.fields[field].required = False


class TariffForm(forms.ModelForm):
    class Meta:
        model = Tariff
        exclude = ('user',)