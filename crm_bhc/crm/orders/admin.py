__author__ = 'tramX'
from django.db import models
from django.contrib import admin

from .models import AdditionalInfoAddress, ContactPhone, OrderAddress, OrderContact, OrderDocument, Order, OrderStatus


class ContactPhoneAdmin(admin.ModelAdmin):
    list_display = ['phone', 'email']


admin.site.register(AdditionalInfoAddress)
admin.site.register(ContactPhone, ContactPhoneAdmin)
admin.site.register(OrderAddress)
admin.site.register(OrderDocument)
admin.site.register(OrderContact)
admin.site.register(Order)
admin.site.register(OrderStatus)