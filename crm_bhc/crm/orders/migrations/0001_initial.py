# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('calls', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalInfoAddress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_address', models.IntegerField(choices=[(11, 'Дом'), (12, 'Владение'), (13, 'Домовладение'), (21, 'Корпус'), (22, 'Строение'), (23, 'Литера'), (24, 'Сооружение'), (25, 'Участок'), (31, 'Офис'), (32, 'Помещение'), (33, 'Квартира'), (34, 'Комната'), (35, 'Бокс')])),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='ContactPhone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('additional_number', models.PositiveSmallIntegerField(null=True, verbose_name='Добавочный', blank=True)),
                ('phone', models.CharField(null=True, max_length=17, verbose_name='Телефон', default=None, blank=True)),
                ('email', models.EmailField(null=True, max_length=255, verbose_name='Почта', default=None, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(null=True, max_length=13, verbose_name='Номер заказа', default=None, blank=True)),
                ('date_created', models.DateField(verbose_name='Дата создания')),
                ('date_updated', models.DateTimeField(verbose_name='Дата обновления', auto_now=True)),
                ('payment_type', models.IntegerField(verbose_name='Способ оплаты', choices=[(1, 'Безналичная оплата'), (2, 'Наличная оплата')], default=1)),
                ('payment_name', models.CharField(null=True, max_length=255, verbose_name='Наименование плательщика', blank=True)),
                ('payment_inn', models.CharField(null=True, max_length=255, verbose_name='ИНН плательщика', blank=True)),
                ('payment_kpp', models.CharField(null=True, max_length=255, verbose_name='КПП плательщика', blank=True)),
                ('delivery_type', models.IntegerField(null=True, verbose_name='Способ доставки', choices=[(1, 'Курьером'), (2, 'Самовывозом')], blank=True)),
            ],
            options={
                'verbose_name': 'Заказ',
                'ordering': ['-date_created', '-date_updated'],
                'verbose_name_plural': 'Заказы',
            },
        ),
        migrations.CreateModel(
            name='OrderAddress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Адрес')),
                ('street', models.CharField(null=True, max_length=100, verbose_name='Улица', blank=True)),
                ('type_house', models.IntegerField(null=True, default=1, choices=[(1, 'Дом'), (2, 'Владение'), (3, 'Домовладение')], blank=True)),
                ('house', models.CharField(null=True, max_length=50, verbose_name='Дом', blank=True)),
                ('type_place', models.IntegerField(null=True, default=1, choices=[(1, 'Корпус'), (2, 'Строение'), (3, 'Литера'), (4, 'Сооружение'), (5, 'Участок')], blank=True)),
                ('place', models.CharField(null=True, max_length=50, blank=True)),
                ('type_appartament', models.IntegerField(null=True, default=1, choices=[(1, 'Офис'), (2, 'Помещение'), (3, 'Квартира'), (4, 'Комната'), (5, 'Бокс')], blank=True)),
                ('appartament', models.CharField(null=True, max_length=50, verbose_name='Офис', blank=True)),
                ('city', models.ForeignKey(default=1, verbose_name='Город, нас.пункт', to='api.City')),
                ('metro', models.ForeignKey(null=True, blank=True, verbose_name='Станция метро', to='api.SubwayStantions')),
            ],
        ),
        migrations.CreateModel(
            name='OrderContact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Контактное лицо', default=1)),
                ('address', models.ManyToManyField(null=True, verbose_name='Адреса', related_name='contacts', to='orders.OrderAddress', blank=True)),
                ('phones', models.ManyToManyField(verbose_name='Телефоны', related_name='phone_contacts', to='orders.ContactPhone')),
            ],
        ),
        migrations.CreateModel(
            name='OrderDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_complete', models.DateField(null=True, verbose_name='Дата готовности документа', blank=True)),
                ('firm', models.CharField(null=True, max_length=255, verbose_name='Название компании', blank=True)),
                ('fname_ip', models.CharField(null=True, max_length=255, verbose_name='Фамилия ИП', blank=True)),
                ('lname_ip', models.CharField(null=True, max_length=255, verbose_name='Имя ИП', blank=True)),
                ('mname_ip', models.CharField(null=True, max_length=255, verbose_name='Отчество ИП', blank=True)),
                ('inn', models.CharField(null=True, max_length=255, verbose_name='ИНН', blank=True)),
                ('ogrn', models.CharField(null=True, max_length=255, verbose_name='ОГРН', blank=True)),
                ('count', models.PositiveIntegerField(verbose_name='Количество документов')),
                ('is_deleted', models.BooleanField(verbose_name='Активность', default=False)),
                ('route', models.CharField(null=True, max_length=255, verbose_name='Номер маршрута', blank=True)),
                ('address', models.ForeignKey(null=True, related_name='order_addresses', default=None, blank=True, verbose_name='Адрес клиента', to='orders.OrderAddress')),
                ('question', models.ForeignKey(related_name='order_documents', verbose_name='Вопрос звонка', to='calls.CallQuestion')),
                ('theme', models.ForeignKey(editable=False, related_name='order_themes', default=7, verbose_name='Тема звонка', to='calls.CallTheme')),
            ],
        ),
        migrations.CreateModel(
            name='OrderStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Статус заказа', unique=True)),
            ],
            options={
                'verbose_name': 'Статус заказа',
                'verbose_name_plural': 'Статусы заказов',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='address',
            field=models.ForeignKey(null=True, related_name='orders', blank=True, verbose_name='Адрес', to='orders.OrderAddress'),
        ),
        migrations.AddField(
            model_name='order',
            name='client',
            field=models.ForeignKey(null=True, related_name='client_orders', default=None, blank=True, verbose_name='Клиент', to='orders.OrderContact'),
        ),
        migrations.AddField(
            model_name='order',
            name='documents',
            field=models.ManyToManyField(verbose_name='Документы в заказе', related_name='document_order', to='orders.OrderDocument'),
        ),
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.ForeignKey(null=True, blank=True, verbose_name='Статус', to='orders.OrderStatus'),
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(null=True, related_name='orders', blank=True, verbose_name='Оператор', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='additionalinfoaddress',
            name='order_address',
            field=models.ForeignKey(related_name='additional_data', to='orders.OrderAddress'),
        ),
    ]
