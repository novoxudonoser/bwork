import json

from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from crm.api.models import AddrObj, City, User
from crm.orders.mixins import RouteQuerysetMixin

__author__ = 'PekopT'
from rest_framework import generics, permissions, serializers, status, response

from .models import OrderDocument, Order, OrderAddress, OrderContact
from .serializers import OrderClientSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderContact


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAddress


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        exclude = ('user', 'client', 'documents', 'date_created', 'status')


class DocumentSerializer(serializers.ModelSerializer):
    document_order = OrderSerializer()
    address = AddressSerializer()

    class Meta:
        model = OrderDocument


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City


class AddrObjSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddrObj


class OrderDocumentList(generics.ListAPIView):
    model = OrderDocument
    serializer_class = DocumentSerializer
    permission_classes = [
        permissions.DjangoModelPermissions
    ]

    def get_queryset(self):
        """
            filtering
        """
        qs = RouteQuerysetMixin()
        qs.request = self.request
        qs = qs.get_queryset()
        qs = qs.values('id',
                       'document_order__number',
                       'document_order__id',
                       'address__name',
                       'route',
                       'document_order__client__name',
                       'document_order__id')

        queryset = OrderDocument.objects.filter(pk__in=[x['id'] for x in qs])
        return queryset


class ClientRequisites(generics.RetrieveAPIView):
    model = Order
    serializer_class = OrderClientSerializer

    def retrieve(self, request):
        id_contact = request.GET.get('id_contact')

        if id_contact:
            try:
                client_order = Order.objects \
                    .filter(client__id=int(id_contact), payment_name__isnull=False) \
                    .exclude(payment_name='').order_by('-id') \
                    .values('payment_name', 'payment_inn', 'payment_kpp')[0]
                # serializer = OrderSerializer(client_order, many=False)
                return Response(client_order)
            except IndexError:
                pass

        return Response([], status=status.HTTP_302_FOUND)


class GetAddrObject(generics.ListAPIView):
    serializer_class = AddrObjSerializer

    def get_queryset(self):
        name_street = self.request.GET.get('street', None)

        addresses = AddrObj.objects \
            .filter(regioncode=77, offname__contains=name_street,
                    placecode='000', citycode='000')

        return addresses


class ExcellentAddNewCity(generics.CreateAPIView):
    serializer_class = CitySerializer

    def create(self, request, *args, **kwargs):
        try:
            return super(ExcellentAddNewCity, self).create(request, *args, **kwargs)
        except ValidationError:
            exist = City.objects.get(name=self.request.POST.get('name'))
            return response.Response(CitySerializer(exist).data,
                                     status=status.HTTP_302_FOUND)


class OrderAddressList(generics.ListAPIView):
    model = OrderAddress
    serializer_class = AddressSerializer
    permission_classes = [
        permissions.DjangoModelPermissions
    ]

    def get_queryset(self):
        qs = OrderAddress.objects.all()

        if self.request.GET.get('client'):
            qs = qs.filter(order_addresses__document_order__client__pk=self.request.GET.get('client'))
            qs.query.group_by = ['order_addresses__document_order__client__pk']
        return qs


class OrderContactPerson(generics.RetrieveAPIView):
    def get(self, request):

        client = request.GET.get('client')
        result = 'no_data'
        client_name = ''
        list_phones = []
        list_orders = []
        list_address = []

        if client:
            client = OrderContact.objects.get(id=client)
            client_name = client.name
            phones = client.phones.all()
            for p in phones:
                list_phones.append({
                    'id': p.id,
                    'additional_number': p.additional_number,
                    'phone': p.phone,
                    'email': p.email
                })

            for ord in client.client_orders.all():
                status = ''
                if ord.status:
                    status = ord.status.name
                documents_list = []
                for d in ord.documents.all():
                    if d.date_complete:
                        dc = d.date_complete.strftime('%d.%m.%Y')
                    else:
                        dc = ''
                    if d.address and d.address.name:
                        address = d.address.name
                    else:
                        address = '-'
                    documents_list.append({
                        'id': d.id,
                        'questions': d.get_questions(),
                        'date_complete': dc,
                        'inn': d.inn,
                        'ogrn': d.ogrn,
                        'count': d.count,
                        'address': address
                    })

                list_orders.append({
                    'id': ord.id,
                    'number': ord.number,
                    'created': ord.date_created.strftime('%d.%m.%Y'),
                    'complete': ord.date_complete,
                    'documents_list': documents_list,
                    'status': status,
                    'documents_count': ord.documents.count()
                })
            for addr in client.address.all():
                list_address.append({
                    'name': addr.name,
                    'id': addr.id
                })
            result = 'success'

        data = {
            'result': result,
            'client_name': client_name,
            'list_phones': list_phones,
            'list_orders': list_orders,
            'list_address': list_address
        }

        return HttpResponse(json.dumps(data))


class OrderDocumentDetail(generics.UpdateAPIView):
    model = OrderDocument
    serializer_class = DocumentSerializer
    queryset = OrderDocument.objects.all()
    permission_classes = [
        permissions.DjangoModelPermissions
    ]
