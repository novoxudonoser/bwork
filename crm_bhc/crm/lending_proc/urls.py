from django.conf.urls import patterns, url

from crm.lending_proc.views import PfoneLogProc,PfoneNum,CreateApplicationUser,Trackclient
from crm.lending_proc.views2 import VisitsListView,QerysView,QerysDetailView,CallsView,AppsView

urlpatterns = patterns('',
   url(r'^log', PfoneLogProc.as_view()),
   url(r'^phoneapi', PfoneNum.as_view()),
   url(r'^application_user_form/$', CreateApplicationUser.as_view(), name='application-user-form'),
   url(r'^track', Trackclient.as_view()),  

   url(r'^visits/?$', VisitsListView.as_view(),name='visits'),
   url(r'^qerys/?$', QerysView.as_view(),name='qerys'),
   url(r'^qerysdetail/?$', QerysDetailView.as_view(),name='qerysdetail'),
   url(r'^apps/?$', AppsView.as_view(),name='apps'),
   url(r'^calls/?$', CallsView.as_view(),name='calls'),
)