from django import template

register = template.Library()

@register.inclusion_tag('form.html',takes_context=True)
def show_form(context):
    return {
        'MNT_ID'            :context['MNT_ID'],
        # 'MNT_TRANSACTION_ID':context['MNT_TRANSACTION_ID'],
        # 'MNT_CURRENCY_CODE' :context['MNT_CURRENCY_CODE'],
        # 'MNT_AMOUNT'        :context['MNT_AMOUNT'],
        # 'MNT_TEST_MODE'     :context['MNT_TEST_MODE'],
        # 'MNT_SUCCESS_URL'   :context['MNT_SUCCESS_URL'],
        # 'MNT_FAIL_URL'      :context['MNT_FAIL_URL'],
        # 'MNT_RETURN_URL'    :context['MNT_RETURN_URL'],
        # 'MNT_INPROGRESS_URL':context['MNT_INPROGRESS_URL'],
        }