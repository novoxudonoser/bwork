from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^example/', include('example.urls')),
                       url(r'', include('cupidus.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^angular/', include('djangular.urls'))
                       )

if settings.DEBUG:
    from django.views.static import serve

    urlpatterns += patterns('',
                            url(r'^(?P<path>favicon\..*)$', serve, {'document_root': settings.STATIC_ROOT}),
                            url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], serve,
                                {'document_root': settings.MEDIA_ROOT}),
                            url(r'^%s(?P<path>.*)$' % settings.STATIC_URL[1:], 'django.contrib.staticfiles.views.serve',
                                dict(insecure=True)),
                            )
