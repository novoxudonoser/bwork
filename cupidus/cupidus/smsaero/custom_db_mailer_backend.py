from dbmail.backends.mail import Sender as SenderBase
from cupidus.smsaero.tools import send_sms
from settings import SMSAERO_USER,SMSAERO_PASSWORD_MD5,SMSAERO_DEFAULT_SIGNATURE

class Sender(SenderBase):
    # channels/recipients processing
    def _get_recipient_list(self, recipient):
        if not isinstance(recipient, list) and '+' not in recipient:
            return self._group_emails(recipient)
        return self._email_to_list(recipient)

    # send message
    def _send(self):
        message=self._render_template(self._template.message, self._kwargs)
        for recipient in self._recipient_list:
            send_sms(SMSAERO_USER,SMSAERO_PASSWORD_MD5,recipient,message,self._subject)



class SenderDebug(Sender):
    """
    Print message to stdout when DEBUG is True
    """
    def _send(self):
        self.debug('Message', self._message)


# helper function, which will be used on code
def send_db_smsaero(slug, *args, **kwargs):
    from dbmail import db_sender

    kwargs['backend'] = 'cupidus.smsaero.custom_db_mailer_backend'
    db_sender(slug, *args, **kwargs)