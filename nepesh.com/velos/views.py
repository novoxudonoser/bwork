import json

from django.core.urlresolvers import reverse
from django.db.models import Avg, Count
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views import generic
from djangoratings.fields import Rating
from hitcount.views import HitCountDetailView

from velos.forms import ReviewForm

from velos.models import Bike, Studio, Review, BikePhoto, StudioPhotos
from velos.models import BikeModels, BikeMarks, BikeColors, BikeWeelSizes, BikeSpeeds, BikeFrames, BikeSuspensions
from velos.models import AdditionalService, Characteristic, BikePurposes, City, Arya, BikeGender

from django.db.models import Max,Min


from settings import FILTER_PRICE_STEP

import json



class BikeFiltering(generic.ListView):
    template_name = 'filter/bike_filtering.html'
    model = Bike

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)


        context['price_start']= int(Bike.objects.all().aggregate(Min('tariff__price_hour'))['tariff__price_hour__min'])
        context['price_step'] = FILTER_PRICE_STEP
        context['price_end']  = int(Bike.objects.all().aggregate(Max('tariff__price_hour'))['tariff__price_hour__max'])

        context['colors']=BikeColors.objects.all()
        context['weelsizes']=BikeWeelSizes.objects.all().order_by('num')
        context['speeds']=BikeSpeeds.objects.all().order_by('num')
        context['chars'] = Characteristic.objects.all()
        return context

    def get_queryset(self):
        queryset = Bike.objects.all()[:20]
        return queryset        

class StudioFiltering(generic.ListView):
    template_name = 'filter/studios_filtering.html'
    model = Studio

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['price_start']= int(Studio.objects.all().aggregate(Min('price_hour_start'))['price_hour_start__min'])
        context['price_step'] = FILTER_PRICE_STEP
        context['price_end']  = int(Studio.objects.all().aggregate(Max('price_hour_start'))['price_hour_start__max'])
        context['additional'] = AdditionalService.objects.all()

        return context

    def get_queryset(self):
        queryset = Studio.objects.all()[:20]
        return queryset        


class Main(generic.ListView):
    model = Bike
    template_name = 'filter/index.html'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        bike_filter={}
        data={}

        marks = BikeMarks.objects.all()
        bike_filter['marks']=[ {'id':mark.pk,'name':mark.name} for mark in marks ]

        models = BikeModels.objects.all()
        bike_filter['models']=[ {'id':model.pk,'name':model.name} for model in models ]

        frames = BikeFrames.objects.all()
        bike_filter['frames']=[ {'id':frame.pk,'name':frame.name} for frame in frames ]

        suspensions = BikeSuspensions.objects.all()
        bike_filter['suspension']=[ {'id':suspension.pk,'name':suspension.name} for suspension in suspensions ]

        purposes = BikePurposes.objects.all()
        bike_filter['purpose']=[ {'id':purpose.pk,'name':purpose.name} for purpose in purposes ]        

        citys = City.objects.all()
        data['citys']=[ {'id':city.pk,'name':city.name,'aryas':[{'id':arya.pk,'name':arya.name} for arya in city.aryas.all()]} for city in citys ]

        genders=BikeGender.objects.all()
        bike_filter['gender']=[ {'id':gender.pk,'name':gender.name} for gender in genders ]

        context['data']=data
        context['bike_filter'] = bike_filter
        return context

def makeid(x):
    id=x[0][3:]
    if x[1] and id and id.isdigit():
        return int(id)

class StudioFilter(generic.ListView):
    template_name = 'filter/studio_list.html'

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    def get_queryset(self):
        data=json.loads(self.request.body.decode('utf-8'))

        queryset = Studio.objects.all()

        additional=data.get('additional')
        if additional:
            ids=[makeid(item) for item in additional.items() if makeid(item)]
            additional=[AdditionalService.objects.get(pk=pk) for pk in ids]
            if additional:
                queryset=queryset.filter(additional__in=additional)

        woman_num=data.get('woman_num')
        if woman_num:
            queryset=queryset.filter(num_women__gte=woman_num['name'])

        men_num  = data.get('men_num')
        if men_num:
            queryset=queryset.filter(num_men__gte=men_num['name'])

        geo=data.get('geo')
        if geo:
            region = geo.get('region')
            if region:
                region=Arya.objects.get(pk=region['id'])
                queryset=queryset.filter(arya=region)
            else:
                city = geo.get('city')
                if city:
                    city=City.objects.get(pk=city['id'])
                    queryset=queryset.filter(city=city)

        price=data.get('price')
        if price:
            queryset=queryset.filter(price_hour_start__range=[price[0],price[1]])

        dostavka=data.get('geo',{}).get('dostavka')
        if dostavka:
            queryset=queryset.fil

        order=data.get('order',{})
        if order:
            order_id=order.get('id')
        # if order_id == 1:
        #     # queryset=queryset.extra(select={'rating': '((100/%s*rating_score/(rating_votes+%s))+100)/2' % (Studio.rating.range, Studio.rating.weight)})
        #     # queryset = queryset.order_by('-rating')
        # if order_id == 2:
        #     queryset = queryset.order_by('-rating')




        queryset=queryset[:20]
        return queryset


class BikeFilter(generic.ListView):
    template_name = 'filter/bike_list.html'

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    def get_queryset(self):
        data=json.loads(self.request.body.decode('utf-8'))

        queryset = Bike.objects.all()

        chars=data.get('chars')
        if chars:
            ids=[makeid(item) for item in chars.items() if makeid(item)]
            chars=[Characteristic.objects.get(pk=pk) for pk in ids]
            if chars:
                queryset=queryset.filter(chars__in=chars)

        colors=data.get('colors')
        if colors:
            ids=[makeid(item) for item in colors.items() if makeid(item)]
            if ids:
                queryset=queryset.filter(color_id__in=ids)

        frame=data.get('frame')
        if frame:
            id=frame.get('id')
            if id:
                queryset=queryset.filter(frame_id=id)

        mark=data.get('mark')
        if mark:
            id=mark.get('id')
            if id:
                queryset=queryset.filter(mark_id=id)


        model=data.get('model')
        if model:
            id=model.get('id')
            if id:
                queryset=queryset.filter(model_id=id)

        price=data.get('price')
        if price:
            queryset=queryset.filter(tariff__price_hour__range=[price[0],price[1]])


        speeds=data.get('speeds')
        if speeds:
            ids=[makeid(item) for item in speeds.items() if makeid(item)]
            if ids:
                queryset=queryset.filter(speeds_id__in=ids)

        weelsizes=data.get('weelsizes')
        if weelsizes:
            ids=[makeid(item) for item in weelsizes.items() if makeid(item)]
            if ids:
                queryset=queryset.filter(weelsize_id__in=ids)

        suspension=data.get('suspension')
        if suspension:
            id=suspension.get('id')
            if id:
                queryset=queryset.filter(suspension_id=id)

        geo=data.get('geo')
        if geo:
            region = geo.get('region')
            if region:
                region=Arya.objects.get(pk=region['id'])
                queryset=queryset.filter(studio__arya=region)
            else:
                city = geo.get('city')
                if city:
                    city=City.objects.get(pk=city['id'])       
                    queryset=queryset.filter(studio__city=city)

        dostavka=data.get('geo',{}).get('dostavka')
        if dostavka:
            queryset=queryset.filter(studio__delivery=True)

        gender=data.get('gender')
        if gender:
            id=gender.get('id')
            queryset=queryset.filter(gender_id=id)

        purpose=data.get('purpose')
        if purpose:
            id=purpose.get('id')
            queryset=queryset.filter(purpose_id=id)                        


        queryset=queryset[:20]
        return queryset



class StudioView(generic.DetailView):
    template_name = 'studio.html'
    model = Studio

    def dispatch(self, request, *args, **kwargs):
        if not kwargs.get('view', False):
            return redirect('studio', pk=self.kwargs.get('pk'), view='velos')
        else:
            return super(StudioView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        c = super(StudioView, self).get_context_data(**kwargs)
        c['view'] = self.kwargs.get('view')

        studios = self.object.get_all_studios()

        c['velos'] = Bike.objects.filter(studio_id__in=studios)
        c['velos_count'] = len(c['velos'])
        c['form'] = ReviewForm()

        c['rating'] = {
            'average': Review.objects.filter(studio=self.object).aggregate(Avg('rating_score')),
            'minus': Review.objects.filter(studio=self.object, rating_score__in=(1, 2)).aggregate(Count('pk')),
            'plus': Review.objects.filter(studio=self.object, rating_score__in=(3, 4)).aggregate(Count('pk'))
        }
        return c

    def post(self, request, *args, **kwargs):
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.studio_id = self.kwargs.get('pk')
            review.rating = Rating(score=request.POST.get('rating'), votes=1)
            review.save()
            self.object = self.get_object()
            self.object.rating.add(score=request.POST.get('rating'), user=request.user,
                                   ip_address=request.META['REMOTE_ADDR'])
        else:
            from django.contrib import messages
            messages.warning(request, messages.INFO, 'Review form invalid')
        return redirect(reverse('studio', kwargs={'pk': self.kwargs.get('pk'), 'view': 'reviews'}))


class BikeView(HitCountDetailView):
    template_name = 'bike_card.html'
    model = Bike
    count_hit = True

    def get_context_data(self, **kwargs):
        c = super(BikeView, self).get_context_data(**kwargs)
        c['rating'] = {
            'average': Review.objects.filter(studio=self.object.studio).aggregate(Avg('rating_score')),
            'minus': Review.objects.filter(studio=self.object.studio, rating_score__in=(1, 2)).aggregate(Count('pk')),
            'plus': Review.objects.filter(studio=self.object.studio, rating_score__in=(3, 4)).aggregate(Count('pk'))
        }

        return c


class BikePhotosView(generic.View):
    def get(self, request, *args, **kwargs):
        qs = BikePhoto.objects.filter(bike_id=self.request.GET.get('pk'))
        return HttpResponse(json.dumps([x.as_dict() for x in qs]), content_type='application/json')


class StudioPhotosView(generic.View):
    def get(self, request, *args, **kwargs):
        qs = StudioPhotos.objects.filter(studio_id=self.request.GET.get('pk'))
        return HttpResponse(json.dumps([x.as_dict() for x in qs]), content_type='application/json')
