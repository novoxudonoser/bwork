from crm.lending_proc.models import ApplicationUser, YandexGetParametrsStatic,YandexGetParametrsDynamic
from crm.lending_proc.models import Pfones,PfoneShown, Lendings,ClientsTrack,Clients
from crm.lending_proc.models import DirectCompanes,DirectGroups,DirectAdds,DirectPhrases,DirectRegions
from crm.lending_proc.models import SearchqQuerys,SearchqQuerysStats,YgetparamesstatSearchqerysRel
from crm.lending_proc.models import PfoneIncomeLog,ApplicationUser

from django.views.generic import View
from django.views.generic.base import TemplateView

from django.http import HttpResponse

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView,View

from django.shortcuts import get_list_or_404, get_object_or_404


class VisitsListView(ListView):
    template_name = 'lend/visits.html'
 
    def get_queryset(self):
        self.request.GET.get('perpage')
        objects = ClientsTrack.objects.all()

        pk_query=self.request.GET.get('pk_query')
        pk_query=int(pk_query) if pk_query and pk_query.isdigit() else None
        pk_gp=self.request.GET.get('pk_gp')
        pk_gp=int(pk_gp) if pk_gp and pk_gp.isdigit() else None

        if pk_query:
            objects=objects.filter(query__id=pk_query)
        if pk_gp:
            objects=objects.filter(query__id=pk_gp)

        objects=objects.order_by('-date')

        perpage = self.request.GET.get('perpage') 
        paginator = Paginator(objects,int(perpage) if perpage and perpage.isdigit() and int(perpage) > 0 else 20)
        page = self.request.GET.get('page')
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        return objects


    def get_context_data(self, **kwargs):
        context = super(VisitsListView, self).get_context_data(**kwargs)
        perpage = self.request.GET.get('perpage')
        perpage = perpage if perpage and perpage.isdigit() and int(perpage) > 0 else 20
        context['perpage']=perpage
        return context




class QerysView(ListView):
    template_name = 'lend/qerys.html'

    def get_queryset(self):
        self.request.GET.get('perpage')
        objects = YgetparamesstatSearchqerysRel.objects.all()


        query=self.request.GET.get('query')
        if query:
            objects=objects.filter(query__qerydata=query)

        perpage = self.request.GET.get('perpage') 
        paginator = Paginator(objects,int(perpage) if perpage and perpage.isdigit() and int(perpage) > 0 else 20)
        page = self.request.GET.get('page')
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        return objects


    def get_context_data(self, **kwargs):
        context = super(QerysView, self).get_context_data(**kwargs)
        perpage = self.request.GET.get('perpage')
        perpage = perpage if perpage and perpage.isdigit() and int(perpage) > 0 else 20
        context['perpage']=perpage
        return context

class QerysDetailView(TemplateView):
    template_name = 'lend/qerysdetail.html'

    def get_context_data(self, **kwargs):
        context = super(QerysDetailView, self).get_context_data(**kwargs)
        pk = self.request.GET.get('pk')
        pk = int(pk) if pk and pk.isdigit() else None
        object = YgetparamesstatSearchqerysRel.objects.filter(pk=pk)
        object = object[0] if object else None
        context['object']=object
        return context
        
class CallsView(ListView):
    template_name = 'lend/calls.html'

    def get_queryset(self):
        self.request.GET.get('perpage')
        objects = PfoneIncomeLog.objects.all()

        objects=objects.order_by('-income_date')

        perpage = self.request.GET.get('perpage') 
        paginator = Paginator(objects,int(perpage) if perpage and perpage.isdigit() and int(perpage) > 0 else 20)
        page = self.request.GET.get('page')
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        return objects


    def get_context_data(self, **kwargs):
        context = super(CallsView, self).get_context_data(**kwargs)
        perpage = self.request.GET.get('perpage')
        perpage = perpage if perpage and perpage.isdigit() and int(perpage) > 0 else 20
        context['perpage']=perpage
        return context

class AppsView(ListView):
    template_name = 'lend/apps.html'

    def get_queryset(self):
        self.request.GET.get('perpage')
        objects = ApplicationUser.objects.all()

        objects=objects.order_by('-created')

        perpage = self.request.GET.get('perpage') 
        paginator = Paginator(objects,int(perpage) if perpage and perpage.isdigit() and int(perpage) > 0 else 20)
        page = self.request.GET.get('page')
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        return objects


    def get_context_data(self, **kwargs):
        context = super(AppsView, self).get_context_data(**kwargs)
        perpage = self.request.GET.get('perpage')
        perpage = perpage if perpage and perpage.isdigit() and int(perpage) > 0 else 20
        context['perpage']=perpage
        return context