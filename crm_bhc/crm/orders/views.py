# -*- coding: utf-8 -*- 
import re

from django.utils import timezone

from datetime import datetime, timedelta

from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Q, Count
from django.forms.formsets import formset_factory
from django.views.generic import ListView, DetailView, View
from django.views.generic.edit import CreateView, UpdateView, FormView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import render

from crm.calls.models import Call, CallTheme
from crm.orders.forms import OrderForm, ContactForm, AddressForm, DocumentForm, PhoneForm
from crm.orders.mixins import RouteQuerysetMixin
from crm.orders.models import AdditionalInfoAddress, Order, OrderAddress, OrderContact, OrderDocument, ContactPhone, TYPE_ADDRESS
from crm.users.mixins import LoginRequiredMixin

__author__ = 'PekopT'


def order_form_errors(POST, order_pk):

    errors = []
    form1 = False
    form2 = False
    form3 = False
    form4 = False

    if order_pk:
        if not POST.get('doc-new-inn') and not POST.get('doc-new-date_complete') and not POST.get('doc-new-lname_ip'):
            save_document = False

    PhoneFormSet = formset_factory(PhoneForm, validate_min=True, can_delete=True)

    # handle documents
    if (order_pk and POST.get('doc-new-question')) or (not order_pk):
        form3 = DocumentForm(None, POST, prefix='doc-new')
        if not form3.is_valid():
            errors.append(form3.errors)

    if POST.get('cntc-name'):
        if POST.get('client'):
            client = OrderContact.objects.get(id=int(POST.get('client')))
            form1 = ContactForm(POST, prefix='cntc', instance=client)
        else:
            form1 = ContactForm(POST, prefix='cntc')
        if not form1.is_valid():
            errors.append(form1.errors)

    if POST.get('phone-0-email') or POST.get('phone-0-phone'):
        init_list = []
        if client:
            for p in client.phones.all().order_by('id'):
                init_list.append({
                    'phone': p.phone,
                    'email': p.email
                })
        form4 = PhoneFormSet(POST, prefix='phone', initial=init_list)

        for f in form4:
            if not f.is_valid():
                errors.append(f.errors)

    if not POST.get('client_address') and (POST.get('addr-street') or POST.get('addr-metro')):
        form2 = AddressForm(user, POST, prefix='addr')
        if not form2.is_valid():
            errors.append(form2.errors)

    return {
        'errors': errors,
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'client': client
    }

class OrderList(LoginRequiredMixin, ListView):
    model = Order

    def get_queryset(self):
        """
            filtering
        """
        queryset = super(OrderList, self).get_queryset()
        term = self.request.GET.get('search', '')
        if term != '':
            queryset = queryset.filter(Q(client__name__icontains=term) |
                                       Q(client__phones__phone__icontains=term) |
                                       Q(client__phones__email__icontains=term) |
                                       Q(number__icontains=term)) \
                .annotate(total=Count('id'))

        filter = self.request.GET.get('filter_theme', '')
        if filter != '':
            queryset = queryset.filter(documents__question__theme=filter)

        if term or filter:
            queryset.query.group_by = ['id']

        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderList, self).get_context_data(**kwargs)
        context['themes'] = CallTheme.objects.all()
        context['filter_theme'] = self.request.GET.get('filter_theme', '')
        context['search'] = self.request.GET.get('search', '')

        return context


class OrderDetail(LoginRequiredMixin, DetailView):
    template_name = 'orders/order.html'
    model = Order


class OrderViewForm(LoginRequiredMixin, FormView):
    model = Order
    template_name = 'orders/order_form.html'
    form_class = OrderForm

    def get(self, request, *args, **kwargs):
        self.call_id = kwargs.pop('call_id', None)
        return super(OrderViewForm, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        new_client = None
        address = None
        user = request.user
        save_document = True

        POST = request.POST

        try:
            order_pk = self.kwargs['pk']
        except KeyError:
            order_pk = None

        data = order_form_errors(POST, order_pk)
        errors, form1, form2, form3, form4, client = data['errors'], data['form1'], data['form2'], data['form3'], data['form4'], data['client']

        self.errors = errors

        ret = super(OrderViewForm, self).post(request, *args, **kwargs)

        if not order_pk:
            order = self.object.instance

        if not errors:
            if order_pk:
                order = Order.objects.get(id=order_pk)
                documents = order.documents.filter(address__isnull=False)
                if documents:
                    address = documents[0].address

            if ((not POST.get('client') and order_pk) or not order_pk or POST.get('client')) and form1:
                form1.save()
                new_client = form1.instance
            elif form1 and order.client and not POST.get('client'):
                new_client = OrderContact.objects.get(pk=order.client_id)

            if new_client:
                order.client = new_client
                order.save()

            if (POST.get('phone-0-email') or POST.get('phone-0-phone')) and new_client:
                if client:
                    for p in client.phones.all():
                        client.phones.remove(p)

                for f in form4:
                    d = f.cleaned_data
                    if not d:
                        continue
                    delete = d['DELETE']
                    if not delete:
                        f.save()
                        phone = f.instance
                        new_client.phones.add(phone)

            if POST.get('addr-street') or POST.get('addr-metro') or POST.get('client_address'):
                client_address = False
                if POST.get('client_address'):
                    try:
                        address = OrderAddress.objects.get(id=int(POST.get('client_address')))
                        client_address = True
                    except OrderAddress.DoesNotExist:
                        pass
                if not client_address and POST.get('addr-street') or POST.get('addr-metro'):
                    if address:
                        form2 = AddressForm(user, POST, prefix='addr', instance=address)
                    else:
                        form2 = AddressForm(user, POST, prefix='addr')
                    form2.save()
                    client_address = True

                if client_address:
                    if address and address.additional_data:
                        for ad in address.additional_data.all():
                            ad.delete()

                    if not address:
                        address = form2.instance

                    for ta in TYPE_ADDRESS:
                        name_key = 'addit-addr-'+str(ta[0])
                        if name_key in POST:
                            ad_inf = POST.get(name_key)
                            if ad_inf:
                                AdditionalInfoAddress.objects \
                                    .create(name=ad_inf, type_address=ta[0], order_address=address)
                                # address.name = address.name + ', ' + ta[1]
                                # address.save()
            else:
                order.address_id = POST.get('address')

            order.save()

            # handle documents
            if form3 and save_document:
                form3.save()
                order.documents.add(form3.instance)

            if address:
                order.address = address
                order.save()
                if form1:
                    order_contact = form1.instance
                    if address not in order_contact.address.all():
                        order_contact.address.add(address)
        return ret

    def get_form(self, form_class):

        try:
            order_pk = self.kwargs['pk']
        except KeyError:
            order_pk = None

        if order_pk:
            order = Order.objects.get(pk=order_pk)
        else:
            order = None

        self.object = order

        return form_class(self.request.user, instance=order, **self.get_form_kwargs())

    def form_valid(self, form):
        if not self.errors:
            if form.is_valid():
                form.save(commit=False)

                if self.request.user.is_authenticated():
                    form.user = self.request.user
                else:
                    form.user_id = 1  # TODO: auth on whole interface. and User only
                form.save()
                order = form.instance
                if not order.number:
                    number = timezone.now().strftime('%d%m') + '-' + str(order.pk)
                    order.number = number
                    order.save()

                self.object = form
            return super(OrderViewForm, self).form_valid(form)
        else:
            from django.forms.util import ErrorList
            errors = form._errors.setdefault("form3_errors", ErrorList())
            errors.append(self.errors)
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):

        order = self.object
        user = self.request.user

        context = super(OrderViewForm, self).get_context_data(**kwargs)

        document, documents, address, orders = None, None, None, None

        if order:
            address = order.address
            document = order.documents.last()
            orders = Order.objects.filter(client=order.client)

        if self.request.POST.get('cntc-name'):
            if self.request.POST.get('client'):
                client = OrderContact.objects.get(id=int(self.request.POST.get('client')))
                context['contact_form'] = ContactForm(self.request.POST, prefix='cntc', instance=client)
            else:
                context['contact_form'] = ContactForm(self.request.POST, prefix='cntc')
        else:
            if order and order.client:
                context['contact_form'] = ContactForm(prefix='cntc', instance=order.client)
            else:
                context['contact_form'] = ContactForm(prefix='cntc')

        PhoneFormSet = formset_factory(PhoneForm, extra=1, validate_min=True,  min_num=0, can_delete=True)
        if self.request.POST.get('phone-0-email') or self.request.POST.get('phone-0-phone'):
            context['phone_formset'] = PhoneFormSet(self.request.POST, prefix='phone')
        else:
            init_list = []
            if order and order.client and order.client.phones.all():
                for p in order.client.phones.all().order_by('id'):
                    init_list.append({
                        'phone': p.phone,
                        'email': p.email,
                        'additional_number': p.additional_number
                    })
            context['phone_formset'] = PhoneFormSet(initial=init_list, prefix='phone')

        if address:
            if self.request.POST.get('addr-street') or self.request.POST.get('addr-metro'):
                context['address_form'] = AddressForm(user, self.request.POST, prefix='addr', instance=address)
            else:
                context['address_form'] = AddressForm(user, prefix='addr', instance=address)
        else:
            if self.request.POST.get('addr-street') or self.request.POST.get('addr-metro'):
                context['address_form'] = AddressForm(user, self.request.POST, prefix='addr')
            else:
                context['address_form'] = AddressForm(user, prefix='addr')

        if self.request.POST:
            context['document_form'] = DocumentForm(None, self.request.POST, prefix='doc-new')
        else:
            if order:
                context['document_form'] = DocumentForm(document, prefix='doc-new')
            else:
                context['document_form'] = DocumentForm(None, prefix='doc-new')

        if self.request.POST and self.errors:
            context['errors'] = self.errors

        context['order'] = order

        context['orders'] = orders

        context['document'] = document

        context['address'] = address

        if address and address.additional_data:
            context['addit_exists'] = list(address.additional_data.values_list('type_address', flat=True).annotate())

        context['type_addresses'] = TYPE_ADDRESS

        return context

    def get_success_url(self, *args, **kwargs):
        action = self.request.POST.get('action', False)
        if action:
            if action == 'save_close':
                return reverse('orders-list')

        order = self.object.instance
        return reverse('orders-update', kwargs={'pk': order.pk})


class OrderDelete(LoginRequiredMixin, DeleteView):
    model = Order
    success_url = reverse_lazy('orders-list')
    form_class = OrderForm


class Request(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        textarea = request.POST.get('form_requests', None)
        email = request.user.settings.email

        if textarea and email:
            textarea = textarea + "С Уважением, Юридическая Поддержка Бизнеса"
            send_mail('Заявки', textarea, settings.DEFAULT_FROM_EMAIL, [email], fail_silently=False, )
        return self.get(request)

    def get(self, request):
        print_page = request.GET.get('print_page', None)
        today = datetime.now().date()
        start = datetime(today.year, today.month, today.day)
        end = start + timedelta(1)

        today_requests = OrderDocument.objects.filter(date_complete__gte=start, date_complete__lte=end)
        requests = OrderDocument.objects.exclude(pk__in=[x.pk for x in today_requests])

        raws_count = today_requests.count() + requests.count()
        data = {
            'length_roday_requests': today_requests.count(),
            'today_requests': today_requests,
            'raws_count': raws_count*2,
            'requests': requests
        }

        if not print_page:
            template = 'orders/requests.html'
        else:
            template = 'orders/print_requests.html'
        return render(request, template, data)


class Route(LoginRequiredMixin, View, RouteQuerysetMixin):
    def post(self, request):
        return self.get(request)

    def get(self, request):

        data = self.get_context_data()
        routes_id = data['filter_route']

        orders = self.get_queryset()

        if routes_id:
            if '-2' in routes_id:
                routes_id.remove('-2')
                orders = orders \
                    .filter(Q(route__in=routes_id) | Q(route__isnull=True))
            else:
                orders = orders.filter(route__in=routes_id)

        orders = orders.values('id',
                                'document_order__number',
                                'address__name',
                                'route',
                                'document_order__client__name'
                        )\
                        .order_by('route')
        data = {
            'orders': orders
        }

        data.update(self.get_context_data())
        return render(request, 'orders/routes.html', data)

    def get_context_data(self):

        routes_id = self.request.POST.getlist('routes', False)

        if routes_id == ['']:
            routes_id = False

        list_routes = list(
            set(OrderDocument.objects \
                   .filter(route__isnull=False) \
                   .values_list('route', flat=True) \
                   .annotate()
                   )
           )

        return {'routes': list_routes, 'filter_route': routes_id}


class ContactList(LoginRequiredMixin, ListView):
    model = OrderContact
    # form_class = OrderContact

    def get_template_names(self):
        return 'contact/list.html'

    def get_queryset(self):
        """
            filtering
        """
        queryset = super(ContactList, self).get_queryset()
        term = self.request.GET.get('search', None)
        if term is not None:
            queryset = queryset.filter(Q(name__icontains=term) |
                                       Q(phones__email__icontains=term) |
                                       Q(phones__phone__icontains=term)) \
                .annotate(total=Count('id'))
        return queryset


class ContactDetail(LoginRequiredMixin, UpdateView):
    model = OrderContact
    form_class = ContactForm

    def get_template_names(self):
        return 'contact/form.html'

    def post(self, request, *args, **kwargs):

        errors = []

        PhoneFormSet = formset_factory(PhoneForm, validate_min=True, can_delete=True)

        if self.request.POST.get('phone-0-email') or self.request.POST.get('phone-0-phone'):
            form4 = PhoneFormSet(request.POST, prefix='phone')

            for f in form4:
                if not f.is_valid():
                    errors.append(f.errors)

        self.errors = errors

        ret = super(ContactDetail, self).post(request, *args, **kwargs)

        if not errors:
            self.object.save()

            if self.request.POST.get('phone-0-email') or self.request.POST.get('phone-0-phone') and not errors:
                client = self.object

                for p in client.phones.all():
                    client.phones.remove(p)
                    p.delete()

                for f in form4:
                    d = f.cleaned_data
                    if d:
                        delete = d['DELETE']
                        if not delete:
                            client.phones.add(f.save())

        return ret

    def form_valid(self, form):
        if self.errors:
            from django.forms.util import ErrorList
            errors = form._errors.setdefault("form3_errors", ErrorList())
            errors.append(self.errors)
            return self.form_invalid(form)
        else:
            return super(ContactDetail, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ContactDetail, self).get_context_data(**kwargs)

        PhoneFormSet = formset_factory(PhoneForm, validate_min=True, can_delete=True)

        if self.request.POST.get('phone-0-email') or self.request.POST.get('phone-0-phone'):
            context['phone_formset'] = PhoneFormSet(self.request.POST, prefix='phone')
        else:
            init_list = []
            for p in self.object.phones.all().order_by('id'):
                init_list.append({
                    'phone': p.phone,
                    'email': p.email,
                    'additional_number': p.additional_number
                })
            context['phone_formset'] = PhoneFormSet(initial=init_list, prefix='phone')

        context['orders'] = Order.objects.filter(client=self.object)
        return context


class ContactDelete(LoginRequiredMixin, DeleteView):
    model = OrderContact
    success_url = reverse_lazy('contact-list')
    form_class = ContactForm

    def get_template_names(self):
        return 'contact/delete.html'


class PhoneDelete(LoginRequiredMixin, DeleteView):
    model = ContactPhone
    form_class = PhoneForm

    def get_template_names(self):
        return 'phone/delete.html'

    def get_success_url(self):
        if self.object.phone_contacts.all():
            contact = self.object.phone_contacts.first().pk
            return reverse('contact-detail', kwargs={'pk': contact})
        return reverse_lazy('contact-list')


class DocumentDetail(LoginRequiredMixin, UpdateView):
    model = OrderDocument
    template_name = 'orders/document_form.html'
    form_class = DocumentForm

    def post(self, request, *args, **kwargs):
        p = super(DocumentDetail, self).post(request, None, *args, **kwargs)
        form2 = AddressForm(self.request.user, request.POST, prefix='addr')
        if form2.is_valid():
            form2.save()

        return p

    def get_context_data(self, **kwargs):
        context = super(DocumentDetail, self).get_context_data(**kwargs)
        if self.object and self.object.address:
            context['address_form'] = AddressForm(self.request.user, instance=self.object.address, prefix='addr')
        else:
            context['address_form'] = AddressForm(self.request.user, prefix='addr')

        return context

    def get_success_url(self):
        # return reverse('document-detail', kwargs={'pk': self.object.pk})
        document = self.object
        return reverse('orders-update', kwargs={'pk': self.object.document_order.last().pk})