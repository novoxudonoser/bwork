from django.db import models
from wagtail.wagtailsnippets.models import register_snippet


@register_snippet
class SiteMenu(models.Model):
    name = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
    alt = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    show_header = models.BooleanField(default=False)
    show_footer = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


@register_snippet
class SocialMenu(models.Model):
    name = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
    alt = models.CharField(max_length=255, null=True, blank=True)
    image = models.ImageField(upload_to='media/socialicons')
    title = models.CharField(max_length=255, null=True, blank=True)
    show_header = models.BooleanField(default=False)
    show_footer = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name