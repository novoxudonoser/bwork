/**
 * Created by PekopT on 02.03.16.
 */
var app = angular.module('app', ['ui.bootstrap', 'ui.select', 'checklist-model', 'ui.bootstrap-slider']);

app.controller('MainCtrl', ['$scope', '$timeout', '$uibModal', function MainCtrl($scope, $timeout, $uibModal) {
    var s = $scope.s = {};

    s.things = [{ id: 1, name: 'foo' }, { id: 2, name: 'bar' }, { id: 3, name: 'baz' }, { id: 4, name: 'qux' }, { id: 5, name: 'quux' }, { id: 6, name: 'corge' }, { id: 7, name: 'grault' }];

    s.openModal = function openModal($event, smyj) {
        // smth
        return $uibModal.open({
            templateUrl: 'confirmationModalTempl',
            controller: 'ModalCtrl as asdfasdfs'
        }).result.then(res => {
            if (res === 'yes') s.modalResult = 'yes';
        }, res => {
            s.modalResult = 'no';
        });
    };

    $timeout(function timeout() {
        $('input.-icheck').icheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
    });

    s.sd = function (a) {
        console.log();
    };
}]);

app.controller('ModalCtrl', ['$scope', '$uibModalInstance', function ConfirmationModalCtrl($scope, $uibModalInstance) {
    const s = this;

    s.yes = () => $uibModalInstance.close('yes');
    s.cancel = () => $uibModalInstance.dismiss();
}]);

app.controller('ReviewFormCtrl', ['$scope', function ReviewFormCtrl($scope, $timeout, $uibModal) {
    $scope.ratingClick = function (smth) {
        $("#id_rating").val(smth);
    };
}]);

app.controller('TopSearchCtrl', ['$scope', function TopSearchCtrl($scope, $timeout, $uibModal) {
    //TODO: make this values gettable from server
    $scope.peoples = [{ 'id': 1, name: '1 человек' }, { 'id': 2, name: 'Компания' }];

    $scope.genders = [{ 'id': 0, name: 'Женский' }, { 'id': 1, name: 'Мужской' }];

    $scope.velotypes = [{ 'id': 1, name: 'Женский' }, { 'id': 2, name: 'Мужской' }, { 'id': 3, name: 'Мужской' }];

    $scope.cities = [{ 'id': 1, name: 'Москва' }, { 'id': 2, name: 'Краснодар' }];

    $scope.regions = [{ 'id': 1, name: 'Бирюлево' }, { 'id': 2, name: 'Бутово' }];

    $scope.selectPeople = function (item, model) {
        if (item.id == 1) {
            //TODO
        } else {
                //TODO
            }
    };
}]);

app.controller('veloListCtrl', ['$scope', function ($scope) {}]);

app.controller('SubscribeCtrl', ['$scope', function ($scope) {
    $scope.submit = function () {
        // TODO: make this creating subscribe
        return false;
    };
}]);