__author__ = 'PekopT'

from django.conf.urls import patterns, url

from .views import CallList, CallDetail, CallCreate, CallDelete, CallUpdate

urlpatterns = patterns('',
                       url(r'^$', CallList.as_view(), name='calls-list'),
                       url(r'(?P<pk>\d+)/?$', CallDetail.as_view(), name='calls-detail'),
                       url(r'(?P<pk>\d+)/edit/?$', CallUpdate.as_view(), name='calls-update'),
                       url(r'(?P<pk>\d+)/delete/?$', CallDelete.as_view(), name='calls-delete'),
                       url(r'^add/?$', CallCreate.as_view(), name='calls-create'),
                       )
