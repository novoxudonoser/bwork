/**
 * Created by PekopT on 02.03.16.
 */
var app = angular.module('app', ['ui.bootstrap', 'ui.select', 'checklist-model', 'ui.bootstrap-slider', 'ngRoute','ngSanitize','ngAnimate'])

app.config(['$interpolateProvider', '$httpProvider', '$routeProvider', function ($interpolateProvider, $httpProvider, $routeProvider) {
    $interpolateProvider.startSymbol('{(')
    $interpolateProvider.endSymbol(')}')

    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

    $routeProvider.
          when('/bikes_filter', {
        templateUrl: 'templates/bikes_filtering',
        controller: 'BikesFilter'
          }).
          when('/studios_filter', {
        templateUrl: 'templates/studios_filtering',
        controller: 'StudiosFilter'
          })
}]);          


var bike_list=''
var studio_list=''
var bike_form=null

app.factory('perssistent',function(){
        perssistent = {};
        perssistent.bikes={};
        perssistent.studios={};
        perssistent.geo={}
        return perssistent
});

main_init = function main_init($scope,$timeout){

        $scope.order = [
            {'id': 1, name: 'По релевантности'},
            {'id': 2, name: 'По цене за час'},
            {'id': 3, name: 'По цене за день'},
            {'id': 4, name: 'По цене за неделю'},
        ];

        $scope.peoples = [
            {'id': 1, name: '1 человек'},
            {'id': 2, name: 'Компания'}
        ];

        $scope.cities = [
            {'id': 1, name: 'Москва'},
            {'id': 2, name: 'Краснодар'}
        ];

        $scope.regions = [
            {'id': 1, name: 'Бирюлево'},
            {'id': 2, name: 'Бутово'}
        ];

        $scope.men_num = [
            {'id': 1, name: '1'},
            {'id': 2, name: '2'},
            {'id': 1, name: '3'},
            {'id': 2, name: '4'},
            {'id': 1, name: '5'},
            {'id': 2, name: '6'},
            {'id': 1, name: '7'},         
        ];


        $scope.woman_num = [
            {'id': 1, name: '1'},
            {'id': 2, name: '2'},
            {'id': 1, name: '3'},
            {'id': 2, name: '4'},
            {'id': 1, name: '5'},
            {'id': 2, name: '6'},
            {'id': 1, name: '7'},         
        ];

        $timeout(function timeout() {
            $('input.-icheck').icheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
            })
        }) 

}

app.controller('BikesFilter', [
    '$scope', '$timeout', '$uibModal','$http','$location','perssistent',

    function BikesFilter($scope, $timeout, $uibModal,$http,$location,perssistent) {
        $scope.data=data

    // $scope.$watch('filterForm', function() {
    //     if ($scope.form){
    //         console.log('sda')
    //     }
    // }, true);        

        main_init($scope,$timeout)

        $scope.makeorder = function(order){
            if(!order){
                order=$scope.order[0]
                return order
            }

        }


        $scope.$watch(['peoplenum','form.gender','form.purpose','geo.city',
            'geo.region','geo.dostavka','form.order','peoplenum','form.gender','form.purpose','geo.city','geo.region',
            'geo.dostavka','form.order'], function() {
                console.log('sda')
                temp=JSON.parse(JSON.stringify(scope.form.geo));
                $scope.form.geo=$scope.geo
                if (JSON.parse(JSON.stringify(perssistent.sform)) != JSON.parse(JSON.stringify($scope.form))){
                    $scope.changeView('bikes_filtering')
                    $scope.form == perssistent.sform
                }
                else{
                   $scope.form.geo=temp 
                }
                
                
            }, true);

        $scope.animate=true
        // $scope.$on('$routeChangeSuccess', function () {
        //   $("#bodycontent").removeClass("toggle");
        // });

        

        $scope.update = function(peoplenum){
            if (peoplenum.id==1){
                $scope.changeView('bikes_filter')
            }
            if (peoplenum.id==2){
                $scope.changeView('studios_filter')
            }                
        }


        $scope.changeView = function(view){
            perssistent.geo=$scope.geo
            perssistent.bikes.geo=JSON.parse(JSON.stringify(perssistent.geo));
            
            perssistent.bikes.form = $scope.form   
            $location.path(view); // path not hash
        }

        if (perssistent.bikes.form){
            $scope.form = perssistent.bikes.form
        }       

        if (perssistent.geo) {
            $scope.geo = perssistent.geo;
        }

        if (!perssistent.bikes.list_html){
            perssistent.bikes.list_html=$('#bicycles_list').html()
        }
        $scope.bike_list_html=perssistent.bikes.list_html

        $scope.submit = function () {
            if (!$scope.form){$scope.form={}}
            $scope.form.geo=$scope.geo
            $http.post('/filter_bicycles', $scope.form).then(function successCallback(response) {
                perssistent.bikes.list_html=response.data
                $scope.bike_list_html=perssistent.bikes.list_html
            }, function errorCallback(response) {
                console.log("error")
            });   
        }

    if(perssistent.bikes.geo  || ((perssistent.geo != perssistent.bikes.geo)) && perssistent.geo){
        $scope.submit()
        perssistent.bikes.geo=perssistent.geo
    }
             
    },
]);


app.controller('StudiosFilter', [
    '$scope', '$timeout', '$uibModal','$http','$location','perssistent',

    function StudiosFilter($scope, $timeout, $uibModal,$http,$location,perssistent) {
        $scope.data=data
        main_init($scope,$timeout)



        $scope.changeView = function(view){
            perssistent.geo=$scope.geo
            perssistent.studios.geo=JSON.parse(JSON.stringify(perssistent.geo));
            perssistent.studios.form = $scope.form   
            $location.path(view); // path not hash
        }

        $scope.update = function(peoplenum){
            if (peoplenum.id==1){
                $scope.changeView('bikes_filter')
            }
            if (peoplenum.id==2){
                $scope.changeView('studios_filter')
            }                
        }

        if (perssistent.studios.form){
            $scope.form = perssistent.studios.form
        }       

        if (perssistent.geo) {
            $scope.geo = perssistent.geo;
        }

        if (!perssistent.studios.list_html){
            perssistent.studios.list_html=$('#studios_list').html()
        }
        $scope.studios_list_html=perssistent.studios.list_html

        $scope.submit = function (e) {
            if (!$scope.form){$scope.form={}}
            $scope.form.geo=$scope.geo

            $http.post('/filter_studios', $scope.form).then(function successCallback(response) {
                perssistent.studios.list_html=response.data
                $scope.studios_list_html=perssistent.studios.list_html
            }, function errorCallback(response) {
                console.log("error")
            });

        }

        if(perssistent.studios.geo  || ((perssistent.geo != perssistent.studios.geo)) && perssistent.geo){
            $scope.submit()
            perssistent.studios.geo=perssistent.geo
        }


    },
]);

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }

        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

app.controller('ModalCtrl', [
    '$scope', '$uibModalInstance',

    function ConfirmationModalCtrl($scope, $uibModalInstance) {
        const s = this


        s.yes = () => $uibModalInstance.close('yes')
        s.cancel = () => $uibModalInstance.dismiss()
    },
]);

app.controller('TopSearchCtrl', [
    '$scope',
    function TopSearchCtrl($scope, $timeout, $uibModal) {
        //TODO: make this values gettable from server


        $scope.selectPeople = function (item, model) {
            if (item.id == 1) {
                //TODO
            } else {
                //TODO
            }
        }
    }
]);


app.controller('ReviewFormCtrl', [
    '$scope',

    function ReviewFormCtrl($scope, $timeout, $uibModal) {
        $scope.ratingClick = function (smth) {
            $("#id_rating").val(smth);
        }
    }
]);

app.controller('addDescCtrl', [
    '$scope',
    function addDescCtrl($scope, $timeout, $uibModal) {

        $scope.changeAdditional = function ($event) {
            var id = $($event.target).data('id');
            var field = $($event.target).data('field');
            var val = $($event.target).is(':checked');
            if (val) {
                $("#" + field + " option[value='" + id + "']").attr('selected', 'selected');
            } else {
                $("#" + field + " option[value='" + id + "']").removeAttr('selected');
            }
        }

    }
]);

app.controller('addVeloCtrl', [
    '$scope', '$uibModal',

    function addVeloCtrl($scope, $uibModal) {
        $scope.showAddForm = function ($event) {
            $("#velo_form").show();
            $($event.target).hide();
        }
        $scope.chooseFile = function () {
            $("#file_1").click();
            return false;
        }
        //$scope.handleFile = function ($event) {
        //    console.log($event);
        //}

        //$("#upload_form").submit(function (e) {
        //    e.preventDefault(); // avoid to execute the actual submit of the form.
        //});

        //$("#file_1").on('change', function (e) {
        //    var file = $(this).val();
        //    console.log(file);
        //    var url = $("#upload_form").attr('action'); // the script where you handle the form input.
        //    var formData = new FormData($("#upload_form")[0]);
        //    console.log(formData);
        //    //$("#upload_form").submit();
        //    $.ajax({
        //        type: "POST",
        //        url: url,
        //        data: formData, // serializes the form's elements.
        //        //xhr: function () {  // Custom XMLHttpRequest
        //        //    var myXhr = $.ajaxSettings.xhr();
        //        //    if (myXhr.upload) { // Check if upload property exists
        //        //        myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
        //        //    }
        //        //    return myXhr;
        //        //},
        //        success: function (data) {
        //            console.log(data)
        //        },
        //        //Options to tell jQuery not to process data or worry about content-type.
        //        cache: false,
        //        contentType: false,
        //        processData: false
        //    });
        //
        //});

        $scope.changeAdditional = function ($event) {
            var id = $($event.target).data('id');
            var field = $($event.target).data('field');
            var val = $($event.target).is(':checked');
            if (val) {
                $("#" + field + " option[value='" + id + "']").attr('selected', 'selected');
            } else {
                $("#" + field + " option[value='" + id + "']").removeAttr('selected');
            }
        }

        $scope.openModal = function (size) {
            var modalInstance = $uibModal.open({
                templateUrl: 'TariffModalContent.html',
                controller: 'TariffInstanceCtrl',
                size: size,
                resolve: {}
            });
        };
        $scope.editModal = function () {
            var val = $("#id_tariff").val();

            $.ajax({
                type: "GET",
                url: '/account/tariff/' + val,
                datatype: 'json',
                success: function (data) {
                    var modalInstance = $uibModal.open({
                        templateUrl: 'TariffModalContent.html',
                        controller: 'TariffInstanceCtrl',

                        resolve: {
                            formData: {
                                id: data.tariff.id,
                                name: data.tariff.name,
                                price_hour: +data.tariff.price_hour,
                                price_2hour: +data.tariff.price_2hour,
                                price_day: +data.tariff.price_day,
                                price_week: +data.tariff.price_week
                            }
                        }
                    });
                }
            })
        }
        //
        $(".r_p_right_textarea select").select2({
            placeholder: 'Выберите значение',
            width: '50%'
        });
        //
        $("#id_studio").select2({
            placeholder: 'Выберите филиал',
            width: '100%'
        });
        //
        $("#id_tariff").select2({
            placeholder: 'Выберите тариф',
            width: '50%'
        });
    }
]);


app.controller('TariffInstanceCtrl', function ($scope, $uibModalInstance, formData) {
    $scope.formData = formData || {}
    $scope.ok = function () {
        if (formData.id) {
            var update = true;
            var url = '/account/tariff/update/' + formData.id;
        } else {
            var url = '/account/tariff/create/';
            var update = false;
        }
        $.ajax({
            type: 'POST',
            url: url,
            data: $scope.formData,
            datatype: 'json',
            success: function (a) {
                if (update) {
                    $("#id_tariff").find("option[value='" + a.tariff.id + "']").text(a.tariff.name);
                    $("#select2-id_tariff-container").text(a.tariff.name); //TODO: don't update
                } else {
                    $("#id_tariff")
                            .append("<option value='" + a.tariff.id + "'>" + a.tariff.name + "</option>").trigger('change')
                            .val(a.tariff.id);
                }
                $('#id_tariff').trigger('change');
                $uibModalInstance.close();
            }
        })
    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('veloListCtrl', [
    '$scope', function ($scope) {

    }
]);


app.controller('SubscribeCtrl', [
    '$scope', function ($scope) {
        $scope.submit = function () {
            // TODO: make this creating subscribe
            return false;
        }
    }
]);

app.directive('ngTmpUpload', function () {
    return {
        require: '?ngModel',
        templateUrl: 'tpl_tmp_upload',
        scope: {},
        controller: function ($scope) {
            if (object.model == 'Bike') {
                var url = "/bike/photos";
            } else {
                var url = "/studio/photos";
            }
            $scope.data = null;
            if (object.pk) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "GET",
                    data: {
                        pk: object.pk
                    },
                    success: function (data) {
                        if (data.length) {
                            $scope.data = data;
                            $scope.$apply();
                        }
                    }
                })
            }

            $scope.clearWidget = function (pk, model) {
                $.ajax({
                    url: '/images/delete',
                    dataType: 'json',
                    type: "POST",
                    data: {
                        pk: pk,
                        model: model
                    },
                    success: function () {
                        $scope.data = $scope.data.filter(function (obj) {
                            return obj.pk !== pk;
                        });
                        $scope.$apply();
                    }
                })
            };

            $scope.chooseFile = function () {
                $("#id_tmp_upload_file").click();
                return false;
            };
        },
        link: function ($scope, $widget, attrs, ngModel) {
            if (ngModel && ngModel.$render && ngModel.$setViewValue) {
                ngModel.$render = function () {
                    $scope.data = ngModel.$viewValue || null;
                    $widget.val($scope.data);
                };
                $scope.$watch('data', function (data) {
                    ngModel.$setViewValue(data);
                });
            }

            var $fileupload = $widget.find('input[type=file]');
            $fileupload.fileupload({
                url: '/images/upload',
                dataType: 'json',
                formData: [],
                done: function (e, data) {
                    if (data.result.status === 'success') {
                        if ($scope.data == null) {
                            $scope.data = []
                        }
                        $scope.data.push(data.result.data);
                    }
                    $scope.$apply();
                },
                fail: function (e, data) {
                    $scope.data = null;
                    $scope.$apply();
                }
            });
        }
    }
});