from datetime import datetime
from django.utils import timezone

__author__ = 'PekopT'

class workDays(object):
    # TODO: update every year, or from database
    weekends = [
        '01-01',
        '01-02',
        '01-03',
        '01-04',
        '01-05',
        '01-06',
        '01-07',
        '01-08',
        '01-09',
        '02-23',
        '03-09',
        '03-11',
        '05-01',
        '05-04',
        '05-09',
        '06-12',
        '11-04',
    ]

    def is_work_day(self, date=None, frmt="%d.%m.%Y"):
        if date is None:
            date = timezone.now()
        else:
            if not isinstance(date, datetime):
                date = timezone.datetime.strptime(date, frmt)

        weekday = date.strftime('%w')
        monthday = date.strftime('%m-%d')

        if (weekday in ['0', '6']) or (monthday in self.weekends):
            return False

        return True


