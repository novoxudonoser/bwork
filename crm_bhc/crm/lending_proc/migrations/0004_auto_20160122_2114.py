# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lending_proc', '0001_squashed_0003_auto_20160110_0825'),
    ]

    operations = [
        migrations.AddField(
            model_name='applicationuser',
            name='coment',
            field=models.TextField(null=True, blank=True, verbose_name='Коментарий'),
        ),
        migrations.AlterField(
            model_name='clientstrack',
            name='url',
            field=models.TextField(verbose_name='url'),
        ),
        migrations.AlterField(
            model_name='manageremail',
            name='active',
            field=models.BooleanField(verbose_name='Активный', default=True),
        ),
        migrations.AlterField(
            model_name='pfoneincomelog',
            name='cookie',
            field=models.CharField(max_length=10, verbose_name='cookie pfone'),
        ),
    ]