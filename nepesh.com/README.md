### Setup

install virtualenv
```
virtualenv env --python python3.4 && \
source env/bin/activate && \
pip install --requirement requirements-dev.txt
```

then:

``` 
npm install -g bower && cd assets && bower install && cd ..
```

then:

In case databases aren't present:

ALL IN ONE bash snippet

1. delete all migrations
2. makemigrations
3. cleardb
4. migrate
5. setup test data
6. collectstatic

```
find . -path "*/migrations/*.py" -not -name "__init__.py" -delete && \
python manage.py makemigrations account pages velos && python manage.py cleardb \
&& python manage.py migrate && python -W ignore manage.py setupdata \
&& python manage.py collectstatic --noinput
```

then:
```
python manage.py runserver
```


***some interesting things***

To start an interactive shell:

```
python manage.py shell_plus --use-pythonrc
```

delete all migrations 

```
find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
```

delete all tables from db

```
python manage.py cleardb
```
./manage.py loadtestdata velos.BikeModels:5 && \
./manage.py loadtestdata velos.BikeMarks:5 && \
./manage.py loadtestdata velos.BikeColors:5 && \
./manage.py loadtestdata velos.BikeWeelSizes:5 && \
./manage.py loadtestdata velos.BikeSizes:5 && \
./manage.py loadtestdata velos.BikeGender:5 && \
./manage.py loadtestdata velos.BikeTypes:5 && \
./manage.py loadtestdata velos.BikeSpeeds:5 && \
./manage.py loadtestdata velos.BikeFrames:5 && \



autofill DB with autofixtures
```
./manage.py loadtestdata velos.Bike:5 --generate-fk ALL && \
./manage.py loadtestdata velos.Review:120 && \
./manage.py loadtestdata velos.Characteristic:5 && \
./manage.py loadtestdata velos.BikeSuspensions:5 && \
./manage.py loadtestdata velos.BikePurposes:5 && \
./manage.py loadtestdata velos.AdditionalService:5 && \
./manage.py loadtestdata velos.Tariff:5 && \
./manage.py loadtestdata velos.Bike:55

```


```
sudo $(which python) manage.py runserver 0.0.0.0:80
```
