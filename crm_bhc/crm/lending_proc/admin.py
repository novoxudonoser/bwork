from django.db import models
from django.contrib import admin
from django.forms import CheckboxSelectMultiple

from crm.lending_proc.models import Pfones,PfoneShown,Companes,PfoneIncomeLog,Lendings,Clients,ClientsTrack

class PfonesAdmin(admin.ModelAdmin):
    def clean(self):
        if self.is_default:
            temp = Pfones.objects.get(is_default=True)
            if temp and self != temp:
                raise forms.ValidationError("Может быть тоько один резервный номер")
        return self.cleaned_data["name"]
    list_display = ('lending', 'number','compane',)
    list_filter = ('is_default','lending','compane')
    ordering = ('lending',)
    search_fields = ('number','compane')

admin.site.register(Pfones,PfonesAdmin)

class PfoneShownAdmin(admin.ModelAdmin):
    list_display = ('client', 'phone','cookie','shown_date')
    list_filter = ('active','phone')
    ordering = ('-shown_date',)
    search_fields = ('cookie',)

admin.site.register(PfoneShown,PfoneShownAdmin)

class CompanesAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
    search_fields = ('name',)

admin.site.register(Companes,CompanesAdmin)

class PfoneIncomeLogAdmin(admin.ModelAdmin):
    list_display = ('lending','phone_from','phone_to','client','income_date','query')
    list_filter = ('lending','phone_to')
    ordering = ('-income_date',)


admin.site.register(PfoneIncomeLog,PfoneIncomeLogAdmin)

class LendingsAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
    search_fields = ('name',)

admin.site.register(Lendings,LendingsAdmin)

from crm.lending_proc.models import ApplicationUser,ManagerEmail


class ManagerEmailAdmin(admin.ModelAdmin):
    list_display = ('email', 'active')
    list_filter = ('active',)
    ordering = ('email',)
    search_fields = ('email',)

admin.site.register(ManagerEmail,ManagerEmailAdmin)

class ApplicationUserAdmin(admin.ModelAdmin):
    list_display = ('lending','user_name','email','phone','ip_address','created','query')
    ordering = ('-created',)
    search_fields = ('email','user_name','phone')

admin.site.register(ApplicationUser,ApplicationUserAdmin)

class ClientsAdmin(admin.ModelAdmin):
    list_display = ('cookie_track','date')
    ordering = ('-date',)
    search_fields = ('cookie_track',)

admin.site.register(Clients,ClientsAdmin)

class ClientsTrackAdmin(admin.ModelAdmin):
    list_display = ('lending','client','url','query','date')
    ordering = ('-date',)
    search_fields = ('client','url')

admin.site.register(ClientsTrack,ClientsTrackAdmin)

from crm.lending_proc.models import DirectCompanes,DirectGroups,DirectPhrases,SearchqQuerys
from crm.lending_proc.models import YandexGetParametrsDynamic,YandexGetParametrsStatic,SearchqQuerysStats
from crm.lending_proc.models import DirectAdds,YgetparamesstatSearchqerysRel

admin.site.register(DirectCompanes)
admin.site.register(DirectGroups)
admin.site.register(DirectAdds)
admin.site.register(DirectPhrases)

class SearchqQuerysAdmin(admin.ModelAdmin):
    readonly_fields=('id',)


admin.site.register(SearchqQuerys,SearchqQuerysAdmin)


class YgetparamesstatSearchqerysRelAdmin(admin.ModelAdmin):
    readonly_fields=('id',)

admin.site.register(YgetparamesstatSearchqerysRel,YgetparamesstatSearchqerysRelAdmin)

class YandexGetParametrsDynamicAdmin(admin.ModelAdmin):
    list_display = (
        'visit','position','position_type','yclid',
        'device_type','retargeting_id','adtarget_id','adtarget_name',
        'addphrases','addphrasestext',
    )
    list_filter = ('device_type','position_type')
    ordering = ('-id',)
    readonly_fields=('id',)

class YandexGetParametrsStaticAdmin(admin.ModelAdmin):
    list_display = (
        'campaign_id','campaign_type','gbid','ad_id',
        'phrase_id','keyword','source','source_type',
        'region_name','region_id',
        )
    list_filter = ('campaign_type','source_type')
    ordering = ('-id',)
    filter_horizontal = ('query',)
    readonly_fields=('id',)

admin.site.register(YandexGetParametrsDynamic,YandexGetParametrsDynamicAdmin)
admin.site.register(YandexGetParametrsStatic,YandexGetParametrsStaticAdmin)
admin.site.register(SearchqQuerysStats)