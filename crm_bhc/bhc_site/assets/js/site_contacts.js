/**
 * Created by PekopT on 17.09.15.
 */

$(function () {
    var smth = true;
    while (smth) {
        if (typeof(ymaps) != 'undefined') {
            setYaMap();
            smth = false;
        }
    }
});

function setYaMap() {
    if ($("#YMapsID").size() > 0) {
        if (typeof(ymaps) != 'undefined') {
            ymaps.ready(function () {
                var z = 10;
                var map = new ymaps.Map("YMapsID", {
                            center: [55.76, 37.64],
                            zoom: z,
                            type: "yandex#map",
                            behaviors: ['default']
                        }
                );
                map.controls.add("mapTools").add("zoomControl").add("typeSelector");
                setYaMap.map = map;

                var data = {}, i = 0;
                $('[data-address]').each(function () {
                    data[i] = $(this).data('address');
                    i++;
                });
                addYaMapObjects(data, ymaps);
            });
        }
    }
}

function addYaMapObjects(addrArr, ymaps) {
    console.log(addrArr);
    var map = setYaMap.map;
    if (typeof(addYaMapObjects.mapObjects) == 'undefined') {
        addYaMapObjects.mapObjects = [];
    }
    var x = 0;
    var points = [];
    for (var k in addrArr) {
        points.push(addrArr[k]);
        x++;
    }
    points = points.unique();
    if (x == 0) return false;
    var multiGeocoder = new MultiplyGeocoder({results: 1});
    multiGeocoder
            .geocode(points)
            .then(
            function (res) {
                var iterator = res.geoObjects.getIterator();
                var next;

                var i = 0;
                if (typeof(myCollection) == 'undefined') {
                    myCollection = new ymaps.GeoObjectCollection();
                } else {
                    myCollection.removeAll();
                }

                while (next = iterator.getNext()) {
                    var coords = next.geometry.getCoordinates();
                    var options = {
                        balloonContentHeader: points[i],
                        balloonContentBody: points[i]
                    };
                    var myPlacemark = new ymaps.Placemark(coords, options);
                    myCollection.add(myPlacemark);
                    i++;
                }

                map.setBounds(getBounds(myCollection), {
                    checkZoomRange: true,
                    callback: function (err) {
                    }
                });
                map.geoObjects.add(myCollection);
            },
            function (err) {
            }
    );
}