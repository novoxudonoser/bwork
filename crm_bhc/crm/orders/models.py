# -*- coding: utf-8 -*- 
from django.core.urlresolvers import reverse

from crm.api.models import SubwayStantions, User

__author__ = 'PekopT'

from django.db import models
from crm.api.models import City
from crm.calls.models import CallQuestion, CallTheme

PAYMENT_TYPE = (
    (1, 'Безналичная оплата'),
    (2, 'Наличная оплата'),
)

DELIVERY_TYPE = (
    (1, 'Курьером'),
    (2, 'Самовывозом'),
)

TYPE_ADDRESS = (
    (11, 'Дом'),
    (12, 'Владение'),
    (13, 'Домовладение'),
    (21, 'Корпус'),
    (22, 'Строение'),
    (23, 'Литера'),
    (24, 'Сооружение'),
    (25, 'Участок'),
    (31, 'Офис'),
    (32, 'Помещение'),
    (33, 'Квартира'),
    (34, 'Комната'),
    (35, 'Бокс'),
)

TYPE_HOUSE = (
    (1, 'Дом'),
    (2, 'Владение'),
    (3, 'Домовладение'),
)

TYPE_PLACE = (
    (1, 'Корпус'),
    (2, 'Строение'),
    (3, 'Литера'),
    (4, 'Сооружение'),
    (5, 'Участок'),
)

TYPE_APPARTAMENT = (
    (1, 'Офис'),
    (2, 'Помещение'),
    (3, 'Квартира'),
    (4, 'Комната'),
    (5, 'Бокс'),
)


class OrderAddress(models.Model):
    name = models.CharField(max_length=255, verbose_name='Адрес')
    city = models.ForeignKey(City, verbose_name='Город, нас.пункт' ,default=1)
    street = models.CharField(max_length=100, verbose_name='Улица', blank=True, null=True)
    type_house = models.IntegerField(choices=TYPE_HOUSE, blank=True, null=True, default=1)
    house = models.CharField(max_length=50, verbose_name='Дом', blank=True, null=True)
    type_place = models.IntegerField(choices=TYPE_PLACE, blank=True, null=True, default=1)
    place = models.CharField(max_length=50, blank=True, null=True)
    type_appartament = models.IntegerField(choices=TYPE_APPARTAMENT, blank=True, null=True, default=1)
    appartament = models.CharField(max_length=50, verbose_name='Офис', blank=True, null=True)

    metro = models.ForeignKey(SubwayStantions, verbose_name='Станция метро', blank=True, null=True)

    def __str__(self):
        return self.name


class AdditionalInfoAddress(models.Model):
    """ Дополнительная информация об адресе """

    order_address = models.ForeignKey(OrderAddress, related_name='additional_data')
    type_address = models.IntegerField(choices=TYPE_ADDRESS)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "%s %s" % (self.get_type_address_display(), self.name)


class ContactPhone(models.Model):
    additional_number = models.PositiveSmallIntegerField(verbose_name='Добавочный', blank=True, null=True)
    phone = models.CharField(max_length=17, verbose_name='Телефон', default=None, blank=True, null=True)
    email = models.EmailField(max_length=255, verbose_name='Почта', default=None, blank=True, null=True)

    def __str__(self):
        return self.phone or self.email


class OrderContact(models.Model):
    name = models.CharField(max_length=255, verbose_name='Контактное лицо', default=1)
    phones = models.ManyToManyField(ContactPhone, verbose_name='Телефоны', related_name='phone_contacts')
    address = models.ManyToManyField(OrderAddress, verbose_name='Адреса', related_name='contacts', blank=True, null=True)

    def __str__(self):
        if self.phones.all():
            return "{0} ({1})".format(self.name, ', '.join([str(x) for x in self.phones.all()]))
        else:
            return self.name

    def get_emails(self):
        return ', '.join([p.get('email') for p in self.phones.all().values('email') if p.get('email')])

    def get_phones(self):
        return ', '.join([p.get('phone') for p in self.phones.all().values('phone') if p.get('phone')])

    def get_additional(self):
        return ', '.join([str(p.get('additional_number')) for p in self.phones.all().values('additional_number') if p.get('additional_number')])

    def get_absolute_url(self):
        return reverse('contact-detail', kwargs={'pk': self.pk})


class OrderDocument(models.Model):
    theme = models.ForeignKey(
        CallTheme,
        verbose_name='Тема звонка',
        related_name='order_themes',
        default=7,
        editable=False,
    )
    question = models.ForeignKey(
        CallQuestion,
        verbose_name='Вопрос звонка',
        related_name='order_documents'
    )
    date_complete = models.DateField(null=True, verbose_name='Дата готовности документа', blank=True)

    # egrul
    firm = models.CharField(max_length=255, verbose_name='Название компании', null=True, blank=True)
    # egrip
    fname_ip = models.CharField(max_length=255, verbose_name='Фамилия ИП', null=True,
                                blank=True)
    lname_ip = models.CharField(max_length=255, verbose_name='Имя ИП', null=True,
                                blank=True)
    mname_ip = models.CharField(max_length=255, verbose_name='Отчество ИП', null=True,
                                blank=True)
    # overall
    inn = models.CharField(max_length=255, verbose_name='ИНН', blank=True, null=True)
    ogrn = models.CharField(max_length=255, verbose_name='ОГРН', blank=True, null=True)
    count = models.PositiveIntegerField(verbose_name="Количество документов")

    is_deleted = models.BooleanField(verbose_name='Активность', default=False)
    address = models.ForeignKey(OrderAddress, verbose_name='Адрес клиента', related_name='order_addresses', null=True,
                                    default=None, blank=True)
    route = models.CharField(max_length=255, verbose_name='Номер маршрута', null=True, blank=True)

    def get_name_comp(self):
        if self.firm:
            return self.firm
        elif self.fname_ip:
            return self.fname_ip
        else:
            return ''

    def get_questions(self):
        return self.question.name

    def __str__(self):
        return "document #" + str(self.pk)


class OrderStatus(models.Model):
    """ Status of order """

    name = models.CharField(max_length=255, verbose_name='Статус заказа', unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Статус заказа'
        verbose_name_plural = 'Статусы заказов'


class Order(models.Model):
    number = models.CharField(max_length=13, verbose_name="Номер заказа", null=True, blank=True, default=None)
    date_created = models.DateField(verbose_name='Дата создания')
    date_updated = models.DateTimeField(verbose_name='Дата обновления', auto_now=True)

    documents = models.ManyToManyField(OrderDocument, verbose_name='Документы в заказе', related_name='document_order')

    user = models.ForeignKey(User, related_name='orders', verbose_name='Оператор', blank=True, null=True)
    client = models.ForeignKey(OrderContact, verbose_name='Клиент', related_name='client_orders', null=True,
                               default=None, blank=True)
    address = models.ForeignKey(OrderAddress, related_name='orders', verbose_name='Адрес', blank=True, null=True)

    payment_type = models.IntegerField(choices=PAYMENT_TYPE, default=1, verbose_name='Способ оплаты')
    payment_name = models.CharField(max_length=255, verbose_name='Наименование плательщика', null=True, blank=True)
    payment_inn = models.CharField(max_length=255, verbose_name='ИНН плательщика', null=True, blank=True)
    payment_kpp = models.CharField(max_length=255, verbose_name='КПП плательщика', null=True, blank=True)

    delivery_type = models.IntegerField(choices=DELIVERY_TYPE, verbose_name='Способ доставки', null=True, blank=True)
    status = models.ForeignKey(OrderStatus, verbose_name='Статус', blank=True, null=True)

    def get_absolute_url(self):
        return reverse('orders-detail', kwargs={'pk': self.pk})

    def calcDateComplete(self):
        return ','.join([x.date_complete.strftime('%d.%m.%Y') for x in self.documents.all() if x.date_complete])

    date_complete = property(calcDateComplete)

    def __str__(self):
        return self.number

    class Meta:
        ordering = ['-date_created', '-date_updated']
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
