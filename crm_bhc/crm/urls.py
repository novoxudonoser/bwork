from django.conf.urls import patterns, include, url
from django.contrib import admin
from crm.calls.views import BaseView

from crm.application_user.views import ApplicationUsers
from crm.orders.views import Request, ContactList, ContactDetail, ContactDelete, PhoneDelete, Route, DocumentDetail
from crm.users.views import LoginView

urlpatterns = patterns('',
       url(r'^/?$', BaseView.as_view()),
       url(r'^api/', include('crm.api.urls')),
       url(r'^calls/', include('crm.calls.urls')),
       url(r'^order/', include('crm.orders.urls')),

       url(r'^login/', LoginView.as_view()),

       url(r'^contact/?$', ContactList.as_view(), name='contact-list'),  # contacts list
       url(r'^contact/(?P<pk>\d+)/?$', ContactDetail.as_view(), name='contact-detail'),

       url(r'^documents/?$', DocumentDetail.as_view(), name='document-detail'),
       url(r'^document/(?P<pk>\d+)/?$', DocumentDetail.as_view(), name='document-detail'),

       url(r'^contact/delete/(?P<pk>\d+)/?$', ContactDelete.as_view(), name='contact-delete'),
       url(r'^phone/delete/(?P<pk>\d+)/?$', PhoneDelete.as_view(), name='phone-delete'),
       url(r'^request/', Request.as_view(), name='request'),
       url(r'^routes/', Route.as_view(), name='routes'),
       url(r'^admin/', include(admin.site.urls)),

       url(r'^applications/$', ApplicationUsers.as_view(), name='applications-users'),

       url(r'lending_proc/', include('crm.lending_proc.urls')),
)
