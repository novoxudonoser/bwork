from pages.models import SiteMenu, SocialMenu

__author__ = 'PekopT'

from django import template

register = template.Library()


@register.inclusion_tag('tags/menu.html', takes_context=True)
def header_menu(context, ):
    return {
        'menu': SiteMenu.objects.filter(show_header=True, active=True),
    }


@register.inclusion_tag('tags/menu.html', takes_context=True)
def footer_menu(context, ):
    return {
        'menu': SiteMenu.objects.filter(show_footer=True, active=True),
    }


@register.inclusion_tag('tags/social_menu.html', takes_context=True)
def social_menu(context, ):
    return {
        'menu': SocialMenu.objects.filter(active=True),
    }
