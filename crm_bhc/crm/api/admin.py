from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User, Post, Photo, City, SubwayLines, SubwayStantions

admin.site.register(User, UserAdmin)
admin.site.register(Post)
admin.site.register(Photo)
admin.site.register(City)
admin.site.register(SubwayLines)
admin.site.register(SubwayStantions)
