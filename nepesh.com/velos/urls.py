from django.conf import urls


from .views import Main, StudioView, BikeView, BikeFilter, BikePhotosView, StudioPhotosView,BikeFiltering,StudioFiltering,StudioFilter


urlpatterns = [
    urls.url(r'^$', Main.as_view(), name='main'),
    urls.url(r'^studio/(?P<pk>\d+)/?$', StudioView.as_view(), name='studio'),

    urls.url(r'^studio/(?P<pk>\d+)/(?P<view>\w+)/?$', StudioView.as_view(), name='studio'),
    urls.url(r'^studio/photos/?$', StudioPhotosView.as_view(), name='bike_photos'),

    urls.url(r'^bike/(?P<pk>\d+)/?$', BikeView.as_view(), name='bike'),
    urls.url(r'^bike/photos/?$', BikePhotosView.as_view(), name='bike_photos'),

    urls.url(r'^filter_bicycles$', BikeFilter.as_view(), name='main'),
    urls.url(r'^filter_studios$', StudioFilter.as_view(), name='345'),
    urls.url(r'^templates/bikes_filtering$', BikeFiltering.as_view(), name='123'),
    urls.url(r'^templates/studios_filtering$', StudioFiltering.as_view(), name='234'),    

]
